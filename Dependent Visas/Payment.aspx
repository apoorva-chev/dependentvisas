﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Payment.aspx.cs" Inherits="Dependent_Visas.Payment" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <section class="products full-width margin-top-40 margin-bottom-40">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="thanks-block full-width">
                            <div class="thanks-icon-block">
                                <i class="cti-icon"></i>
                            </div>
                            <div class="thanks-title">
                                Thanks for placing order!
                           
                            </div>
                            <div class="thanks-desc">
                                <p>One of our executive will be in contact with you further process. Feel free to contact us on 1800 0000 00000</p>
                                <p>One of our executive will be in contact with you further process.</p>
                            </div>
                        </div>

                        <div class="order-info full-width">
                            <div class="order-info-header full-width">
                                Below is the order placed with us
                           
                            </div>
                            <div class="order-info-list-block full-width">
                                <div class="order-list-block full-width">
                                    <ul class="order-list full-width">
                                        <li>
                                            <div class="each-order-info">
                                                <div class="each-order-info-property">
                                                    Invoice No:
                                               
                                                </div>
                                                <div class="each-order-info-value">
                                                    <asp:Label Text="" ID="lblInvNo" runat="server" />
                                                </div>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="each-order-info">
                                                <div class="each-order-info-property">
                                                    Order Date:
                                               
                                                </div>
                                                <div class="each-order-info-value">
                                                    <asp:Label Text="" ID="lblOrderDate" runat="server" />
                                                </div>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="each-order-info">
                                                <div class="each-order-info-property">
                                                    Payment Status:
                                               
                                                </div>
                                                <div class="each-order-info-value">
                                                    <asp:Label Text="" ID="lblPaymentStatus" runat="server" />
                                                </div>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="each-order-info">
                                                <div class="each-order-info-property">
                                                    Total Amount:
                                               
                                                </div>
                                                <div class="each-order-info-value">
                                                    <asp:Label Text="" ID="lblOrderAmount" runat="server" />
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="each-order-info">
                                                <div class="each-order-info-property">
                                                    Transaction ID:                                               
                                                </div>
                                                <div class="each-order-info-value">
                                                    <asp:Label Text="" ID="LblTransactionid" runat="server" />
                                                </div>
                                            </div>
                                        </li>
                                        <li style="display: none">
                                            <div class="each-order-info-property">
                                                <a href="#" class="download-link">Download Invoice</a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
         <asp:Literal ID="ltlFooter" runat="server" EnableViewState="false"></asp:Literal>
    </form>
     <script src="/js/jquery.min.js"></script>
    <script src="/js/slick.min.js"></script>
    <script src="/js/common.js"></script>
</body>
</html>
