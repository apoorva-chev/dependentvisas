﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using static Dependent_Visas.Translayer;

namespace Dependent_Visas
{
    public partial class Products : System.Web.UI.Page
    {
        Product objproduct = new Product();
        int pos;
        PagedDataSource adsource = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.ViewState["vs"] = 0;
                GetCountriesCode();
                GetDependencyType();

            }
            pos = (int)this.ViewState["vs"];
            AllProductDetails();


        }
        void GetCountriesCode()
        {
           objproduct.Action= "Country";
           
            DataTable dt = BLL.GetProduct(objproduct);
            if (dt != null && dt.Rows.Count > 0)
            {
                //chkCountry.DataSource = dt;
                //chkCountry.DataTextField = "Name";
                //chkCountry.DataValueField = "id";
                //chkCountry.DataBind();
                //foreach (ListItem item in chkCountry.Items)
                //{
                //    //item.Attributes.Add("class", "badge");
                //    item.Text = "<span class=\"badge\">" + item.Text + "</span>";
                //    //chklstFSOptions.Items.Add(item);
                //}
                foreach (DataRow dr in dt.Rows)
                {
                    ListItem lObj = new ListItem();
                    lObj.Text = dr["Name"].ToString();
                    lObj.Value = dr["id"].ToString();
                    chkCountry.Items.Add(lObj);
                }
            }
        }
        void GetDependencyType()
        {
            DataTable dtDependentType = new DataTable();
            dtDependentType.Columns.Add(new DataColumn("Id", typeof(int)));
            dtDependentType.Columns.Add(new DataColumn("DepedendentType", typeof(string)));
            Product objproduct = new Product();
            objproduct.Action = "Dependency";
            DataTable dt = BLL.GetProduct(objproduct);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    if (Convert.ToInt16(dr["DependencyType"]) == 1)
                    {
                        dtDependentType.Rows.Add(Convert.ToInt16(dr["DependencyType"]), "Spouse");
                    }
                    else if (Convert.ToInt16(dr["DependencyType"]) == 2)
                    {
                        dtDependentType.Rows.Add(Convert.ToInt16(dr["DependencyType"]), "Kids");
                    }
                    else if (Convert.ToInt16(dr["DependencyType"]) == 3)
                    {
                        dtDependentType.Rows.Add(Convert.ToInt16(dr["DependencyType"]), "Parents");
                    }
                }
                if (dtDependentType.Rows.Count > 0)
                {
                    //chkDependend.DataSource = dtDependentType;
                    //chkDependend.DataTextField = "DepedendentType";
                    //chkDependend.DataValueField = "Id";
                    //chkDependend.DataBind();
                    //foreach (ListItem item in chkDependend.Items)
                    //{
                    //    //item.Attributes.Add("class", "badge");
                    //    item.Text = "<span class=\"badge\">" + item.Text  + "</span>";
                    //    //chklstFSOptions.Items.Add(item);
                    //}

                    foreach (DataRow dr in dtDependentType.Rows)
                    {
                        ListItem lObj = new ListItem();
                        lObj.Text = dr["DepedendentType"].ToString();
                        lObj.Value = dr["Id"].ToString();
                        chkDependend.Items.Add(lObj);
                    }
                }
            }
        }

        void AllProductDetails()
        {
            objproduct.Action = "AllProductDetails";
            DataTable dt = BLL.GetProduct(objproduct);
            if (dt.Rows.Count > 0)
            {
                adsource = new PagedDataSource();
                adsource.DataSource = dt.DefaultView;
                adsource.PageSize = 3;
                adsource.AllowPaging = true;
                adsource.CurrentPageIndex = pos;
                ln1.Enabled = !adsource.IsFirstPage;
                lniPrevious.Enabled = !adsource.IsFirstPage;
                ln3.Enabled = !adsource.IsLastPage;
                lnNext.Enabled = !adsource.IsLastPage;
                dlProductsDisplay.DataSource = adsource;
                dlProductsDisplay.DataBind();

            }

        }
        protected void chkCountry_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (chkCountry.SelectedItem != null)
            {
                foreach (ListItem checkBox in chkCountry.Items)
                {
                    if (checkBox.Selected == true)
                    {
                        ViewState["CountryID"] = checkBox.Value;
                        break;
                    }
                }
                if (ViewState["CountryID"] != null)
                {
                    CountryProductDisplay();
                }
            }
            else
            {
                AllProductDetails();
            }
        }
        protected void chkDependend_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (chkDependend.SelectedItem != null)
            {
                foreach (ListItem checkBox in chkDependend.Items)
                {
                    if (checkBox.Selected == true)
                    {
                        ViewState["DependencyID"] = checkBox.Value;
                        break;
                    }
                }
                if (ViewState["DependencyID"] != null)
                {
                    DependencyProductDisplay();
                }
            }
            else
            {
                AllProductDetails();
            }
        }

        void CountryProductDisplay()
        {
            objproduct.Action = "SelectCountry";
            if (ViewState["CountryID"] != null)
            {
                BLL.ICountryID = Convert.ToInt32(ViewState["CountryID"]);
            }
            DataTable dt = BLL.GetProduct(objproduct);
            if (dt.Rows.Count > 0)
            {

                dlProductsDisplay.DataSource = null;
                dlProductsDisplay.DataBind();
                adsource = new PagedDataSource();
                adsource.DataSource = dt.DefaultView;
                adsource.PageSize = 3;
                adsource.AllowPaging = true;
                adsource.CurrentPageIndex = pos;
                ln1.Enabled = !adsource.IsFirstPage;
                lniPrevious.Enabled = !adsource.IsFirstPage;
                ln3.Enabled = !adsource.IsLastPage;
                lnNext.Enabled = !adsource.IsLastPage;

                dlProductsDisplay.DataSource = adsource;
                dlProductsDisplay.DataBind();
            }
        }
        void DependencyProductDisplay()
        {
            objproduct.Action = "SelectDependency";
            if (ViewState["DependencyID"] != null)
            {
                BLL.IDependencyID = Convert.ToInt32(ViewState["DependencyID"]);
            }
            DataTable dt = BLL.GetProduct(objproduct);
            if (dt.Rows.Count > 0)
            {
                dlProductsDisplay.DataSource = null;
                dlProductsDisplay.DataBind();
                adsource = new PagedDataSource();
                adsource.DataSource = dt.DefaultView;
                adsource.PageSize = 3;
                adsource.AllowPaging = true;
                adsource.CurrentPageIndex = pos;
                ln1.Enabled = !adsource.IsFirstPage;
                lniPrevious.Enabled = !adsource.IsFirstPage;
                ln3.Enabled = !adsource.IsLastPage;
                lnNext.Enabled = !adsource.IsLastPage;
                dlProductsDisplay.DataSource = adsource;
                dlProductsDisplay.DataBind();
            }

        }
        public void lniPrevious_Click(object sender, EventArgs e)
        {

        }
        public void ln1_Click(object sender, EventArgs e)
        {
            if (ViewState["CountryID"] != null)
            {
                pos = (int)this.ViewState["vs"];
                pos -= 1;
                this.ViewState["vs"] = pos;
                CountryProductDisplay();
            }
            else if (ViewState["DependencyID"] != null)
            {
                pos = (int)this.ViewState["vs"];
                pos -= 1;
                this.ViewState["vs"] = pos;
                DependencyProductDisplay();
            }
            else
            {

                pos = (int)this.ViewState["vs"];
                pos -= 1;
                this.ViewState["vs"] = pos;
                AllProductDetails();
            }
        }
        public void ln2_Click(object sender, EventArgs e)
        {
            if (ViewState["CountryID"] != null)
            {
                pos = 0;
                CountryProductDisplay();
            }
            else if (ViewState["DependencyID"] != null)
            {
                pos = 0;
                DependencyProductDisplay();
            }
            else
            {
                pos = 0;
                AllProductDetails();
            }
        }
        public void ln3_Click(object sender, EventArgs e)
        {
            if (ViewState["CountryID"] != null)
            {
                pos = adsource.PageCount - 1;
                CountryProductDisplay();
            }
            else if (ViewState["DependencyID"] != null)
            {
                pos = adsource.PageCount - 1;
                DependencyProductDisplay();
            }
            else
            {
                pos = adsource.PageCount - 1;
                AllProductDetails();
            }
        }
        public void lnNext_Click(object sender, EventArgs e)
        {
            if (ViewState["CountryID"] != null)
            {
                pos = (int)this.ViewState["vs"];
                pos += 1;
                this.ViewState["vs"] = pos;
                CountryProductDisplay();
            }
            else if (ViewState["DependencyID"] != null)
            {
                pos = (int)this.ViewState["vs"];
                pos += 1;
                this.ViewState["vs"] = pos;
                DependencyProductDisplay();
            }
            else
            {
                pos = (int)this.ViewState["vs"];
                pos += 1;
                this.ViewState["vs"] = pos;
                AllProductDetails();
            }
        }

    }
}