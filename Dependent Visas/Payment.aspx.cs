﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static Dependent_Visas.Translayer;

namespace Dependent_Visas
{
    public partial class Payment : System.Web.UI.Page
    {
        List<KeyValuePair<string, string>> responseMapp;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Session["invno"] = "ESTSI/2019/3/23/5";
                //if (string.IsNullOrEmpty(Convert.ToString(Session["invno"])))
                //{
                //    Response.Redirect("/products", false);
                //    return;
                //}


                //ltlHeader.Text = Common.Header();
                //ltlFooter.Text = Common.Footer();



                if (!this.Page.IsPostBack)
                {
                    //new DataAccess().GetOrderDetailsByInvoice(Session["invno"].ToString());

                    UpdateEBSPayment();
                    //DataSet ds = new DataAccess().GetOrderDetailsByInvoice(Session["invno"].ToString());
                    //if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    //{
                    //    DataRow dr = ds.Tables[0].Rows[0];
                    //    lblInvNo.Text = dr["InvoiceNo"].ToString();
                    //    lblOrderDate.Text = Convert.ToDateTime(dr["OrderDate"]?.ToString()).ToString("dd MMM yyyy");

                    //    if (dr["Paymentstatus"]?.ToString() == "1")
                    //        lblPaymentStatus.Text = "Paid";
                    //    else
                    //        lblPaymentStatus.Text = "Pending";

                    //    lblOrderAmount.Text = dr["finalAmount"]?.ToString();
                    //    LblTransactionid.Text = dr["TransactionId"]?.ToString();
                    //}
                    //Session["invno"] = "";
                }
            }
            catch (Exception exc)
            {

                throw exc;
            }
        }

        public void UpdateEBSPayment()
        {
            paymentdetails objPayment = new paymentdetails();
            List<KeyValuePair<string, string>> postparamslist = new List<KeyValuePair<string, string>>();

            if (Request.HttpMethod == "GET")
            {
                for (int i = 0; i < Request.QueryString.Keys.Count; i++)
                {
                    KeyValuePair<string, string> postparam = new KeyValuePair<string, string>(Request.QueryString.Keys[i], Request.QueryString[i]);
                    postparamslist.Add(postparam);
                }
            }
            else
            {
                for (int i = 0; i < Request.Form.Keys.Count; i++)
                {
                    KeyValuePair<string, string> postparam = new KeyValuePair<string, string>(Request.Form.Keys[i], Request.Form[i]);
                    postparamslist.Add(postparam);
                }
            }

            postparamslist.Sort(Compare1);


            //string HashData = "c8acb7dfa3aba08d899bc12928775006"; //Pass Your SecretKey Here

            string HashData = ConfigurationManager.AppSettings["secretKeyStudy"]; //Pass Your SecretKey Here

            string resp_hd = Request.Params["SecureHash"];
            int rc = Convert.ToInt32(Request.Params["ResponseCode"]);
            string rflag = Request.Params["IsFlagged"];

            foreach (KeyValuePair<string, string> param in postparamslist)
            {
                if (param.Key != "SecureHash" && param.Value != "")
                    HashData += "|" + param.Value;
            }

            string hashedvalue = "";

            objPayment.responseMessage = postparamslist.Find(x => x.Key == "ResponseMessage").Value;
            objPayment.transactionDate = postparamslist.Find(x => x.Key == "DateCreated").Value;
            objPayment.paymentId = postparamslist.Find(x => x.Key == "PaymentID").Value;
            objPayment.transactionId = postparamslist.Find(x => x.Key == "TransactionID").Value;
            objPayment.orderRefNo = postparamslist.Find(x => x.Key == "MerchantRefNo").Value;
            objPayment.responseCode = postparamslist.Find(x => x.Key == "ResponseCode").Value;
            objPayment.amount = postparamslist.Find(x => x.Key == "Amount").Value;
            hashedvalue += computeHash(HashData);

            if (hashedvalue == resp_hd)
            {
                if (rc == 0)
                {
                    if (rflag == "NO")
                    {
                        objPayment.Paymentstatus = "1";
                        //Response.Write("<center>Your Transaction was successful</center>");
                    }
                    else
                    {
                        objPayment.Paymentstatus = "2";
                    }
                    //Response.Write("<center>Your payment is kept on hold</center>");
                }
                else if (rc == 1)
                    //Response.Write("<center>Your payment is pending</center>"); //ResponseCode '1' is only applcable for NEFT/Challan based payments
                    objPayment.Paymentstatus = "2";
                else
                    //Response.Write("<center>Your transaction failed</center>");
                    objPayment.Paymentstatus = "3";
            }
            else
                Response.Write("<center>Hash Validation Failed!</center>");

            //new DataAccess().UpdateOnlinePayment(orderRefNo, paymentId, transactionId, amount, transactionDate, responseMessage, Paymentstatus);
            //if (objPayment.Paymentstatus == "1")
            //{
            //    //Session["invno"] = orderRefNo;
            //    //new DataAccess().SendEmail(-1, "Support", "noreply@email.studyideas.com", "Bharathi@email.studyideas.com", "", "", "Online Payment Received from " + orderRefNo, "<p>Hi,</p><p>An order placed with online payment Please check orders screen for more information.</p><p>Thanks &amp; Regards,</p><p>Studyidea</p>", 0, "", "5");
            //}

        }

        static string computeHash(string input)
        {

            byte[] data = null;

            /*select your hashing algorithm below */
            data = HashAlgorithm.Create("SHA512").ComputeHash(Encoding.ASCII.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString().ToUpper();

        }

        static int Compare1(KeyValuePair<string, string> a, KeyValuePair<string, string> b)
        {
            return a.Key.CompareTo(b.Key);
        }


        #region Paynear parameters

        //public void Paynearpaymentupdate()
        //{
        //    List<KeyValuePair<string, string>> inparams = new List<KeyValuePair<string, string>>();
        //    for (int i = 0; i < Request.Form.Keys.Count; i++)
        //    {
        //        inparams.Add(new KeyValuePair<string, string>(Request.Form.Keys[i], Request[Request.Form.Keys[i]]));
        //    }
        //    PaynearEpay PaymentService = new PaynearEpay();
        //    List<KeyValuePair<string, string>> statusResponseMap = PaymentService.getPaymentResponse(inparams);
        //    //After getreturn "statusResponseMap" from EpayController we checking "statusResponseMap" null or not-null
        //    if (statusResponseMap != null)
        //    {
        //        // success
        //        //merchant need to be bind data with Response page
        //        orderRefNo = statusResponseMap.Find(x => x.Key == "orderRefNo").Value;
        //        amount = statusResponseMap.Find(x => x.Key == "amount").Value;
        //        paymentId = statusResponseMap.Find(x => x.Key == "paymentId").Value;
        //        //merchantId = statusResponseMap.Find(x => x.Key == "merchantId").Value;
        //        transactionId = statusResponseMap.Find(x => x.Key == "transactionId").Value;
        //        responseMessage = statusResponseMap.Find(x => x.Key == "responseMessage").Value;
        //        responseCode = statusResponseMap.Find(x => x.Key == "responseCode").Value;
        //        //transactionType = statusResponseMap.Find(x => x.Key == "transactionType").Value;
        //        transactionDate = statusResponseMap.Find(x => x.Key == "transactionDate").Value;
        //        //currencyCode = statusResponseMap.Find(x => x.Key == "currencyCode").Value;
        //        //paymentMethod = statusResponseMap.Find(x => x.Key == "paymentMethod").Value;
        //        //paymentBrand = statusResponseMap.Find(x => x.Key == "paymentBrand").Value;
        //        if (responseCode.ToString() == "000")
        //        {
        //            new DataAccess().UpdateOnlinePayment(orderRefNo, paymentId, transactionId, amount, transactionDate, responseMessage);
        //        }
        //    }
        //}


        #endregion
        // -- Start HTML minify code ----
        protected override void Render(HtmlTextWriter writer)
        {
            using (HtmlTextWriter htmlwriter = new HtmlTextWriter(new System.IO.StringWriter()))
            {
                base.Render(htmlwriter);
                string html = htmlwriter.InnerWriter.ToString();
                foreach (string tag in tags)
                    html = this.RemoveTag(tag, html);
                writer.Write(RemoveWhitespaceFromHtml(html));
            }
        }
        public string RemoveTag(string tag, string html)
        {
            int lowerBound = html.IndexOf(tag);
            if (lowerBound < 0) return html;
            while (html[lowerBound--] != '<') ;
            int upperBound = html.IndexOf("/>", lowerBound) + 2;
            html = html.Remove(lowerBound, upperBound - lowerBound);
            if (html.Contains(tag)) html = this.RemoveTag(tag, html);
            return html;
        }

        // -- Start HTML minify code ----
        private static readonly Regex RegexBetweenTags = new Regex(@">(?! )\s+", RegexOptions.Compiled);
        private static readonly Regex RegexLineBreaks = new Regex(@"([\n\s])+?(?<= {2,})<", RegexOptions.Compiled);
        string[] tags = { "__VIEWSTATE", "__EVENTTARGET", "__EVENTARGUMENT", "__EVENTVALIDATION" };

        public static string RemoveWhitespaceFromHtml(string html)
        {
            html = RegexBetweenTags.Replace(html, ">");
            html = RegexLineBreaks.Replace(html, "<");
            int t1 = html.IndexOf("<title>");
            int t2 = html.IndexOf("</title>") + 8;
            if (t1 != -1 && t2 != -1)
            {
                string newTitleTag = html.Substring(t1, t2 - t1);
                //html = html.Replace(newTitleTag, string.Format("<title>{0}</title>", pagetitle));
            }
            return html.Trim();
        }
    }
}