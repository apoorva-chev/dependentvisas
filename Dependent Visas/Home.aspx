﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DependentVisas.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Dependent_Visas.Home" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
    <div>
        <section class="full-width banner-block main-content bg-secondary">
      <div class="container">
        <div class="row align-items-stretch">
          <div class="col-12 col-md-6 col-lg-8">
            <div
              class="full-width banner h-100 d-flex align-items-center justify-content-center text-white"
            >
              Banner Comes here
            </div>
          </div>
          <div class="col-12 col-md-6 col-lg-4">
             <div
              class="full-width home-quick-enquiry-block text-white py-1"
            >
              <div class="home-enquiry-title full-width heading-md text-center">
                <span>Quick Enquiry</span>
              </div>
              <ul class="home-enquiry-form full-width list-unstyled mb-0">
                <li class="qc-name">
                  <label class="full-width">Name:</label>
                  <input
                    type="text"
                    class="full-width form-control"
                    placeholder="Your Name" required="required" id="txtName" runat="server"
                  />
                </li>
                <li class="qc-email">
                  <label class="full-width">Email:</label>
                  <input
                    type="email"
                    class="full-width form-control"
                    placeholder="Your Email Id" required="required" id="txtEmail" runat="server"
                  />
                    <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtEmail" ErrorMessage="Invalid Email Format" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                </li>
                <li class="qc-contact">
                  <label class="full-width">Contact Number:</label>
                  <div class="full-width">
                    <div class="row no-gutters">
                      <div class="col-4">
                        <select name="" id="ddlCountry" class="form-control" runat="server">
                         <%-- <option value="">+91 IND</option>
                          <option value="">+1234 ABCD</option>
                          <option value="">+91 IND</option>
                          <option value="">+1234 ABCD</option>--%>
                        </select>
                      </div>
                      <div class="col-8 pl-1">
                        <input
                          type="tel"
                          class="form-control full-width"
                          placeholder="Your Mobile Number" required="required" id="txtContact" runat="server"
                        />
                      </div>
                    </div>
                  </div>
                </li>
                <li class="qc-actions text-right">
                  <label class="checkbox">
                    <input type="checkbox" class="checkbox-input" id="chkTerms" runat="server" />

                    <span class="checkbox-text text-white">
                      I agree
                      <a href="#" class="text-white text-underline"
                        >Terms &amp; Conditions</a
                      >
                    </span>
                  </label>
                  <div class="text-right submit-enquiry-btn-block">
                    <asp:button runat="server" ID="btnEnquiry" Text="Submit Enquiry" OnClick="btnEnquiry_Click"
                      class="btn btn-primary px-3 border-white submit-enquiry-btn">
                      
                    </asp:button>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="full-width home-sections py-3">
      <div class="container">
        <!-- Most Viewed Starts -->
        <div class="row">
          <div class="col-12">
            <div class="full-width each-home-section most-viewed">
              <div class="full-width each-home-section-heading pb-2">
                <h1 class="heading-lg">Most viewed Dependent Visas</h1>
                <a href="products.aspx" class="view-all-links">(View All)</a>
              </div>
                <asp:DataList ID="dlMostViewed" runat="server" RepeatColumns="4">
                    <ItemTemplate>
                        <div class="each-home-section-list-block full-width">
                <ul class="each-home-section-list row align-items-stretch">
                  <li  class="col-12 col-md-6 col-lg-11">
                    <div class="each-product move-product">
                      <a
                        href="productDetails.aspx"
                        class="full-width each-product-link-block"
                      >
                        <div class="each-product-title-block full-width">
                          <span
                            class="each-product-image full-width"
                            style="background-image: url(images/visa-1.jpg);"
                          >
                          </span>
                          <span class="each-product-title-text"
                            ><%#Eval("Name")%></span
                          >
                        </div>
                        <div class="each-product-desc full-width text-justify">
                          <%#Eval("Description")%>
                        </div>
                        <div class="each-product-action full-width text-center">
                          <span class="each-product-btn">
                            Get Visa
                          </span>
                        </div>
                      </a>

                      <div class="each-product-tags-list full-width">
                        <a href="#" class="each-tag">spouse</a>
                        <a href="#" class="each-tag">parents</a>
                        <a href="#" class="each-tag">+2 more</a>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
                    </ItemTemplate>
                </asp:DataList>
              <%--<div class="each-home-section-list-block full-width">
                <ul class="each-home-section-list row align-items-stretch">
                  <li class="col-12 col-md-6 col-lg-3">
                    <div class="each-product move-product">
                      <a
                        href="productDetails.aspx"
                        class="full-width each-product-link-block"
                      >
                        <div class="each-product-title-block full-width">
                          <span
                            class="each-product-image full-width"
                            style="background-image: url(images/visa-1.jpg);"
                          >
                          </span>
                          <span class="each-product-title-text"
                            >Dependent Visa 1</span
                          >
                        </div>
                        <div class="each-product-desc full-width text-justify">
                          Lorem ipsum dolor sit, amet consectetur adipisicing
                          elit. Necessitatibus eius aspernatur officia quisquam
                          porro quaerat dignissimos rem iste esse amet sint
                          facilis placeat, aliquid neque doloremque eos laborum
                          ipsa nobis.
                        </div>
                        <div class="each-product-action full-width text-center">
                          <span class="each-product-btn">
                            Get Visa
                          </span>
                        </div>
                      </a>

                      <div class="each-product-tags-list full-width">
                        <a href="#" class="each-tag">spouse</a>
                        <a href="#" class="each-tag">parents</a>
                        <a href="#" class="each-tag">+2 more</a>
                      </div>
                    </div>
                  </li>
                  <li class="col-12 col-md-6 col-lg-3">
                    <div class="each-product move-product">
                      <a
                        href="productDetails.aspx"
                        class="full-width each-product-link-block"
                      >
                        <div class="each-product-title-block full-width">
                          <span
                            class="each-product-image full-width"
                            style="background-image: url(images/visa-1.jpg);"
                          >
                          </span>
                          <span class="each-product-title-text"
                            >Dependent Visa 2</span
                          >
                        </div>
                        <div class="each-product-desc full-width text-justify">
                          Lorem ipsum dolor sit, amet consectetur adipisicing
                          elit. Necessitatibus eius aspernatur officia quisquam
                          porro quaerat dignissimos rem iste esse amet sint
                          facilis placeat, aliquid neque doloremque eos laborum
                          ipsa nobis.
                        </div>
                        <div class="each-product-action full-width text-center">
                          <span class="each-product-btn">
                            Get Visa
                          </span>
                        </div>
                      </a>

                      <div class="each-product-tags-list full-width">
                        <a href="#" class="each-tag">parents</a>
                        <a href="#" class="each-tag">spouse</a>
                      </div>
                    </div>
                  </li>
                  <li class="col-12 col-md-6 col-lg-3">
                    <div class="each-product move-product">
                      <a
                        href="productDetails.aspx"
                        class="full-width each-product-link-block"
                      >
                        <div class="each-product-title-block full-width">
                          <span
                            class="each-product-image full-width"
                            style="background-image: url(images/visa-1.jpg);"
                          >
                          </span>
                          <span class="each-product-title-text"
                            >Dependent Visa 3</span
                          >
                        </div>
                        <div class="each-product-desc full-width text-justify">
                          Lorem ipsum dolor sit, amet consectetur adipisicing
                          elit. Necessitatibus eius aspernatur officia quisquam
                          porro quaerat dignissimos rem iste esse amet sint
                          facilis placeat, aliquid neque doloremque eos laborum
                          ipsa nobis.
                        </div>
                        <div class="each-product-action full-width text-center">
                          <span class="each-product-btn">
                            Get Visa
                          </span>
                        </div>
                      </a>

                      <div class="each-product-tags-list full-width">
                        <a href="#" class="each-tag">spouse</a>
                        <a href="#" class="each-tag">parents</a>
                      </div>
                    </div>
                  </li>
                  <li class="col-12 col-md-6 col-lg-3">
                    <div class="each-product move-product">
                      <a
                        href="productDetails.aspx"
                        class="full-width each-product-link-block"
                      >
                        <div class="each-product-title-block full-width">
                          <span
                            class="each-product-image full-width"
                            style="background-image: url(images/visa-1.jpg);"
                          >
                          </span>
                          <span class="each-product-title-text"
                            >Dependent Visa 4</span
                          >
                        </div>
                        <div class="each-product-desc full-width text-justify">
                          Lorem ipsum dolor sit, amet consectetur adipisicing
                          elit. Necessitatibus eius aspernatur officia quisquam
                          porro quaerat dignissimos rem iste esse amet sint
                          facilis placeat, aliquid neque doloremque eos laborum
                          ipsa nobis.
                        </div>
                        <div class="each-product-action full-width text-center">
                          <span class="each-product-btn">
                            Get Visa
                          </span>
                        </div>
                      </a>

                      <div class="each-product-tags-list full-width">
                        <a href="#" class="each-tag">spouse</a>
                        <a href="#" class="each-tag">parents</a>
                        <a href="#" class="each-tag">+2 more</a>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>--%>
            </div>
          </div>
        </div>
        <!-- Most Viewed Ends -->


        <!-- Country Dpendent Visas Starts -->
        <div class="row">
          <div class="col-12">
            <div class="full-width each-home-section most-viewed">
              <div class="full-width each-home-section-heading pb-2">
                <h2 class="heading-lg">Dependent Visas by Country</h2>
                <a href="products.aspx" class="view-all-links">(View All)</a>
              </div>
              <div class="each-home-section-list-block full-width">
                <ul class="each-home-section-list row align-items-stretch">
                  <li class="col-12 col-md-6 col-lg-3">
                    <div class="each-product each-product-by-country">
                      <a href="products.aspx" class="each-product-link-block full-width">
                        <div class="each-product-country-flag full-width" style="background-image: url(images/country.jpg);">
                        </div>
                        <div class="each-product-action full-width text-center">
                          <span class="each-product-btn">
                            Get Visa
                          </span>
                        </div>
                      </a>
                    </div>
                  </li>
                  <li class="col-12 col-md-6 col-lg-3">
                    <div class="each-product each-product-by-country">
                      <a href="products.aspx" class="each-product-link-block full-width">
                        <div class="each-product-country-flag full-width" style="background-image: url(images/country.jpg);">
                        </div>
                        <div class="each-product-action full-width text-center">
                          <span class="each-product-btn">
                            Get Visa
                          </span>
                        </div>
                      </a>
                    </div>
                  </li>
                  <li class="col-12 col-md-6 col-lg-3">
                    <div class="each-product each-product-by-country">
                      <a href="products.aspx" class="each-product-link-block full-width">
                        <div class="each-product-country-flag full-width" style="background-image: url(images/country.jpg);">
                        </div>
                        <div class="each-product-action full-width text-center">
                          <span class="each-product-btn">
                            Get Visa
                          </span>
                        </div>
                      </a>
                    </div>
                  </li>
                  <li class="col-12 col-md-6 col-lg-3">
                    <div class="each-product each-product-by-country">
                      <a href="products.aspx" class="each-product-link-block full-width">
                        <div class="each-product-country-flag full-width" style="background-image: url(images/country.jpg);">
                        </div>
                        <div class="each-product-action full-width text-center">
                          <span class="each-product-btn">
                            Get Visa
                          </span>
                        </div>
                      </a>
                    </div>
                  </li>

                </ul>
              </div>
            </div>
          </div>
        </div>
        <!-- Country Dpendent Visas Ends -->

        <!-- Recently added Dependent Visas Starts -->
        <div class="row">
          <div class="col-12 pt-1">
            <div class="full-width each-home-section dependent-visas-1">
              <div class="full-width each-home-section-heading pb-2">
                <h1 class="heading-lg">Recently Added Dependent Visas</h1>
                <a href="products.aspx" class="view-all-links">(View All)</a>
              </div>
                <asp:DataList ID="dlProductsDisplay" runat="server" RepeatColumns="4">
                    <ItemTemplate>
                        <div class="each-home-section-list-block full-width">
                <ul class="each-home-section-list row align-items-stretch">
                  <li  class="col-12 col-md-6 col-lg-11" style="padding-right:25px">
                    <div class="each-product move-product">
                      <a
                        href="productDetails.aspx"
                        class="full-width each-product-link-block"
                      >
                        <div class="each-product-title-block full-width">
                          <span
                            class="each-product-image full-width"
                            style="background-image: url(images/visa-1.jpg);"
                          >
                          </span>
                          <span class="each-product-title-text"
                            ><%#Eval("Name")%></span
                          >
                        </div>
                        <div class="each-product-desc full-width text-justify">
                          <%#Eval("Description")%>
                        </div>
                        <div class="each-product-action full-width text-center">
                          <span class="each-product-btn">
                            Get Visa
                          </span>
                        </div>
                      </a>

                      <div class="each-product-tags-list full-width">
                        <a href="#" class="each-tag">spouse</a>
                        <a href="#" class="each-tag">parents</a>
                        <a href="#" class="each-tag">+2 more</a>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
                    </ItemTemplate>
                </asp:DataList>
              <%--<div class="each-home-section-list-block full-width">
                <ul class="each-home-section-list row align-items-stretch">
                  <li class="col-12 col-md-6 col-lg-3">
                    <div class="each-product move-product">
                      <a
                        href="productDetails.aspx"
                        class="full-width each-product-link-block"
                      >
                        <div class="each-product-title-block full-width">
                          <span
                            class="each-product-image full-width"
                            style="background-image: url(images/visa-1.jpg);"
                          >
                          </span>
                          <span class="each-product-title-text"
                            >Dependent Visa 1</span
                          >
                        </div>
                        <div class="each-product-desc full-width text-justify">
                          Lorem ipsum dolor sit, amet consectetur adipisicing
                          elit. Necessitatibus eius aspernatur officia quisquam
                          porro quaerat dignissimos rem iste esse amet sint
                          facilis placeat, aliquid neque doloremque eos laborum
                          ipsa nobis.
                        </div>
                        <div class="each-product-action full-width text-center">
                          <span class="each-product-btn">
                            Get Visa
                          </span>
                        </div>
                      </a>

                      <div class="each-product-tags-list full-width">
                        <a href="#" class="each-tag">spouse</a>
                        <a href="#" class="each-tag">parents</a>
                        <a href="#" class="each-tag">+2 more</a>
                      </div>
                    </div>
                  </li>
                  <li class="col-12 col-md-6 col-lg-3">
                    <div class="each-product move-product">
                      <a
                        href="productDetails.aspx"
                        class="full-width each-product-link-block"
                      >
                        <div class="each-product-title-block full-width">
                          <span
                            class="each-product-image full-width"
                            style="background-image: url(images/visa-1.jpg);"
                          >
                          </span>
                          <span class="each-product-title-text"
                            >Dependent Visa 2</span
                          >
                        </div>
                        <div class="each-product-desc full-width text-justify">
                          Lorem ipsum dolor sit, amet consectetur adipisicing
                          elit. Necessitatibus eius aspernatur officia quisquam
                          porro quaerat dignissimos rem iste esse amet sint
                          facilis placeat, aliquid neque doloremque eos laborum
                          ipsa nobis.
                        </div>
                        <div class="each-product-action full-width text-center">
                          <span class="each-product-btn">
                            Get Visa
                          </span>
                        </div>
                      </a>

                      <div class="each-product-tags-list full-width">
                        <a href="#" class="each-tag">parents</a>
                        <a href="#" class="each-tag">spouse</a>
                      </div>
                    </div>
                  </li>
                  <li class="col-12 col-md-6 col-lg-3">
                    <div class="each-product move-product">
                      <a
                        href="productDetails.aspx"
                        class="full-width each-product-link-block"
                      >
                        <div class="each-product-title-block full-width">
                          <span
                            class="each-product-image full-width"
                            style="background-image: url(images/visa-1.jpg);"
                          >
                          </span>
                          <span class="each-product-title-text"
                            >Dependent Visa 3</span
                          >
                        </div>
                        <div class="each-product-desc full-width text-justify">
                          Lorem ipsum dolor sit, amet consectetur adipisicing
                          elit. Necessitatibus eius aspernatur officia quisquam
                          porro quaerat dignissimos rem iste esse amet sint
                          facilis placeat, aliquid neque doloremque eos laborum
                          ipsa nobis.
                        </div>
                        <div class="each-product-action full-width text-center">
                          <span class="each-product-btn">
                            Get Visa
                          </span>
                        </div>
                      </a>

                      <div class="each-product-tags-list full-width">
                        <a href="#" class="each-tag">spouse</a>
                        <a href="#" class="each-tag">parents</a>
                      </div>
                    </div>
                  </li>
                  <li class="col-12 col-md-6 col-lg-3">
                    <div class="each-product move-product">
                      <a
                        href="productDetails.aspx"
                        class="full-width each-product-link-block"
                      >
                        <div class="each-product-title-block full-width">
                          <span
                            class="each-product-image full-width"
                            style="background-image: url(images/visa-1.jpg);"
                          >
                          </span>
                          <span class="each-product-title-text"
                            >Dependent Visa 4</span
                          >
                        </div>
                        <div class="each-product-desc full-width text-justify">
                          Lorem ipsum dolor sit, amet consectetur adipisicing
                          elit. Necessitatibus eius aspernatur officia quisquam
                          porro quaerat dignissimos rem iste esse amet sint
                          facilis placeat, aliquid neque doloremque eos laborum
                          ipsa nobis.
                        </div>
                        <div class="each-product-action full-width text-center">
                          <span class="each-product-btn">
                            Get Visa
                          </span>
                        </div>
                      </a>

                      <div class="each-product-tags-list full-width">
                        <a href="#" class="each-tag">spouse</a>
                        <a href="#" class="each-tag">parents</a>
                        <a href="#" class="each-tag">+2 more</a>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>--%>
            </div>
          </div>
        </div>
        <!-- Recently added Dependent Visas Ends -->

        <!-- Dependent Visas for Spouse Starts -->
        <div class="row">
          <div class="col-12 pt-1">
            <div class="full-width each-home-section dependent-visas-2">
              <div class="full-width each-home-section-heading pb-2">
                <h3 class="heading-lg">Dependent Visas for Spouse</h3>
                <a href="products.aspx" class="view-all-links">(View All)</a>
              </div>
                <asp:DataList ID="dlSpouseVisas" runat="server" RepeatColumns="4">
                    <ItemTemplate>
                        <div class="each-home-section-list-block full-width">
                <ul class="each-home-section-list row align-items-stretch">
                  <li  class="col-12 col-md-6 col-lg-11">
                    <div class="each-product move-product">
                      <a
                        href="productDetails.aspx"
                        class="full-width each-product-link-block"
                      >
                        <div class="each-product-title-block full-width">
                          <span
                            class="each-product-image full-width"
                            style="background-image: url(images/visa-1.jpg);"
                          >
                          </span>
                          <span class="each-product-title-text"
                            ><%#Eval("Name")%></span
                          >
                        </div>
                        <div class="each-product-desc full-width text-justify">
                          <%#Eval("Description")%>
                        </div>
                        <div class="each-product-action full-width text-center">
                          <span class="each-product-btn">
                            Get Visa
                          </span>
                        </div>
                      </a>

                      <div class="each-product-tags-list full-width">
                        <a href="#" class="each-tag">spouse</a>
                        <a href="#" class="each-tag">parents</a>
                        <a href="#" class="each-tag">+2 more</a>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
                    </ItemTemplate>
                </asp:DataList>
              <%--<div class="each-home-section-list-block full-width">
                <ul class="each-home-section-list row align-items-stretch">
                  <li class="col-12 col-md-6 col-lg-3">
                    <div class="each-product move-product">
                      <a
                        href="productDetails.aspx"
                        class="full-width each-product-link-block"
                      >
                        <div class="each-product-title-block full-width">
                          <span
                            class="each-product-image full-width"
                            style="background-image: url(images/visa-1.jpg);"
                          >
                          </span>
                          <span class="each-product-title-text"
                            >Dependent Visa 1</span
                          >
                        </div>
                        <div class="each-product-desc full-width text-justify">
                          Lorem ipsum dolor sit, amet consectetur adipisicing
                          elit. Necessitatibus eius aspernatur officia quisquam
                          porro quaerat dignissimos rem iste esse amet sint
                          facilis placeat, aliquid neque doloremque eos laborum
                          ipsa nobis.
                        </div>
                        <div class="each-product-action full-width text-center">
                          <span class="each-product-btn">
                            Get Visa
                          </span>
                        </div>
                      </a>

                      <div class="each-product-tags-list full-width">
                        <a href="#" class="each-tag">spouse</a>
                        <a href="#" class="each-tag">parents</a>
                        <a href="#" class="each-tag">+2 more</a>
                      </div>
                    </div>
                  </li>
                  <li class="col-12 col-md-6 col-lg-3">
                    <div class="each-product move-product">
                      <a
                        href="productDetails.aspx"
                        class="full-width each-product-link-block"
                      >
                        <div class="each-product-title-block full-width">
                          <span
                            class="each-product-image full-width"
                            style="background-image: url(images/visa-1.jpg);"
                          >
                          </span>
                          <span class="each-product-title-text"
                            >Dependent Visa 2</span
                          >
                        </div>
                        <div class="each-product-desc full-width text-justify">
                          Lorem ipsum dolor sit, amet consectetur adipisicing
                          elit. Necessitatibus eius aspernatur officia quisquam
                          porro quaerat dignissimos rem iste esse amet sint
                          facilis placeat, aliquid neque doloremque eos laborum
                          ipsa nobis.
                        </div>
                        <div class="each-product-action full-width text-center">
                          <span class="each-product-btn">
                            Get Visa
                          </span>
                        </div>
                      </a>

                      <div class="each-product-tags-list full-width">
                        <a href="#" class="each-tag">parents</a>
                        <a href="#" class="each-tag">spouse</a>
                      </div>
                    </div>
                  </li>
                  <li class="col-12 col-md-6 col-lg-3">
                    <div class="each-product move-product">
                      <a
                        href="productDetails.aspx"
                        class="full-width each-product-link-block"
                      >
                        <div class="each-product-title-block full-width">
                          <span
                            class="each-product-image full-width"
                            style="background-image: url(images/visa-1.jpg);"
                          >
                          </span>
                          <span class="each-product-title-text"
                            >Dependent Visa 3</span
                          >
                        </div>
                        <div class="each-product-desc full-width text-justify">
                          Lorem ipsum dolor sit, amet consectetur adipisicing
                          elit. Necessitatibus eius aspernatur officia quisquam
                          porro quaerat dignissimos rem iste esse amet sint
                          facilis placeat, aliquid neque doloremque eos laborum
                          ipsa nobis.
                        </div>
                        <div class="each-product-action full-width text-center">
                          <span class="each-product-btn">
                            Get Visa
                          </span>
                        </div>
                      </a>

                      <div class="each-product-tags-list full-width">
                        <a href="#" class="each-tag">spouse</a>
                        <a href="#" class="each-tag">parents</a>
                      </div>
                    </div>
                  </li>
                  <li class="col-12 col-md-6 col-lg-3">
                    <div class="each-product move-product">
                      <a
                        href="productDetails.aspx"
                        class="full-width each-product-link-block"
                      >
                        <div class="each-product-title-block full-width">
                          <span
                            class="each-product-image full-width"
                            style="background-image: url(images/visa-1.jpg);"
                          >
                          </span>
                          <span class="each-product-title-text"
                            >Dependent Visa 4</span
                          >
                        </div>
                        <div class="each-product-desc full-width text-justify">
                          Lorem ipsum dolor sit, amet consectetur adipisicing
                          elit. Necessitatibus eius aspernatur officia quisquam
                          porro quaerat dignissimos rem iste esse amet sint
                          facilis placeat, aliquid neque doloremque eos laborum
                          ipsa nobis.
                        </div>
                        <div class="each-product-action full-width text-center">
                          <span class="each-product-btn">
                            Get Visa
                          </span>
                        </div>
                      </a>

                      <div class="each-product-tags-list full-width">
                        <a href="#" class="each-tag">spouse</a>
                        <a href="#" class="each-tag">parents</a>
                        <a href="#" class="each-tag">+2 more</a>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>--%>
            </div>
          </div>
        </div>
        <!-- Dependent Visas for Spouse Ends -->

        <!-- Dependent Visas for Kids Starts -->
        <div class="row">
          <div class="col-12 pt-1">
            <div class="full-width each-home-section dependent-visas-3">
              <div class="full-width each-home-section-heading pb-2">
                <h4 class="heading-lg">Dependent Visas for Kids</h4>
                <a href="products.aspx" class="view-all-links">(View All)</a>
              </div>
                <asp:DataList ID="dlKidsVisa" runat="server" RepeatColumns="4">
                    <ItemTemplate>
                        <div class="each-home-section-list-block full-width">
                <ul class="each-home-section-list row align-items-stretch">
                  <li  class="col-12 col-md-6 col-lg-11">
                    <div class="each-product move-product">
                      <a
                        href="productDetails.aspx"
                        class="full-width each-product-link-block"
                      >
                        <div class="each-product-title-block full-width">
                          <span
                            class="each-product-image full-width"
                            style="background-image: url(images/visa-1.jpg);"
                          >
                          </span>
                          <span class="each-product-title-text"
                            ><%#Eval("Name")%></span
                          >
                        </div>
                        <div class="each-product-desc full-width text-justify">
                          <%#Eval("Description")%>
                        </div>
                        <div class="each-product-action full-width text-center">
                          <span class="each-product-btn">
                            Get Visa
                          </span>
                        </div>
                      </a>

                      <div class="each-product-tags-list full-width">
                        <a href="#" class="each-tag">spouse</a>
                        <a href="#" class="each-tag">parents</a>
                        <a href="#" class="each-tag">+2 more</a>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
                    </ItemTemplate>
                </asp:DataList>
              <%--<div class="each-home-section-list-block full-width">
                <ul class="each-home-section-list row align-items-stretch">
                  <li class="col-12 col-md-6 col-lg-3">
                    <div class="each-product move-product">
                      <a
                        href="productDetails.aspx"
                        class="full-width each-product-link-block"
                      >
                        <div class="each-product-title-block full-width">
                          <span
                            class="each-product-image full-width"
                            style="background-image: url(images/visa-1.jpg);"
                          >
                          </span>
                          <span class="each-product-title-text"
                            >Dependent Visa 1</span
                          >
                        </div>
                        <div class="each-product-desc full-width text-justify">
                          Lorem ipsum dolor sit, amet consectetur adipisicing
                          elit. Necessitatibus eius aspernatur officia quisquam
                          porro quaerat dignissimos rem iste esse amet sint
                          facilis placeat, aliquid neque doloremque eos laborum
                          ipsa nobis.
                        </div>
                        <div class="each-product-action full-width text-center">
                          <span class="each-product-btn">
                            Get Visa
                          </span>
                        </div>
                      </a>

                      <div class="each-product-tags-list full-width">
                        <a href="#" class="each-tag">spouse</a>
                        <a href="#" class="each-tag">parents</a>
                        <a href="#" class="each-tag">+2 more</a>
                      </div>
                    </div>
                  </li>
                  <li class="col-12 col-md-6 col-lg-3">
                    <div class="each-product move-product">
                      <a
                        href="productDetails.aspx"
                        class="full-width each-product-link-block"
                      >
                        <div class="each-product-title-block full-width">
                          <span
                            class="each-product-image full-width"
                            style="background-image: url(images/visa-1.jpg);"
                          >
                          </span>
                          <span class="each-product-title-text"
                            >Dependent Visa 2</span
                          >
                        </div>
                        <div class="each-product-desc full-width text-justify">
                          Lorem ipsum dolor sit, amet consectetur adipisicing
                          elit. Necessitatibus eius aspernatur officia quisquam
                          porro quaerat dignissimos rem iste esse amet sint
                          facilis placeat, aliquid neque doloremque eos laborum
                          ipsa nobis.
                        </div>
                        <div class="each-product-action full-width text-center">
                          <span class="each-product-btn">
                            Get Visa
                          </span>
                        </div>
                      </a>

                      <div class="each-product-tags-list full-width">
                        <a href="#" class="each-tag">parents</a>
                        <a href="#" class="each-tag">spouse</a>
                      </div>
                    </div>
                  </li>
                  <li class="col-12 col-md-6 col-lg-3">
                    <div class="each-product move-product">
                      <a
                        href="productDetails.aspx"
                        class="full-width each-product-link-block"
                      >
                        <div class="each-product-title-block full-width">
                          <span
                            class="each-product-image full-width"
                            style="background-image: url(images/visa-1.jpg);"
                          >
                          </span>
                          <span class="each-product-title-text"
                            >Dependent Visa 3</span
                          >
                        </div>
                        <div class="each-product-desc full-width text-justify">
                          Lorem ipsum dolor sit, amet consectetur adipisicing
                          elit. Necessitatibus eius aspernatur officia quisquam
                          porro quaerat dignissimos rem iste esse amet sint
                          facilis placeat, aliquid neque doloremque eos laborum
                          ipsa nobis.
                        </div>
                        <div class="each-product-action full-width text-center">
                          <span class="each-product-btn">
                            Get Visa
                          </span>
                        </div>
                      </a>

                      <div class="each-product-tags-list full-width">
                        <a href="#" class="each-tag">spouse</a>
                        <a href="#" class="each-tag">parents</a>
                      </div>
                    </div>
                  </li>
                  <li class="col-12 col-md-6 col-lg-3">
                    <div class="each-product move-product">
                      <a
                        href="productDetails.aspx"
                        class="full-width each-product-link-block"
                      >
                        <div class="each-product-title-block full-width">
                          <span
                            class="each-product-image full-width"
                            style="background-image: url(images/visa-1.jpg);"
                          >
                          </span>
                          <span class="each-product-title-text"
                            >Dependent Visa 4</span
                          >
                        </div>
                        <div class="each-product-desc full-width text-justify">
                          Lorem ipsum dolor sit, amet consectetur adipisicing
                          elit. Necessitatibus eius aspernatur officia quisquam
                          porro quaerat dignissimos rem iste esse amet sint
                          facilis placeat, aliquid neque doloremque eos laborum
                          ipsa nobis.
                        </div>
                        <div class="each-product-action full-width text-center">
                          <span class="each-product-btn">
                            Get Visa
                          </span>
                        </div>
                      </a>

                      <div class="each-product-tags-list full-width">
                        <a href="#" class="each-tag">spouse</a>
                        <a href="#" class="each-tag">parents</a>
                        <a href="#" class="each-tag">+2 more</a>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>--%>
            </div>
          </div>
        </div>
        <!-- Dependent Visas for Kids Ends -->
      </div>
    </section>
    </div>
</asp:Content>
