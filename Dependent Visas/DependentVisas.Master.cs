﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static Dependent_Visas.Translayer;

namespace Dependent_Visas
{
    public partial class DependentVisas1 : System.Web.UI.MasterPage
    {
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CartCount();
            }
        }
        //public void CartCount()
        //{
        //    CartDetails objCart = new CartDetails();
        //    objCart.Operation = "Get";
        //    objCart.RegistrationId = Convert.ToInt32(Session["UserId"]);
        //    dt = BLL.GetDetails(objCart);
        //    if (dt.Rows.Count > 0)
        //        lblcount.Text = Convert.ToString(dt.Rows[0]["Count"]);
        //    else
        //        lblcount.Text = "0";
        //}
        public void CartCount()
        {
            CartDetails objCart = new CartDetails();
            objCart.Operation = "Get";
            objCart.RegistrationId = Convert.ToInt32(Session["UserId"]);
            ds = BLL.GetDetails(objCart);
            if (ds.Tables[0].Rows.Count > 0)
                lblcount.Text = Convert.ToString(ds.Tables[0].Rows[0]["Count"]);
            else
                lblcount.Text = "0";
        }
    }
}