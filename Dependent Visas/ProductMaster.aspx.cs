﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static Dependent_Visas.Translayer;

namespace Dependent_Visas
{
    public partial class ProductMaster : System.Web.UI.Page
    {
        DataTable dt = new DataTable();
        bool bFileUpload2; bool bFileUpload1;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                LoadCountry();
                btnSubmit.Visible = true;
                btnUpdate.Visible = false;
                if (!IsPostBack)
                {
                    if (Request.QueryString["id"] != null)
                    {
                        LoadDetails();
                        btnSubmit.Visible = false;
                        btnUpdate.Visible = true;
                    }
                }
            }
            catch (Exeception ex)
            {

            }
        }

        public void LoadCountry()
        {
            try
            {
                dt = BLL.CountryCode();
                if (dt.Rows.Count > 0)
                {
                    ddlCountry.DataTextField = "name";
                    ddlCountry.DataValueField = "id";
                    //dt.Rows.InsertAt(0,"");
                    ddlCountry.DataSource = dt;
                    ddlCountry.DataBind();
                }
            }
            catch (Exeception ex)
            {

            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtName.Text != string.Empty || txtAmount.Text != string.Empty)
                {
                    Product objProduct = new Product();
                    objProduct.Name = txtName.Text.Substring(0, 1).ToUpper() + txtName.Text.Substring(1);
                    if (objProduct.Name != string.Empty)
                    {
                        objProduct.Action = "GetDetails";
                        // objProduct.Name = txtName.Text;
                        DataTable dt = BLL.GetDetails(objProduct);
                        if (dt.Rows.Count > 0)
                        {
                            Response.Write("<script language=javascript>alert('Product Name already exists')</script>");
                            return;
                        }
                        else
                        {
                            objProduct.Action = "Insert";
                            objProduct.Description = txtDescription.Text.Replace(Environment.NewLine, "<br />");
                            objProduct.Benefits = txtBenefits.Text.Replace(Environment.NewLine, "<br />");
                            objProduct.AboutDependentVisas = txtAbout.Text.Replace(Environment.NewLine, "<br />");
                            objProduct.Eligibility = txtEligibility.Text.Replace(Environment.NewLine, "<br />");
                            objProduct.Requirements = txtRequirements.Text.Replace(Environment.NewLine, "<br />");
                            // objProduct.Processflow=
                            string filename = "";
                            if (fileupload1.HasFile)
                            {
                                //if (bFileUpload1 == true)
                                //{
                                filename = Path.GetFileName(fileupload1.FileName);
                                var path = Path.Combine(Server.MapPath("~/Uploads/"), filename);
                                fileupload1.SaveAs(path);
                                objProduct.Processflow = filename;
                                //}
                            }
                            else
                                objProduct.Processflow = imgProcess.ImageUrl.Replace("~/Uploads/", "");
                            objProduct.DependencyType = Convert.ToInt32(ddlVisatype.SelectedValue);
                            objProduct.status = 1;
                            objProduct.Amount = Convert.ToDecimal(txtAmount.Text);
                            objProduct.CountryId = Convert.ToInt32(ddlCountry.SelectedValue);
                            string file = "";
                            if (fileupload2.HasFile)
                            {
                                //if (bFileUpload2 == true)
                                //{
                                file = Path.GetFileName(fileupload2.FileName);
                                var path = Path.Combine(Server.MapPath("~/Uploads/"), filename);
                                fileupload2.SaveAs(path);
                                objProduct.ImageUrl = file;
                                //}
                            }
                            else
                                objProduct.ImageUrl = imgProductimage.ImageUrl.Replace("~/Uploads/", "");
                            //Image1.ImageUrl = file;
                            bool status = Convert.ToBoolean(BLL.Products(objProduct));
                            if (status == true)
                            {
                                Response.Write("<script language=javascript>alert('Product Details Saved')</script>");
                                clearcontrols();
                            }

                            else
                                Response.Write("<script language=javascript>alert('Product Details Not Saved')</script>");
                        }
                    }
                }
            }
            catch (Exeception ex)
            {

            }
        }

        public void clearcontrols()
        {
            txtName.Text = string.Empty;
            txtDescription.Text = string.Empty;
            txtBenefits.Text = string.Empty;
            txtAbout.Text = string.Empty;
            txtEligibility.Text = string.Empty;
            txtRequirements.Text = string.Empty;
            ddlVisatype.SelectedIndex = 0;
            ddlCountry.SelectedIndex = 0;
            txtAmount.Text = string.Empty;
            imgProductimage.ImageUrl = "";
            imgProcess.ImageUrl = "";
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            clearcontrols();
        }
        public void LoadDetails()
        {
            try
            {
                if (Request.QueryString["id"] != null)
                {
                    Product objproduct = new Product();
                    BLL.StrAction = "SelectIndividual";
                    BLL.IProductID = Convert.ToInt32(Request.QueryString["id"]);
                    DataTable dt = BLL.GetProductDetails();
                    if (dt.Rows.Count > 0)
                    {
                        txtName.Text = Convert.ToString(dt.Rows[0]["Name"]);
                        txtDescription.Text = Convert.ToString(dt.Rows[0]["Description"]).Replace("<br />", Environment.NewLine);
                        txtBenefits.Text = Convert.ToString(dt.Rows[0]["Benefits"]).Replace("<br />", Environment.NewLine);
                        txtAbout.Text = Convert.ToString(dt.Rows[0]["AboutDependentVisas"]).Replace("<br />", Environment.NewLine);
                        txtEligibility.Text = Convert.ToString(dt.Rows[0]["Eligibility"]).Replace("<br />", Environment.NewLine);
                        txtRequirements.Text = Convert.ToString(dt.Rows[0]["Requirements"]).Replace("<br />", Environment.NewLine);
                        ddlCountry.SelectedValue = Convert.ToString(dt.Rows[0]["CountryID"]);
                        imgProcess.ImageUrl = Convert.ToString(dt.Rows[0]["ProcessFlow"]);
                        imgProductimage.ImageUrl = Convert.ToString(dt.Rows[0]["ImageURL"]);
                        txtAmount.Text = Convert.ToString(dt.Rows[0]["Amount"]);
                        ddlVisatype.SelectedValue = Convert.ToString(dt.Rows[0]["DependencyType"]);
                    }
                }
            }
            catch (Exeception ex)
            {

            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["id"] != null)
                {
                    Product objProduct = new Product();
                    objProduct.ProductId = Convert.ToInt32(Request.QueryString["id"]);
                    objProduct.Action = "Update";
                    objProduct.Name = txtName.Text.Substring(0, 1).ToUpper() + txtName.Text.Substring(1);
                    objProduct.Description = txtDescription.Text.Replace(Environment.NewLine, "<br />");
                    objProduct.Benefits = txtBenefits.Text.Replace(Environment.NewLine, "<br />");
                    objProduct.AboutDependentVisas = txtAbout.Text.Replace(Environment.NewLine, "<br />");
                    objProduct.Eligibility = txtEligibility.Text.Replace(Environment.NewLine, "<br />");
                    objProduct.Requirements = txtRequirements.Text.Replace(Environment.NewLine, "<br />");
                    // objProduct.Processflow=
                    string filename = "";
                    if (fileupload1.HasFile)
                    {
                        filename = Path.GetFileName(fileupload1.FileName);
                        var path = Path.Combine(Server.MapPath("~/Uploads/"), filename);
                        fileupload1.SaveAs(path);
                        objProduct.Processflow = filename;
                    }
                    else
                        objProduct.Processflow = imgProcess.ImageUrl;
                    objProduct.Processflow = filename;
                    objProduct.DependencyType = Convert.ToInt32(ddlVisatype.SelectedValue);
                    objProduct.status = 1;
                    objProduct.Amount = Convert.ToDecimal(txtAmount.Text);
                    objProduct.CountryId = Convert.ToInt32(ddlCountry.SelectedValue);
                    string file = "";
                    if (fileupload2.HasFile)
                    {
                        file = Path.GetFileName(fileupload2.FileName);
                        var path = Path.Combine(Server.MapPath("~/Uploads/"), filename);
                        fileupload1.SaveAs(path);
                        objProduct.ImageUrl = file;
                    }
                    else
                        objProduct.ImageUrl = imgProductimage.ImageUrl;
                    //Image1.ImageUrl = file;
                    bool status = Convert.ToBoolean(BLL.Products(objProduct));
                    if (status == true)
                    {
                        Response.Write("<script language=javascript>alert('Product Details Updated')</script>");
                        clearcontrols();
                    }

                    else
                        Response.Write("<script language=javascript>alert('Product Details Not Updated')</script>");
                }
            }
            catch (Exeception ex)
            {

            }
        }

        //protected void CVfileupload_ServerValidate(object source, ServerValidateEventArgs args)
        //{
        //    System.Drawing.Image img = System.Drawing.Image.FromStream(fileupload1.PostedFile.InputStream);
        //    int height = img.Height;
        //    int width = img.Width;
        //    decimal size = Math.Round(((decimal)fileupload1.PostedFile.ContentLength / (decimal)1024), 2);
        //    if (size > 100)
        //    {
        //        CVfileupload.ErrorMessage = "File size must not exceed 100 KB.";
        //        args.IsValid = false;
        //    }
        //    if (height > 100 || width > 100)
        //    {
        //        CVfileupload.ErrorMessage = "Height and Width must not exceed 100px.";
        //        args.IsValid = false;
        //    }
        //}

        private string GetImageDimension()
        {
            System.IO.Stream stream = fileupload1.PostedFile.InputStream;
            System.Drawing.Image image = System.Drawing.Image.FromStream(stream);

            int height = image.Height;
            int width = image.Width;

            return "Height: " + height + "; Width: " + width;
        }

        //protected void CVfileupload_ServerValidate(object source, ServerValidateEventArgs args)
        //{
        //    if (fileupload2.HasFile)
        //    {
        //        System.Drawing.Image img = System.Drawing.Image.FromStream(fileupload2.PostedFile.InputStream);

        //        var height = img.Height;
        //        var width = img.Width;
        //        if (height <= 350 && width <= 350)
        //        {
        //            args.IsValid = true;
        //            btnSubmit.Enabled = true;
        //            bFileUpload2 = true;
        //        }
        //        else
        //        {
        //            args.IsValid = false;
        //            bFileUpload2 = false;
        //            Response.Write("<script language=javascript>alert('Uploaded Image file should not exceed height 350px and width 350px')</script>");
        //            return;
        //        }
        //    }
        //}

        //protected void cmfileupload_ServerValidate(object source, ServerValidateEventArgs args)
        //{
        //    if (fileupload1.HasFile)
        //    {
        //        System.Drawing.Image img = System.Drawing.Image.FromStream(fileupload1.PostedFile.InputStream);

        //        var height = img.Height;
        //        var width = img.Width;
        //        if (height <= 300 && width <= 700)
        //        {
        //            args.IsValid = true;
        //            btnSubmit.Enabled = true;
        //            bFileUpload1 = true;
        //        }
        //        else
        //        {
        //            args.IsValid = false;
        //            bFileUpload1 = false;
        //            Response.Write("<script language=javascript>alert('Uploaded Image file should not exceed height 300px and width 700px')</script>");
        //            return;
        //        }
        //    }
        //}

        protected void btnPreview_Click(object sender, EventArgs e)
        {
            if (fileupload2.HasFile)
            {
                System.Drawing.Image img = System.Drawing.Image.FromStream(fileupload2.PostedFile.InputStream);

                var height = img.Height;
                var width = img.Width;
                if (height <= 350 && width <= 350)
                {
                    //args.IsValid = true;
                    btnSubmit.Enabled = true;
                    bFileUpload2 = true;
                    string folderPath = Server.MapPath("~/Uploads/");
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    //Check whether Directory (Folder) exists.
                    if (!Directory.Exists(folderPath))
                    {
                        //If Directory (Folder) does not exists Create it.
                        Directory.CreateDirectory(folderPath);
                    }

                    //Save the File to the Directory (Folder).
                    fileupload2.SaveAs(folderPath + Path.GetFileName(fileupload2.FileName));

                    //Display the Picture in Image control.
                    imgProductimage.ImageUrl = "~/Uploads/" + Path.GetFileName(fileupload2.FileName);
                    btnRemove.Visible = true;
                }
                else
                {
                    // args.IsValid = false;
                    bFileUpload2 = false;
                    Response.Write("<script language=javascript>alert('Uploaded Image file should not exceed height 350px and width 350px')</script>");
                    return;
                }
            }
        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            imgProductimage.ImageUrl = "";
            //fileupload2.HasFile = "";
        }

        protected void btnPreviewProcess_Click(object sender, EventArgs e)
        {
            if (fileupload1.HasFile)
            {
                System.Drawing.Image img = System.Drawing.Image.FromStream(fileupload1.PostedFile.InputStream);

                var height = img.Height;
                var width = img.Width;
                if (height <= 300 && width <= 700)
                {
                    //args.IsValid = true;
                    btnSubmit.Enabled = true;
                    bFileUpload2 = true;
                    string folderPath = Server.MapPath("~/Uploads/");
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    //Check whether Directory (Folder) exists.
                    if (!Directory.Exists(folderPath))
                    {
                        //If Directory (Folder) does not exists Create it.
                        Directory.CreateDirectory(folderPath);
                    }

                    //Save the File to the Directory (Folder).
                    fileupload1.SaveAs(folderPath + Path.GetFileName(fileupload1.FileName));

                    //Display the Picture in Image control.
                    imgProcess.ImageUrl = "~/Uploads/" + Path.GetFileName(fileupload1.FileName);
                    btnRemoveProcess.Visible = true;
                }
                else
                {
                    // args.IsValid = false;
                    bFileUpload2 = false;
                    Response.Write("<script language=javascript>alert('Uploaded Image file should not exceed height 300px and width 700px')</script>");
                    return;
                }
            }
        }

        protected void btnRemoveProcess_Click(object sender, EventArgs e)
        {
            imgProcess.ImageUrl = "";
        }
    }
}