﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static Dependent_Visas.Translayer;

namespace Dependent_Visas
{
    public partial class ProductGrid : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                LoadGrid();
            }
           
        }

        public void LoadGrid()
        {
            Products objProducts = new Products();
            DataTable dt = BLL.GetProduct();
            dlProducts.DataSource = dt;
            dlProducts.DataBind();
        }

        protected void btnEdit_Command(object sender, CommandEventArgs e)
        {
            var cmd = e.CommandArgument;
            Response.Redirect("ProductMaster.aspx?id="+ cmd);
        }

        protected void btnDelete_Command(object sender, CommandEventArgs e)
        {
            var Del = e.CommandArgument;
            Product objProducts = new Product();
            objProducts.ProductId = Convert.ToInt32(Del);
            bool status = Convert.ToBoolean(BLL.DeleteProduct(objProducts));
            if(status==true)
                Response.Write("<script language=javascript>alert('Product Deleted')</script>");
            else
                Response.Write("<script language=javascript>alert('Product Not Deleted')</script>");
        }
    }
}