﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProductMaster.aspx.cs" Inherits="Dependent_Visas.ProductMaster" %>
 <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<!DOCTYPE html>

<%--<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
</body>
</html>--%>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CRM - Dashboard</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700,800" rel="stylesheet">
    <link rel="stylesheet" href="css/crm.css">
    <link rel="stylesheet" href="css/fonts/font-awesome-material-icons.css">
    <style>
        .branding-block-logo {
            background-image: url(images/opulentus-circle.png);
        }

        @media screen and (min-width: 600px) {
            .branding-block-logo {
                background-image: url(images/opulentus-logo.jpg);
            }
        }
    </style>
    <script src="js/jquery.min.js"></script>
    <%--  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<link href="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css"
    rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js"></script>
<link href="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
<script src="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js" type="text/javascript"></script>--%>
    <script>
        //$(function () {
        //    $('[id*=lbVisaType]').multiselect({
        //        includeSelectAllOption: true
        //    });
        //});

        function handleWindowClose() {
            if ((window.event.clientX < 0) || (window.event.clientY < 0)) {
                event.returnValue = "If you have made any changes to the fields without clicking the Save button, your changes will be lost.";
            }
        }
    </script>
    <script type="text/javascript" language="javascript">
        function controlEnter(obj, event) {
            var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            if (keyCode == 13) {
                __doPostBack(obj, '');
                //document.getElementById(obj).click();
                return false;
            }
            else {
                return true;
            }
        }

        $(document).ready(function () {
            var box;
            box = document.getElementById("box");
            box.addEventListener("dragenter", OnDragEnter, false);
            box.addEventListener("dragover", OnDragOver, false);
            box.addEventListener("drop", OnDrop, false);
        });
        $(document).ready(function () {
            var box;
            box = document.getElementById("box1");
            box.addEventListener("dragenter", OnDragEnter, false);
            box.addEventListener("dragover", OnDragOver, false);
            box.addEventListener("drop", OnDrop, false);
        });

        function OnDragEnter(e) {
            e.stopPropagation();
            e.preventDefault();
        }

        function OnDragOver(e) {
            e.stopPropagation();
            e.preventDefault();
        }

        function OnDrop(e) {
            e.stopPropagation();
            e.preventDefault();
            selectedFiles = e.dataTransfer.files;
            $("#box").text(selectedFiles.length + " file(s) selected for uploading!");
        }
    </script>

    <script type="text/javascript">
        $(function () {
            $('#<%=fileupload2.ClientID %>').change(function () {
                //alert("test");
                //var fileupload = document.getElementById("fileupload2");
                //alert(fileupload[0]);

                //for (var i = 0; i < fileUpload[0].files.length; i++) {
                //    CurrentSize = CurrentSize + fileUpload[0].files[i].size;
                //    alert(CurrentSize);
                //}
                //args.IsValid = CurrentSize < maxFileSize;
                //  }

                ////Check whether HTML5 is supported.
                //if (typeof (fileUpload.files) != "undefined") {
                //    //Initiate the FileReader object.
                //    var reader = new FileReader();
                //    //Read the contents of Image File.
                //    reader.readAsDataURL(fileUpload.files[0]);
                //    alert(fileUpload.files[0]);
                //    reader.onload = function (e) {
                //        //Initiate the JavaScript Image object.
                //        var image = new Image();

                //        //Set the Base64 string return from FileReader as source.
                //        image.src = e.target.result;

                //        //Validate the File Height and Width.
                //        image.onload = function () {
                //            var height = this.height;
                //            var width = this.width;
                //            alert(height);
                //            debugger;
                //            if (height > 100 || width > 100) {
                //                alert("Height and Width must not exceed 100px.");
                //                return false;
                //            }
                //            alert("Uploaded image has valid Height and Width.");
                //            return true;
                //        };

                //    }
                //}
                var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
                if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                    alert("Only '.jpeg','.jpg', '.png', '.gif', '.bmp' formats are allowed.");
                }

            })
        })
    </script>
    <script type="text/javascript">
        function upload(sender, args) {
            debugger;
            //alert("test");
            args.IsValid = true;
            var maxFileSize = 10 * 1024 * 1024; // 10MB
            var CurrentSize = 0; var height = 0;
            var fileUpload = $("[id$='fileupload2']");
            alert(fileUpload);
            for (var i = 0; i < fileUpload[0].files.length; i++) {
                CurrentSize = CurrentSize + fileUpload[0].files[i].size;
               // alert(CurrentSize);
                //height = height + fileUpload[0].files[i].height;
                //alert(height);
            };
            args.IsValid = CurrentSize < maxFileSize;
        }
    </script>
</head>

<body class="dashboard menu-small">
    <!--  has-sidebar-md -->
    
    <div class="outer-block">
        <header class="header full-width">
            <div class="branding-block">
                <a href="index.html" class="branding-block-logo background-contain"></a>
            </div>
            <div class="header-actions">
                <div class="full-width ">
                    <button class="desktop-menu-toggle d-none d-lg-block">
                        <i
                            class="material-icons">menu</i></button>
                    <nav class="header-nav-block ">
                        <ul class="header-nav-list full-width">
                            <li>
                                <div class="each-header-nav-icon-block dropdown">
                                    <button
                                        class="each-header-nav-icon dropdown-btn notifications-btn"
                                        data-toggle="tooltip" title="Notifications">
                                        <i class="material-icons">notifications</i>
                                    </button>
                                    <div
                                        class="each-header-nav-content dropdown-content notifications-content">
                                        <button class="dropdown-content-close">
                                            <i
                                                class="material-icons">close</i></button>
                                        <ul class="notifications-list max-height-300">
                                            <li>
                                                <a href="#" class="each-notification">
                                                    <div class="each-notificatoion-icon">
                                                        <i
                                                            class="material-icons">notifications</i>
                                                    </div>
                                                    <div class="each-notification-text">
                                                        Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="each-notification">
                                                    <div class="each-notificatoion-icon">
                                                        <i
                                                            class="material-icons">notifications</i>
                                                    </div>
                                                    <div class="each-notification-text">
                                                        Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="each-notification">
                                                    <div class="each-notificatoion-icon">
                                                        <i
                                                            class="material-icons">notifications</i>
                                                    </div>
                                                    <div class="each-notification-text">
                                                        Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="each-notification">
                                                    <div class="each-notificatoion-icon">
                                                        <i
                                                            class="material-icons">notifications</i>
                                                    </div>
                                                    <div class="each-notification-text">
                                                        Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="each-notification">
                                                    <div class="each-notificatoion-icon">
                                                        <i
                                                            class="material-icons">notifications</i>
                                                    </div>
                                                    <div class="each-notification-text">
                                                        Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="each-notification">
                                                    <div class="each-notificatoion-icon">
                                                        <i
                                                            class="material-icons">notifications</i>
                                                    </div>
                                                    <div class="each-notification-text">
                                                        Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="each-notification">
                                                    <div class="each-notificatoion-icon">
                                                        <i
                                                            class="material-icons">notifications</i>
                                                    </div>
                                                    <div class="each-notification-text">
                                                        Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="each-notification">
                                                    <div class="each-notificatoion-icon">
                                                        <i
                                                            class="material-icons">notifications</i>
                                                    </div>
                                                    <div class="each-notification-text">
                                                        Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="each-notification">
                                                    <div class="each-notificatoion-icon">
                                                        <i
                                                            class="material-icons">notifications</i>
                                                    </div>
                                                    <div class="each-notification-text">
                                                        Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="each-notification">
                                                    <div class="each-notificatoion-icon">
                                                        <i
                                                            class="material-icons">notifications</i>
                                                    </div>
                                                    <div class="each-notification-text">
                                                        Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                    </div>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="each-header-nav-icon-block dropdown">
                                    <button class="each-header-nav-icon dropdown-btn email-btn boun"
                                        data-toggle="tooltip" title="Emails">
                                        <i class="material-icons">email</i>
                                    </button>
                                    <div
                                        class="each-header-nav-content dropdown-content email-content">
                                        <button class="dropdown-content-close">
                                            <i
                                                class="material-icons">close</i></button>
                                        <ul class="notifications-list max-height-300">
                                            <li>
                                                <a href="#" class="each-notification">
                                                    <div class="each-notificatoion-icon">
                                                        <i
                                                            class="material-icons">email</i>
                                                    </div>
                                                    <div class="each-notification-text">
                                                        Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="each-notification">
                                                    <div class="each-notificatoion-icon">
                                                        <i
                                                            class="material-icons">email</i>
                                                    </div>
                                                    <div class="each-notification-text">
                                                        Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="each-notification">
                                                    <div class="each-notificatoion-icon">
                                                        <i
                                                            class="material-icons">email</i>
                                                    </div>
                                                    <div class="each-notification-text">
                                                        Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="each-notification">
                                                    <div class="each-notificatoion-icon">
                                                        <i
                                                            class="material-icons">email</i>
                                                    </div>
                                                    <div class="each-notification-text">
                                                        Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="each-notification">
                                                    <div class="each-notificatoion-icon">
                                                        <i
                                                            class="material-icons">email</i>
                                                    </div>
                                                    <div class="each-notification-text">
                                                        Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="each-notification">
                                                    <div class="each-notificatoion-icon">
                                                        <i
                                                            class="material-icons">email</i>
                                                    </div>
                                                    <div class="each-notification-text">
                                                        Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="each-notification">
                                                    <div class="each-notificatoion-icon">
                                                        <i
                                                            class="material-icons">email</i>
                                                    </div>
                                                    <div class="each-notification-text">
                                                        Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="each-notification">
                                                    <div class="each-notificatoion-icon">
                                                        <i
                                                            class="material-icons">email</i>
                                                    </div>
                                                    <div class="each-notification-text">
                                                        Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="each-notification">
                                                    <div class="each-notificatoion-icon">
                                                        <i
                                                            class="material-icons">email</i>
                                                    </div>
                                                    <div class="each-notification-text">
                                                        Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="each-notification">
                                                    <div class="each-notificatoion-icon">
                                                        <i
                                                            class="material-icons">email</i>
                                                    </div>
                                                    <div class="each-notification-text">
                                                        Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                    </div>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="each-header-nav-icon-block dropdown">
                                    <button class="each-header-nav-icon dropdown-btn schedule-btn"
                                        data-toggle="tooltip" title="Calls Details">
                                        <i class="material-icons">schedule</i>
                                    </button>
                                    <div
                                        class="each-header-nav-content dropdown-content pt-0 schedule-content">
                                        <button class="dropdown-content-close">
                                            <i
                                                class="material-icons">close</i></button>
                                        <ul class="notifications-list max-height-300">
                                            <li class="alert-success">
                                                <div class="row">
                                                    <div class="col-7 text-left">
                                                        Completed
                                                                                    Call Duration
                                                    </div>
                                                    <div class="col-5 text-right">
                                                        00:00:00
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="alert-info">
                                                <div class="row">
                                                    <div class="col-7 text-left">
                                                        Wrapup Call
                                                                                    Duration
                                                    </div>
                                                    <div class="col-5 text-right">
                                                        00:00:00
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="alert-danger">
                                                <div class="row">
                                                    <div class="col-7 text-left">
                                                        Failed Call
                                                                                    Duration
                                                    </div>
                                                    <div class="col-5 text-right">
                                                        00:00:00
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="alert-warning">
                                                <div class="row">
                                                    <div class="col-7 text-left">
                                                        Walkin
                                                                                    Duration
                                                    </div>
                                                    <div class="col-5 text-right">
                                                        00:00:00
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="bg-success text-white">
                                                <div class="row">
                                                    <div class="col-7 text-left">
                                                        Total
                                                                                    Duration
                                                    </div>
                                                    <div class="col-5 text-right">
                                                        00:00:00
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </nav>
                    <div class="header-profile-block">
                        <button class="header-profile-pic background-cover"
                            style="background-image: url(images/executive.jpg);">
                            S
                        </button>
                        <div class="header-profile-nav">
                            <ul class="header-profile-nav-list">
                                <li>
                                    <a href="#">My profile</a>
                                </li>
                                <li>
                                    <a href="#">My Settings</a>
                                </li>
                                <li>
                                    <a href="#">Logout</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <button class="mobile-menu-toggle-btn"><i class="mobile-menu-toggle-icon"></i></button>
                </div>
            </div>
        </header>
        <section class="full-width main-content">
            <nav class="left-sidebar">
                <div class="main-navigation full-width">
                    <ul class="main-navigation-list full-width">
                        <li>
                            <a href="index.html" class="each-link">
                                <div class="each-link-icon">
                                    <i class="material-icons">dashboard</i>
                                </div>
                                <div class="each-link-text">Dashboard</div>
                            </a>
                        </li>
                        <li>
                            <a href="profile.html" class="each-link">
                                <div class="each-link-icon">
                                    <i class="material-icons">face</i>
                                </div>
                                <div class="each-link-text">Profile</div>
                            </a>
                        </li>
                        <li>
                            <a href="form.html" class="each-link active">
                                <div class="each-link-icon">
                                    <i class="material-icons">info</i>
                                </div>
                                <div class="each-link-text">Form Page</div>
                            </a>
                        </li>
                        <li>
                            <a href="grid.html" class="each-link">
                                <div class="each-link-icon">
                                    <i class="material-icons">border_all</i>
                                </div>
                                <div class="each-link-text">Grid Page</div>
                            </a>
                        </li>
                        <li>
                            <a href="popup.html" class="each-link">
                                <div class="each-link-icon">
                                    <i class="material-icons">launch</i>
                                </div>
                                <div class="each-link-text">Popup Page</div>
                            </a>
                        </li>
                        <li>
                            <a href="push-notifications.html" class="each-link">
                                <div class="each-link-icon">
                                    <i class="material-icons">arrow_right_alt</i>
                                </div>
                                <div class="each-link-text">Push Notifications</div>
                            </a>
                        </li>
                        <li>
                            <a href="push-notifications.html" class="each-link">
                                <div class="each-link-icon">
                                    <i class="material-icons">format_align_right</i>
                                </div>
                                <div class="each-link-text">Right Sidebar</div>
                            </a>
                        </li>
                        <li>
                            <a href="order-detail.html" class="each-link">
                                <div class="each-link-icon">
                                    <i class="material-icons">assignment_turned_in</i>
                                </div>
                                <div class="each-link-text">Order Detail Page</div>
                            </a>
                        </li>
                        <li class="">
                            <div class="has-sub-nav-link-block">
                                <a href="push-notifications.html" class="each-link ">
                                    <div class="each-link-icon">
                                        <i class="material-icons">list</i>
                                    </div>
                                    <div class="each-link-text">Other UI Elements</div>
                                </a>
                                <button class="has-sub-nav-toggle-btn">
                                    <i class="has-sub-nav-toggle-btn-icon"></i>
                                </button>
                            </div>
                            <ul class="sub-nav">
                                <li>
                                    <a href="loading.html" class="each-link ">
                                        <div class="each-link-icon">
                                            <i class="material-icons">replay</i>
                                        </div>
                                        <div class="each-link-text">Loading</div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="">
                            <div class="has-sub-nav-link-block">
                                <a href="leads.html" class="each-link">
                                    <div class="each-link-icon">
                                        <i class="material-icons">scatter_plot</i>
                                    </div>
                                    <div class="each-link-text">Dummy</div>
                                </a>
                                <button class="has-sub-nav-toggle-btn">
                                    <i class="has-sub-nav-toggle-btn-icon"></i>
                                </button>
                            </div>
                            <ul class="sub-nav">
                                <li>
                                    <a href="#" class="each-link">
                                        <div class="each-link-icon">
                                            <i class="material-icons">equalizer</i>
                                        </div>
                                        <div class="each-link-text">Dummy Sub Link 1</div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="each-link">
                                        <div class="each-link-icon">
                                            <i class="material-icons">equalizer</i>
                                        </div>
                                        <div class="each-link-text">Dummy Sub Link 2</div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="each-link">
                                        <div class="each-link-icon">
                                            <i class="material-icons">equalizer</i>
                                        </div>
                                        <div class="each-link-text">Dummy Sub Link 3</div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="each-link">
                                        <div class="each-link-icon">
                                            <i class="material-icons">equalizer</i>
                                        </div>
                                        <div class="each-link-text">Dummy Sub Link 4</div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
            <div class="content">
                <div class="full-width bg-light position-relative h-100">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-md-8 offset-md-2 pt-2">
                                <div class="full-width shadow form-block bg-white">
                                    <div
                                        class="full-width font-size-md font-weight-bold py-15 border-bottom px-2">
                                        Add Visa
                                    </div>
                                    <form id="form1" runat="server">
                                         <ajax:ToolkitScriptManager ID="ScriptManager1" runat="server"/>
                                        <div class="full-width form-block px-2 py-15">
                                            <ul class="form-list list-unstyled full-width">
                                                <li class="full-width mb-1">
                                                    <label class="full-width" style="font-weight:bold">Product Name </label>
                                                    <div class="full-width">
                                                        <%--<input type="text" class="form-control">--%>
                                                        <asp:TextBox runat="server" ID="txtName" Width="650" ValidationGroup="Products"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" Text="Please enter Name" ForeColor="Red" ValidationGroup="Products" Display="Dynamic">*</asp:RequiredFieldValidator>
                                                    </div>
                                                </li>
                                                <li class="full-width mb-1">
                                                    <label class="full-width" style="font-weight:bold">Amount </label>
                                                    <div class="full-width">
                                                        <%--<input type="text" class="form-control">--%>
                                                        <asp:TextBox runat="server" ID="txtAmount" Width="200" ValidationGroup="Products"></asp:TextBox>
                                                        <asp:Label ID="lblamount" runat="server" Text=".00"></asp:Label>
                                                        <asp:RequiredFieldValidator ID="rfvAmount" runat="server" ControlToValidate="txtAmount" Text="Please enter Amount" ForeColor="Red" ValidationGroup="Products" Display="Dynamic">*</asp:RequiredFieldValidator>
                                                    </div>
                                                </li>
                                                <li class="full-width mb-1">
                                                    <label class="full-width" style="font-weight:bold">Product Image </label>
                                                    
                                                    <div class="full-width">
                                                        
                                                        <%-- <input type="email" class="form-control">--%>
                                                        <div id="box">Drag & Drop files from your machine on this box.</div>
                                                       <%-- <asp:UpdatePanel ID="upid" runat="server">
                                                            <ContentTemplate>--%>
                                                            <asp:FileUpload ID="fileupload2" runat="server" />
                                                        &nbsp;&nbsp;
                                                                 <%--</ContentTemplate>
                                                        </asp:UpdatePanel>--%>
                                                          <asp:Button ID="btnPreview" runat="server" OnClick="btnPreview_Click" Text="Preview" />
                                                        <asp:Button ID="btnRemove" runat="server" Text="Remove" OnClick="btnRemove_Click" Visible="false" />
                                                        <asp:Image ID="imgProductimage" runat="server" />
                                                        <%--<asp:CustomValidator ID="CVfileupload" ClientValidationFunction="upload" ForeColor="Red" runat="server" OnServerValidate="CVfileupload_ServerValidate" ControlToValidate="fileupload2" ValidateEmptyText="true" ValidationGroup="Products" Display="Dynamic" />--%>
                                                        <%-- <asp:ValidationSummary ID="vgFileUpload" runat="server" />--%>
                                                    </div>
                                                </li>
                                                <li class="full-width mb-1">
                                                    <label class="full-width" style="font-weight:bold">Country </label>
                                                    <div class="full-width">
                                                        <asp:DropDownList ID="ddlCountry" runat="server" Width="200" ValidationGroup="Products" Display="Dynamic"></asp:DropDownList>

                                                    </div>
                                                </li>
                                                <li class="full-width mb-1">
                                                    <label class="full-width" style="font-weight:bold">Visa For </label>
                                                    <div class="full-width" style="width: 100px;">
                                                        <%-- <asp:ListBox ID="lbVisaType" runat="server" Width="200" ValidationGroup="Products" Display="Dynamic" >
                                                            <asp:ListItem Value="0" Text="Select"></asp:ListItem>
                                                            <asp:ListItem Value="1" Text="Spouse"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="Kids"></asp:ListItem>
                                                            <asp:ListItem Value="3" Text="Parents"></asp:ListItem>
                                                        </asp:ListBox>--%>
                                                        <asp:DropDownList ID="ddlVisatype" runat="server" Width="200" ValidationGroup="Products" Display="Dynamic">
                                                            <asp:ListItem Value="0" Text="Select"></asp:ListItem>
                                                            <asp:ListItem Value="1" Text="Spouse"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="Kids"></asp:ListItem>
                                                            <asp:ListItem Value="3" Text="Parents"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="rfvVisatype" runat="server" ControlToValidate="txtDescription" Text="Please select Visa Type" InitialValue="0" ForeColor="Red" ValidationGroup="Products" Display="Dynamic">*</asp:RequiredFieldValidator>
                                                    </div>
                                                </li>
                                                <li class="full-width mb-1">
                                                    <label class="full-width" style="font-weight:bold">Description </label>
                                                    <div class="full-width">
                                                        <%--<input type="email" class="form-control" multiple>--%>
                                                        <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" Width="650" ValidationGroup="Products"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ControlToValidate="txtDescription" Text="Please enter Description" ForeColor="Red" ValidationGroup="Products" Display="Dynamic">*</asp:RequiredFieldValidator>
                                                    </div>
                                                </li>
                                                <li class="full-width mb-1">
                                                    <label class="full-width" style="font-weight:bold">Benefits </label>
                                                    <div class="full-width">
                                                        <%--<input type="email" class="form-control">--%>
                                                        <asp:TextBox ID="txtBenefits" runat="server" TextMode="MultiLine" Width="650" ValidationGroup="Products"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvbenefits" runat="server" ControlToValidate="txtBenefits" Text="Please enter Description" ForeColor="Red" ValidationGroup="Products" Display="Dynamic">*</asp:RequiredFieldValidator>
                                                    </div>
                                                </li>
                                                <li class="full-width mb-1">
                                                    <label class="full-width" style="font-weight:bold">About Dependent Visas </label>
                                                    <div class="full-width">
                                                        <%--<input type="email" class="form-control">--%>
                                                        <asp:TextBox ID="txtAbout" runat="server" TextMode="MultiLine" Width="650" ValidationGroup="Products"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvAbout" runat="server" ControlToValidate="txtAbout" Text="Please enter About Dependent Visas" ForeColor="Red" ValidationGroup="Products" Display="Dynamic">*</asp:RequiredFieldValidator>
                                                    </div>
                                                </li>
                                                <li class="full-width mb-1">
                                                    <label class="full-width" style="font-weight:bold">Who is Eligible? </label>
                                                    <div class="full-width">
                                                        <%-- <input type="email" class="form-control">--%>
                                                        <asp:TextBox ID="txtEligibility" runat="server" TextMode="MultiLine" Width="650" ValidationGroup="Products"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvEligibility" runat="server" ControlToValidate="txtEligibility" Text="Please enter Description" ForeColor="Red" ValidationGroup="Products" Display="Dynamic">*</asp:RequiredFieldValidator>
                                                    </div>
                                                </li>
                                                <li class="full-width mb-1">
                                                    <label class="full-width" style="font-weight:bold">How can I apply?</label>
                                                    <div class="full-width">
                                                        <asp:TextBox ID="txtRequirements" runat="server" TextMode="MultiLine" Width="650" ValidationGroup="Products"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvRequirements" runat="server" ControlToValidate="txtRequirements" Text="Please enter Requirements" ForeColor="Red" ValidationGroup="Products" Display="Dynamic">*</asp:RequiredFieldValidator>
                                                    </div>
                                                </li>

                                                <li class="full-width mb-1" >
                                                    <label class="full-width" style="font-weight:bold">ProcessFlow Image </label>
                                                    <div class="full-width">
                                                        
                                                        <%-- <input type="email" class="form-control">--%>
                                                        <div id="box1">Drag & Drop files from your machine on this box.</div>
                                                        <asp:FileUpload ID="fileupload1" runat="server" />
                                                        &nbsp;&nbsp;
                                                        <asp:Button ID="btnPreviewProcess" runat="server" Text="Preview" OnClick="btnPreviewProcess_Click" />
                                                        <asp:Button ID="btnRemoveProcess" runat="server" Text="Remove" OnClick="btnRemoveProcess_Click" Visible="false" />
                                                        <asp:Image ID="imgProcess" runat="server" />
                                                        <%--<asp:CustomValidator ID="cmfileupload" ClientValidationFunction="upload" ForeColor="Red" runat="server" OnServerValidate="cmfileupload_ServerValidate" ControlToValidate="fileupload1" ValidateEmptyText="true" ValidationGroup="Products" Display="Dynamic" />--%>
                                                    </div>
                                                </li>

                                                <li class="full-width mb-1 pt-1">
                                                    <div class="full-width">
                                                        <asp:Button runat="server" ID="btnCancel" Text="Cancel" OnClick="btnCancel_Click"
                                                            class="btn btn-outline-gray px-3 mr-1"></asp:Button>
                                                        <asp:Button runat="server" ID="btnSubmit" Text="Submit" OnClick="btnSubmit_Click" ValidationGroup="Products"
                                                            class="btn btn-primary px-3"></asp:Button>&nbsp;&nbsp;&nbsp;
                                                                             <asp:Button runat="server" ID="btnUpdate" Text="Update" OnClick="btnUpdate_Click" ValidationGroup="Products"
                                                                                 class="btn btn-primary px-3"></asp:Button>
                                                        <asp:ValidationSummary ID="Productdetails" runat="server" ValidationGroup="Products" />
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/common.js"></script>
    <script>
        $(document).ready(function () {
            $('input.datepicker').datepicker({
                dateFormat: 'yy-mm-dd',
                showButtonPanel: true,
                changeMonth: true,
                changeYear: true,
                defaultDate: +0,
                showAnim: "fold"
            });
        });
    </script>
</body>

</html>
