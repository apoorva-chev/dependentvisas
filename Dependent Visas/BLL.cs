﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using static Dependent_Visas.Translayer;

namespace Dependent_Visas
{

    public class BLL
    {
        static DAL Dobj = new DAL();
        static SqlCommand cmd;
        DataSet ds;
        DataTable dt = new DataTable();
        static string strAction;
        static int iCountryID, iDependencyID;
        static int iProductID;
        public static string StrAction
        {
            get
            {
                return strAction;
            }
            set
            {
                strAction = value;
            }
        }


        public static int ICountryID
        {
            get
            {
                return iCountryID;
            }

            set
            {
                iCountryID = value;
            }
        }

        public static int IDependencyID
        {
            get
            {
                return iDependencyID;
            }

            set
            {
                iDependencyID = value;
            }
        }

        public static int IProductID
        {
            get
            {
                return iProductID;
            }

            set
            {
                iProductID = value;
            }
        }

        public static int Registration(Registration objregistration)
        {
            int status = 0;
            cmd = new SqlCommand();
            cmd.CommandText = "USP_Registration";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Operation", "Insert");
            cmd.Parameters.AddWithValue("@RegisteredBy", objregistration.RegisteredBy);
            cmd.Parameters.AddWithValue("@RegisterUpload", objregistration.RegisterUpload);
            cmd.Parameters.AddWithValue("@Name", objregistration.Name);
            cmd.Parameters.AddWithValue("@Email", objregistration.Email);
            cmd.Parameters.AddWithValue("@ContactNo", objregistration.ContactNo);
            cmd.Parameters.AddWithValue("@Password", objregistration.Password);
            cmd.Parameters.AddWithValue("@TermsandConditions", objregistration.TermsandConditions);
            status = Dobj.ExecuteNonQuery(cmd);
            return status;
        }

        public static int Enquiry(Enquiry objenquiry)
        {
            int status = 0;
            cmd = new SqlCommand();
            cmd.CommandText = "USP_Enquiry";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Name", objenquiry.Name);
            cmd.Parameters.AddWithValue("@Email", objenquiry.Email);
            cmd.Parameters.AddWithValue("@ContactNo", objenquiry.ContactNo);
            status = Dobj.ExecuteNonQuery(cmd);
            return status;
        }

        public static DataTable Login(Registration objregistration)
        {
            DataTable dt = new DataTable();
            cmd = new SqlCommand();
            cmd.CommandText = "USP_Registration";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Operation", "Login");
            cmd.Parameters.AddWithValue("@Email", objregistration.Email);
            cmd.Parameters.AddWithValue("@Password", objregistration.Password);
            dt = Dobj.ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable Validate(Registration objregistration)
        {
            DataTable dt = new DataTable();
            cmd = new SqlCommand();
            cmd.CommandText = "USP_Registration";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Operation", "Validate");
            cmd.Parameters.AddWithValue("@Email", objregistration.Email);
            dt = Dobj.ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable CountryCode()
        {
            DataTable dt = new DataTable();
            cmd = new SqlCommand();
            cmd.CommandText = "USP_CountryCode";
            cmd.CommandType = CommandType.StoredProcedure;
            dt = Dobj.ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetProduct(Product objproduct)
        {
            DataTable dt = new DataTable();
            cmd = new SqlCommand();
            cmd.CommandText = "sp_ProductsDisplay";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", objproduct.Action);
            cmd.Parameters.AddWithValue("@CountryID", ICountryID);
            cmd.Parameters.AddWithValue("@DependencyType", IDependencyID);
            dt = Dobj.ExecuteDataTable(cmd);
            return dt;
        }

        public static int Products(Product objproducts)
        {
            int status = 0;
            cmd = new SqlCommand();
            cmd.CommandText = "USP_Products";
            cmd.CommandType = CommandType.StoredProcedure;
            if (objproducts.Action == "Insert")
            {
                cmd.Parameters.AddWithValue("@Operation", objproducts.Action);
                cmd.Parameters.AddWithValue("@Name", objproducts.Name);
                cmd.Parameters.AddWithValue("@Description", objproducts.Description);
                cmd.Parameters.AddWithValue("@Benefits", objproducts.Benefits);
                cmd.Parameters.AddWithValue("@AboutDependencyVisas", objproducts.AboutDependentVisas);
                cmd.Parameters.AddWithValue("@Eligibility", objproducts.Eligibility);
                cmd.Parameters.AddWithValue("@Requirements", objproducts.Requirements);
                cmd.Parameters.AddWithValue("@ProcessFlow", objproducts.Processflow);
                cmd.Parameters.AddWithValue("@DependencyType", objproducts.DependencyType);
                cmd.Parameters.AddWithValue("@Status", objproducts.status);
                cmd.Parameters.AddWithValue("@Amount", objproducts.Amount);
                cmd.Parameters.AddWithValue("@CountryId", objproducts.CountryId);
                cmd.Parameters.AddWithValue("@ImageUrl", objproducts.ImageUrl);
                cmd.Parameters.AddWithValue("@CreatedBy", objproducts.CreatedBy);
                status = Dobj.ExecuteNonQuery(cmd);
                return status;
            }
            else if (objproducts.Action == "Update")
            {
                cmd.Parameters.AddWithValue("@Operation", objproducts.Action);
                cmd.Parameters.AddWithValue("@Name", objproducts.Name);
                cmd.Parameters.AddWithValue("@Description", objproducts.Description);
                cmd.Parameters.AddWithValue("@Benefits", objproducts.Benefits);
                cmd.Parameters.AddWithValue("@AboutDependencyVisas", objproducts.AboutDependentVisas);
                cmd.Parameters.AddWithValue("@Eligibility", objproducts.Eligibility);
                cmd.Parameters.AddWithValue("@Requirements", objproducts.Requirements);
                cmd.Parameters.AddWithValue("@ProcessFlow", objproducts.Processflow);
                cmd.Parameters.AddWithValue("@DependencyType", objproducts.DependencyType);
                cmd.Parameters.AddWithValue("@Status", objproducts.status);
                cmd.Parameters.AddWithValue("@Amount", objproducts.Amount);
                cmd.Parameters.AddWithValue("@CountryId", objproducts.CountryId);
                cmd.Parameters.AddWithValue("@ImageUrl", objproducts.ImageUrl);
                cmd.Parameters.AddWithValue("@ModifiedBy", objproducts.ModifiedBy);
                cmd.Parameters.AddWithValue("@ProductId", objproducts.ProductId);
                status = Dobj.ExecuteNonQuery(cmd);
                return status;
            }
            return status;
        }

        public static DataTable GetProductDetails()
        {
            DataTable dt = new DataTable();
            cmd = new SqlCommand();
            cmd.CommandText = "sp_ProductDetails";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", StrAction);
            cmd.Parameters.AddWithValue("@ProductID", IProductID);
            dt = Dobj.ExecuteDataTable(cmd);
            return dt;
        }

        public static int CartDetails(CartDetails objcart)
        {
            int status = 0;
            cmd = new SqlCommand();
            cmd.CommandText = "USP_Cart";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Operation", objcart.Operation);
            cmd.Parameters.AddWithValue("@Name", objcart.Name);
            cmd.Parameters.AddWithValue("@Description", objcart.Description);
            cmd.Parameters.AddWithValue("@ProductId", objcart.ProductId);
            cmd.Parameters.AddWithValue("@Amount", objcart.Amount);
            cmd.Parameters.AddWithValue("@InvoiceNo", objcart.InvoiceNo);
            cmd.Parameters.AddWithValue("@RegistrationId", objcart.RegistrationId);
            cmd.Parameters.AddWithValue("@CreatedBy", objcart.CreatedBy);
            cmd.Parameters.AddWithValue("@ModifiedBy", objcart.ModifiedBy);
            status = Dobj.ExecuteNonQuery(cmd);
            return status;
        }

        public static DataSet GetDetails(CartDetails objcart)
        {
            // DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            cmd = new SqlCommand();
            cmd.CommandText = "USP_Cart";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Operation", objcart.Operation);
            cmd.Parameters.AddWithValue("@RegistrationId", objcart.RegistrationId);
            ds = Dobj.ExecuteDataSet(cmd);
            return ds;
        }

        public static DataTable GetInvoice(CartDetails objcart)
        {
            DataTable dt = new DataTable();
            cmd = new SqlCommand();
            cmd.CommandText = "USP_Cart";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Operation", objcart.Operation);
            dt = Dobj.ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetProduct()
        {
            DataTable dt = new DataTable();
            cmd = new SqlCommand();
            cmd.CommandText = "USP_Products";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Operation", "Get");
            dt = Dobj.ExecuteDataTable(cmd);
            return dt;
        }
        public static int DeleteProduct(Product objproduct)
        {
            int status = 0;
            DataTable dt = new DataTable();
            cmd = new SqlCommand();
            cmd.CommandText = "USP_Products";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Operation", "Delete");
            cmd.Parameters.AddWithValue("@ProductId", objproduct.ProductId);
            status = Dobj.ExecuteNonQuery(cmd);
            return status;
        }

        public static int Questions(Question qObj)
        {
            cmd = new SqlCommand();
            cmd.CommandText = "sp_QuestionAnswers";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", qObj.StrAction);
            cmd.Parameters.AddWithValue("@Question", qObj.QuestionName);
            cmd.Parameters.AddWithValue("@CreaterName", qObj.CreatedBy);
           // cmd.Parameters.AddWithValue("@CreatedDate", qObj.CreatedDate);
            cmd.Parameters.AddWithValue("@ProductID", qObj.ProductID);
            cmd.Parameters.AddWithValue("@IPAddress", qObj.IPAddress);
            int iCount = Dobj.ExecuteNonQuery(cmd);
            return iCount;


        }
        public static DataTable GetQuestions(Question qObj)
        {
            DataTable dt = new DataTable();
            cmd = new SqlCommand();
            cmd.CommandText = "sp_QuestionAnswers";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", qObj.StrAction);
            cmd.Parameters.AddWithValue("@ProductID", qObj.ProductID);
            cmd.Parameters.AddWithValue("@QuestionID", qObj.QuestionID);

            dt = Dobj.ExecuteDataTable(cmd);
            return dt;
        }

        public static int Rating(Rating rObj)
        {
            cmd = new SqlCommand();
            cmd.CommandText = "sp_Rating";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", rObj.StrAction);
            cmd.Parameters.AddWithValue("@RatingCount1", rObj.RatingCount1);
            cmd.Parameters.AddWithValue("@RatingCount2", rObj.RatingCount2);
            cmd.Parameters.AddWithValue("@RatingCount3", rObj.RatingCount3);
            cmd.Parameters.AddWithValue("@RatingCount4", rObj.RatingCount4);
            cmd.Parameters.AddWithValue("@RatingCount5", rObj.RatingCount5);
            cmd.Parameters.AddWithValue("@CreatedBy", rObj.CreatedBy);
            cmd.Parameters.AddWithValue("@ProductID", rObj.ProductID);
            cmd.Parameters.AddWithValue("@IpAddress", rObj.IPAddress);

               int iCount = Dobj.ExecuteNonQuery(cmd);
            return iCount;
        }

        public static DataTable GetRatings(Rating rObj)
        {
            DataTable dt = new DataTable();
       

            cmd = new SqlCommand();
            cmd.CommandText = "sp_Rating";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", rObj.StrAction);
            cmd.Parameters.AddWithValue("@ProductID", rObj.ProductID);
            dt = Dobj.ExecuteDataTable(cmd);
            //SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DependentVisas"].ToString());
            //string strQuery = "select CreatedBy,sum(RatingCount) as SumCount, count(RatingCount) as RecordCount from Rating Where ProductID = " + Translayer.Rating.ProductID + " group by CreatedBy";
            //SqlDataAdapter da = new SqlDataAdapter(strQuery, con);
            //da.Fill(dt);
            return dt;
        }

        public static int Deletecart(CartDetails objcart)
        {
            int status = 0;
            cmd = new SqlCommand();
            cmd.CommandText = "USP_Cart";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Operation", objcart.Operation);
            cmd.Parameters.AddWithValue("@RegistrationId", objcart.RegistrationId);
            cmd.Parameters.AddWithValue("@ProductId", objcart.ProductId);
            status = Dobj.ExecuteNonQuery(cmd);
            return status;
        }

        public static DataTable GetDetails(Product objproduct)
        {
            DataTable dt = new DataTable();
            cmd = new SqlCommand();
            cmd.CommandText = "USP_Products";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Operation", objproduct.Action);
            cmd.Parameters.AddWithValue("@Name", objproduct.Name);
            dt = Dobj.ExecuteDataTable(cmd);
            return dt;
        }
        public static int InsertMostViewes(MostViewing mObj)
        {
            DAL dObj = new DAL();
            cmd = new SqlCommand();
            cmd.CommandText = "sp_MostViewed";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", mObj.Action);
            cmd.Parameters.AddWithValue("@ProductID", mObj.ProductID);
            cmd.Parameters.AddWithValue("@ViewedBy", mObj.ViewedBy);
            cmd.Parameters.AddWithValue("@IPAddress", mObj.IPAddress);
            int iCount = dObj.ExecuteNonQuery(cmd);
            return iCount;
        }

        public static DataTable GetRelatedVisas(Product objproduct)
        {
            DataTable dt = new DataTable();
            cmd = new SqlCommand();
            cmd.CommandText = "USP_Products";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Operation", objproduct.Action);
            cmd.Parameters.AddWithValue("@CountryId", objproduct.CountryId);
            cmd.Parameters.AddWithValue("@DependencyType", objproduct.DependencyType);
            dt = Dobj.ExecuteDataTable(cmd);
            return dt;
        }
    }
}