﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="ProductDetails.aspx.cs" Inherits="Dependent_Visas.ProductDetails" %>
 <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Dependent Visas</title>
    <!-- PWA Resources Starts -->
    <meta name="theme-color" content="#1a248f" />
   <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

    <link
      rel="icon"
      sizes="192x192"
      href="images/icons/c634d504-b2c5-a929-eaea-0974e25a3fe5.webPlatform.png"
    />
    <link rel="manifest" href="manifest.json" />
    <!-- PWA Resources Starts -->
    <!-- <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <link rel="icon" href="favicon.ico" type="image/x-icon" /> -->
    <link
      href="https://fonts.googleapis.com/css?family=PT+Sans:400,700&display=swap"
      rel="stylesheet"
    />
    <link
      rel="stylesheet"
      href="https://cdn.materialdesignicons.com/4.7.95/css/materialdesignicons.min.css"
    />
    <!-- Editor styles -->
    <link rel="stylesheet" href="editor/css/froala_editor.css">
    <link rel="stylesheet" href="editor/css/froala_style.css">
    <link rel="stylesheet" href="editor/css/plugins/code_view.css">
    <link rel="stylesheet" href="editor/css/plugins/image.css">

    <link rel="stylesheet" href="css/dependent-visas.css" />
    <script src="js/jquery.min.js"></script>
      <script type="text/javascript">
    function Showalert() {
        alert('You want to close window');
        window.close();
    }
</script>
  </head>
  <body>
    <header class="full-width header border-bottom">
      <div class="container">
        <div class="row">
          <div class="col-5 col-md-2">
            <div class="full-width branding-block">
              <a
                href="index.html"
                class="branding d-block"
                title="Dependent Visas"
              >
                <img
                  src="images/dependent-visas-logo.svg"
                  alt="Depdent Visas"
                  title="Dependent Visas"
                />
              </a>
            </div>
          </div>
          <div class="col-7 col-md-10">
            <div class="full-width mobile-menu-btn-nav-block">
              <nav class="main-navigation-block">
                <ul class="main-navigation list-unstyled mb-0">
                  <li>
                    <a href="Home.aspx" class="each-nav-link">
                      Home
                    </a>
                  </li>
                  <li>
                    <a href="Products.aspx" class="each-nav-link active"
                      >Products</a
                    >
                  </li>
                  <li>
                    <a href="about-us.html" class="each-nav-link">About us</a>
                  </li>
                  <li>
                    <a href="faqs.html" class="each-nav-link">FAQs</a>
                  </li>
                  <li>
                    <a href="contact-us.html" class="each-nav-link"
                      >Contact us</a
                    >
                  </li>
                  <li class="login-register-block">
                    <a href="Login.aspx" class="login-register-btn"
                      >Login / Signup</a
                    >
                  </li>
                </ul>
              </nav>
              <a href="cart.aspx" class="header-cart-btn mr-1 mr-md-0 ml-md-1">
                <i class="mdi mdi-cart"></i>
                <span class="header-cart-count"><asp:Label ID="lblcount" runat="server"></asp:Label></span>
              </a>
              <button class="mobile-menu-toggle-btn">
                <i class="mobile-menu-toggle-icon"></i>
              </button>
            </div>
          </div>
        </div>
      </div>
    </header>
    <form name="frmProductDetails" runat="server">
        <ajax:ToolkitScriptManager ID="ScriptManager1" runat="server"/>
    <section class="full-width banner-block main-content bg-secondary">
      <div class="container">
        <div class="row align-items-stretch">
          <div class="col-12">
            <div
              class="full-width home-quick-enquiry-block text-white py-1 qe-block"
            >
              <div class="home-enquiry-title full-width heading-md text-center">
                <span>Quick Enquiry</span>
              </div>
              <ul class="home-enquiry-form full-width list-unstyled mb-0">
                <li class="qc-name">
                  <label class="full-width">Name:</label>
                  <input
                    type="text"
                    class="full-width form-control"
                    placeholder="Your Name" id="txtName" runat="server"
                  />
                </li>
                <li class="qc-email">
                  <label class="full-width">Email:</label>
                  <input
                    type="email"
                    class="full-width form-control"
                    placeholder="Your Email Id" id="txtEmail" runat="server"
                  />
                </li>
                <li class="qc-contact">
                  <label class="full-width">Contact Number:</label>
                  <div class="full-width">
                    <div class="row no-gutters">
                      <div class="col-4">
                        <select name="" id="ddlCountry" class="form-control" runat="server">
                          <option value="">+91 IND</option>
                          <option value="">+1234 ABCD</option>
                          <option value="">+91 IND</option>
                          <option value="">+1234 ABCD</option>
                        </select>
                      </div>
                      <div class="col-8 pl-1">
                        <input
                          type="tel" id="txtContact" runat="server"
                          class="form-control full-width"
                          placeholder="Your Mobile Number"
                        />
                      </div>
                    </div>
                  </div>
                </li>
                <li class="qc-actions text-right">
                  <label class="checkbox">
                    <input type="checkbox" class="checkbox-input" />
                    <span class="checkbox-text text-white">
                      I agree
                      <a href="#" class="text-white text-underline"
                        >Terms &amp; Conditions</a
                      >
                    </span>
                  </label>
                  <div class="text-right submit-enquiry-btn-block">
                    <asp:button ID="btnEnquiry" runat="server" OnClick="btnEnquiry_Click"
                      class="btn btn-primary px-3 border-white submit-enquiry-btn" Text="Submit Enquiry"
                    >
                    </asp:button>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="full-width">
      <div class="container">
        <div class="row align-items-stretch">
          <div class="col-12 col-lg-8">
            <div class="full-width product-detail">
              <!-- Bredcrum Navigation Starts -->
              <div class="full-width breadcrum-nav-block">
                <ul class="breadcrum-nav-list list-unstyled mb-0">
                  <li>
                    <a href="index.html" class="breadcrum-nav-link">Home</a>
                  </li>
                  <li>
                    <a href="products.html" class="breadcrum-nav-link"
                      >Products</a
                    >
                  </li>
                  <li>
                    Product Detail Name
                  </li>
                </ul>
              </div>
              <!-- Bredcrum Navigation Ends -->

              <!-- Product Detail Full Block Starts -->
              <div class="full-width product-detail-info-block">
                <div class="product-detail-basic full-width py-3">
                  <div class="row pb-2">
                    <div class="col-12 col-md-5 col-lg-4">
                      <div class="full-width product-image-block">
                       <%-- <img
                          src="images/product-detail.jpg"
                          alt=""
                          class="img-fluid border"
                        />--%>
                          <img id="imgProduct" runat="server" alt="ProuctImage" class="img-fluid border" />
                      </div>
                    </div>
                    <div class="col-12 col-md-7 col-lg-8">
                      <div class="full-width product-info">
                        <div class="full-width border-bottom pb-1">
                          <h1 class="full-width product-title heading-md">
                            <%--Australia--%>
                              <asp:Label ID="lblProductName" runat="server"></asp:Label>
                          </h1>
                          <div class="product-review-stars full-width">
                            <div class="product-stars-block">
                              <div class="product-stars with-arrow">
                                <i class="mdi mdi-star"></i>
                                <i class="mdi mdi-star"></i>
                                <i class="mdi mdi-star"></i>
                                <i class="mdi mdi-star-half"></i>
                                <i class="mdi mdi-star-outline"></i>
                              </div>
                              <div class="product-stars-percentage-info">
                                <div class="product-stars-block-main">
                                  <div
                                    class="product-stars-percentage-count full-width text-center"
                                  >
                                    3.5 out of 5
                                  </div>
                                  <div
                                    class="product-percentage-block full-width"
                                  >
                                    <div
                                      class="each-product-percentage full-width"
                                    >
                                      <div class="each-product-percentage-text">
                                        5 stars
                                      </div>
                                      <div class="each-product-percentage-fill">
                                        <span style="width: 70%;"></span>
                                      </div>
                                      <div
                                        class="each-product-percentage-count"
                                      >
                                        53%
                                      </div>
                                    </div>
                                    <div
                                      class="each-product-percentage full-width"
                                    >
                                      <div class="each-product-percentage-text">
                                        4 stars
                                      </div>
                                      <div class="each-product-percentage-fill">
                                        <span style="width: 60%;"></span>
                                      </div>
                                      <div
                                        class="each-product-percentage-count"
                                      >
                                        75%
                                      </div>
                                    </div>
                                    <div
                                      class="each-product-percentage full-width"
                                    >
                                      <div class="each-product-percentage-text">
                                        3 stars
                                      </div>
                                      <div class="each-product-percentage-fill">
                                        <span style="width: 60%;"></span>
                                      </div>
                                      <div
                                        class="each-product-percentage-count"
                                      >
                                        63%
                                      </div>
                                    </div>
                                    <div
                                      class="each-product-percentage full-width"
                                    >
                                      <div class="each-product-percentage-text">
                                        2 stars
                                      </div>
                                      <div class="each-product-percentage-fill">
                                        <span style="width: 60%;"></span>
                                      </div>
                                      <div
                                        class="each-product-percentage-count"
                                      >
                                        33%
                                      </div>
                                    </div>
                                    <div
                                      class="each-product-percentage full-width"
                                    >
                                      <div class="each-product-percentage-text">
                                        1 stars
                                      </div>
                                      <div class="each-product-percentage-fill">
                                        <span style="width: 60%;"></span>
                                      </div>
                                      <div
                                        class="each-product-percentage-count"
                                      >
                                        23%
                                      </div>
                                    </div>
                                  </div>
                                  <div
                                    class="product-see-all-reviews full-width text-center"
                                  >
                                    <a href="#">See all 615 reviews</a>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="product-start-reviews-faqs-count">
                              <a href="#" class="product-start-reviews-count"
                                >759 customers</a
                              ><span class="px-1 text-gray">|</span>
                              <a href="#" class="product-start-reviews-count"
                                >1000+ answered questions</a
                              >
                            </div>
                          </div>
                        </div>
                        <div class="full-width pt-15 product-detail-pricing">
                          <div class="orginal-price text-primary heading-sm">
                            <span class="font-size text-gray">Price:</span>
                            <span class="price-currency-symbol"><i class="mdi mdi-currency-inr"></i></span>
                           <%-- <span class="pricing-number">5,000.00</span>--%>
                              <asp:Label ID="lblPrice" runat="server"></asp:Label>
                          </div>
                        </div>
                        <div class="full-width product-detail-actions-block my-15">
                          <asp:button class="btn btn-primary px-4 heading-sm" id="btnCartdetails" runat="server" Text="Get Visa" OnClick="btnCartdetails_Click"></asp:button>
                        </div>
                        <div class="full-width product-small-info mb-1 pt-1">
                          <p> <strong>Description:</strong> <br>  
                           <%-- Lorem ipsum, dolor sit amet consectetur adipisicing
                          elit. Quae, dolorum recusandae voluptatum architecto
                          delectus maxime labore perspiciatis nemo.--%>
                        <asp:Label ID="lblDescription" runat="server"></asp:Label>
                          </p>
                        </div>
                        <div class="full-width pt-1">
                          <div class="full-width">
                            <strong>Benefits</strong>
                          </div>
                          <ul class="check-list list-unstyled mb-0 full-width">
                           <%-- <li>--%>
                              <%--Lorem ipsum dolor sit amet consectetur,
                              adipisicing elit. Porro.--%>
                              <asp:Label ID="lblBenefits" runat="server"></asp:Label>
                          <%--  </li>--%>
                          <%--  <li>
                              Praesentium explicabo adipisci iusto sapiente,
                              itaque, cumque voluptatum neque.
                            </li>--%>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <!-- About Depedent Visas Starts -->
                    <div class="col-12 pb-2">
                      <div class="full-width each-product-detail-row">
                        <h2 class="heading-sm font-weight-bold full-width pb-1">
                          About Dependent Visas
                        </h2>
                        <div class="full-width each-product-detail-description">
                          <p>
                          <%--  Lorem ipsum dolor sit amet consectetur, adipisicing
                            elit. Porro, in ex! Exercitationem illo totam neque
                            voluptatum mollitia. Praesentium explicabo adipisci
                            iusto sapiente, itaque, cumque voluptatum neque,
                            magnam voluptate tempora dignissimos?--%>
                             <asp:Label ID="lblDependentVisas" runat="server"></asp:Label>
                          </p>
                          <ul class="mb-0">
                           <%-- <li>--%>
                             <%-- Lorem ipsum dolor sit amet consectetur,
                              adipisicing elit.--%>
                               <%--<asp:Label ID="lblSplitData" runat="server"></asp:Label>
                            </li>--%>
                           <%-- <li>
                              Porro, in ex! Exercitationem illo totam neque
                              voluptatum mollitia.
                            </li>
                            <li>
                              Praesentium explicabo adipisci iusto sapiente,
                              itaque, cumque voluptatum neque, magnam voluptate
                              tempora dignissimos?
                            </li>--%>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <!-- About Depedent Visas Ends -->

                    <!-- Who are eligible Starts -->
                    <div class="col-12 pb-2">
                      <div class="full-width each-product-detail-row">
                        <h2 class="heading-sm font-weight-bold full-width pb-1">
                          Who are eligible
                        </h2>
                        <div class="full-width each-product-detail-description">
                          <ul class="check-list list-unstyled mb-0">
                            <%--<li>--%>
                             <%-- Lorem ipsum dolor sit amet consectetur,
                              adipisicing elit.--%>
                              <asp:Label ID="lblEligible" runat="server"></asp:Label>
                           <%-- </li>--%>
                          <%--  <li>
                              Porro, in ex! Exercitationem illo totam neque
                              voluptatum mollitia.
                            </li>
                            <li>
                              Praesentium explicabo adipisci iusto sapiente,
                              itaque, cumque voluptatum neque, magnam voluptate
                              tempora dignissimos?
                            </li>--%>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <!-- Who are eligible Ends -->

                    <!-- Requirements Starts -->
                    <div class="col-12 pb-2">
                      <div class="full-width each-product-detail-row">
                        <h2 class="heading-sm font-weight-bold full-width pb-1">
                          Requirements
                        </h2>
                        <div class="full-width each-product-detail-description">
                          <ul class="check-list list-unstyled mb-0">
                            <%--<li>--%>
                             <%-- Lorem ipsum dolor sit amet consectetur,
                              adipisicing elit.--%>
                                <asp:Label ID="lblRequirements" runat="server"></asp:Label>
                          <%--  </li>--%>
                           <%-- <li>
                              Porro, in ex! Exercitationem illo totam neque
                              voluptatum mollitia.
                            </li>
                            <li>
                              Praesentium explicabo adipisci iusto sapiente,
                              itaque, cumque voluptatum neque, magnam voluptate
                              tempora dignissimos?
                            </li>--%>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <!-- Requirements Ends -->

                    <!-- Process flow Starts -->
                    <div class="col-12 pb-2">
                      <div class="full-width each-product-detail-row">
                        <h2 class="heading-sm font-weight-bold full-width pb-1">
                          Process flow
                        </h2>
                        <div
                          class="full-width each-product-detail-description mt-3"
                        >
                          <div class="row align-items-stretch">
                            <div
                              class="col-12 col-md-3 mb-3 mb-md-0 pb-3 pb-md-0"
                            >
                              <div >
                               <%-- <span class="process-flow-each-step-number"
                                  >1</span
                                >--%>
                                <span>
                                    <img id="imgprocess" runat="server" />
                                </span>
                              </div>
                            </div>
                          <%--  <div
                              class="col-12 col-md-3 mb-3 mb-md-0 pb-3 pb-md-0"
                            >
                              <div class="process-flow-each-step">
                                <span class="process-flow-each-step-number"
                                  >2</span
                                >
                                <span class="process-folow-each-step-title"
                                  >Submit Requirements</span
                                >
                              </div>
                            </div>
                            <div
                              class="col-12 col-md-3 mb-3 mb-md-0 pb-3 pb-md-0"
                            >
                              <div class="process-flow-each-step">
                                <span class="process-flow-each-step-number"
                                  >3</span
                                >
                                <span class="process-folow-each-step-title"
                                  >We will process requirements</span
                                >
                              </div>
                            </div>
                            <div class="col-12 col-md-3">
                              <div class="process-flow-each-step">
                                <span class="process-flow-each-step-number"
                                  >4</span
                                >
                                <span class="process-folow-each-step-title"
                                  >Visa is delivered</span
                                >
                              </div>
                            </div>--%>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- Process flow Ends -->

                    <!-- Questions & Answers, Reviews Starts -->
                    <div class="col-12 py-2">
                      <div class="full-width qa-reviews">
                        <ul
                          class="nav nav-tabs qa-reviews-tabs"
                          id="myTab"
                          role="tablist"
                        >
                          <li class="nav-item">
                            <a
                              class="nav-link active"
                              id="qa-tab"
                              data-toggle="tab"
                              href="#qa"
                              role="tab"
                              aria-controls="qa"
                              aria-selected="true"
                              >Q &amp; A</a
                            >
                          </li>
                          <li class="nav-item">
                            <a
                              class="nav-link"
                              id="reviews-tab"
                              data-toggle="tab"
                              href="#reviews"
                              role="tab"
                              aria-controls="reviews"
                              aria-selected="false"
                              >Reviews</a
                            >
                          </li>
                        </ul>
                        <div class="tab-content" id="qaReviewsTabs">
                          <div
                            class="tab-pane fade show active"
                            id="qa"
                            role="tabpanel"
                            aria-labelledby="qa-tab"
                             
                          >
                            <div class="qa-search-block full-width">
                              <div
                                class="qa-search-title full-width heading-sm"
                              >
                                <strong
                                  >Have a question? Search for answers</strong
                                >
                              </div>
                     


                              <div class="qa-search-input-block full-width">
         

                                <%--<input
                                  type="search"
                                  class="form-control full-width"
                                />--%>
                                  <asp:HiddenField ID="hidSession" runat="server" />
                                  <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control full-width" AutoPostBack="true" OnTextChanged="txtSearch_Changed"></asp:TextBox>
                                  <ajax:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="txtSearch"
MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="1000" ServiceMethod="GetQuestionsAll"></ajax:AutoCompleteExtender>
                              </div>
                              <div
                                class="qa-post-question-block full-width text-center p-2 bg-light mt-1"
                              >
                                <div class="post-question-action-block full-width" id="divSelectQuestion" runat="server" visible="false">
                                    Didn't get the right answer you were looking for
                                  <button type="button" class="btn btn-outline-primary postQuestionBtn"
                                    id="btnPostBack" >Post a Question</button>
                                </div>
                                <div class="post-question-input-block full-width pt-2 d-none">
                                  <script type="text/javascript" src="editor/js/froala_editor.min.js"></script>
                                  <script type="text/javascript" src="editor/js/plugins/align.min.js"></script>
                                  <script type="text/javascript" src="editor/js/plugins/code_beautifier.min.js"></script>
                                  <script type="text/javascript" src="editor/js/plugins/code_view.min.js"></script>
                                  <script type="text/javascript" src="editor/js/plugins/draggable.min.js"></script>
                                  <script type="text/javascript" src="editor/js/plugins/image.min.js"></script>
                                  <script type="text/javascript" src="editor/js/plugins/image_manager.min.js"></script>
                                  <script>
                                      var hid = document.getElementById("hidSession").value;
                                      if (hid != "") {
                                          //alert(hid);
                                          $(document).ready(function () {
                                              var editor = new FroalaEditor('.questionEditor');
                                              editor;

                                              $('.postQuestionBtn, .question-close-btn').click(function () {

                                                  if ($('.post-question-input-block').hasClass('d-none')) {
                                                      $('.post-question-input-block').removeClass('d-none').slideDown('fast');
                                                  } else {
                                                      $('.post-question-input-block').addClass('d-none').slideUp('fast');
                                                  }
                                              });
                                              $('#btnSubmit').click(function () {
                                                  output = $('.fr-view').html();
                                                  //alert(output);
                                                  //alert("Click Once to Submit");
                                              });
                                          });
                                          $(document).ready(function () {
                                              $('#btnSubmit').click(function () {
                                                  $.ajax({
                                                      type: 'POST',
                                                      contentType: "application/json; charset=utf-8",
                                                      url: 'ProductDetails.aspx/InsertMethod',
                                                      data: "{'Question1':'" + output + "'}",
                                                      async: false,
                                                      success: function (response) {
                                                          //$('#txtUserName').val('');
                                                          //$('#txtEmail').val('');
                                                          alert("Record Has been Saved in Database");
                                                      },
                                                      error: function () {
                                                          console.log('there is some error');
                                                      }
                                                  });
                                              });
                                          });
                                      }
                                      else {
                                          $('#btnPostBack').click(function () {
                                              Navigate();
                                              //alert(output);
                                              //alert("Click Once to Submit");
                                          });
                                         
                                      }
                                      function Navigate()
                                      {
                                          //alert("Hello");
                                          var url = "Login.aspx";
                                          $(location).attr('href', url);
                                      }
                                  </script>
                                  <div class="questionEditor"></div>
                                  <div class="questionEditorSubit-block full-width text-right pt-2" >
                                    <button class="btn btn-outline-gray px-3 mr-2 question-close-btn" type="button">Close</button>
                                    <button class="btn btn-primary px-3"  id="btnSubmit" type="button">Submit</button>
                                  </div>
                                </div>
                              </div>
                                <div id="divNoQuestionAnswers" runat="server" visible="false">
                                    <asp:Label ID="lblError" runat="server"></asp:Label>
                                </div>
                              <div class="qa-list-block full-width pt-2" id="divQuestionAnswer" runat="server" visible="false">
                                <ul class="qa-list list-unstyled mb-0 full-width">
                                  <li class="full-width">
                                    <div class="each-qa-block full-width">
                                      <div class="each-qa-up-down-vote-block">
                                        <button class="up-vote-btn">
                                          <i class="mdi mdi-thumb-up"></i>
                                        </button>
                                        <div class="each-qa-vote-count">
                                          <span
                                            class="each-qa-vote-count-number"
                                            >99K</span
                                          >
                                          <span class="each-qa-vote-text"
                                            >Votes</span
                                          >
                                        </div>
                                        <button class="down-vote-btn">
                                          <i class="mdi mdi-thumb-down"></i>
                                        </button>
                                      </div>
                                      <div class="each-qa-content-block">
                                        <div class="each-question full-width">
                                          <div class="each-qa-title each-question-title">Question:</div>
                                          <div class="each-qa-title-content each-question-title-content">
                                           <%-- What is a visit or tourist visa?  --%>
                                               <asp:Label ID="lblQuestion1" runat="server"></asp:Label>
                                          </div>
                                        </div>
                                        <div class="each-answer full-width">
                                          <div class="each-qa-title each-answer-title">Answer:</div>
                                          <div class="each-qa-title-content each-answer-title-content">
                                            <div class="each-answer-block full-width mb-2">
                                              <div class="each-qa-title-content-text full-width"><%--Lorem ipsum dolor sit amet consectetur adipisicing elit. Error eius dignissimos alias aliquam velit, ex ipsum beatae dolores est illum ut? Sed aliquam id eveniet perspiciatis. Fugiat omnis nisi ipsa?--%>
                                                  <asp:Label ID="lblAnswer1" runat="server"></asp:Label></div>
                                              <div class="each-qa-user-date-block full-width">
                                                By <a href="#"><asp:Label ID="lblUsername1" runat="server"></asp:Label></a> <%--on 12th Oct, 2019--%><asp:Label ID="lblDate1" runat="server"></asp:Label>
                                              </div>
                                            </div>
                                            <div class="each-answer-block full-width mb-2">
                                              <div class="each-qa-title-content-text full-width"><%--Lorem ipsum dolor sit amet consectetur adipisicing elit. Error eius dignissimos alias aliquam velit, ex ipsum--%><asp:Label ID="lblAnswer2" runat="server"></asp:Label></div>
                                              <div class="each-qa-user-date-block full-width">
                                                By <a href="#"><%--Yugandhar--%><asp:Label ID="lblUsername2" runat="server"></asp:Label></a><asp:Label ID="lblDate2" runat="server"></asp:Label><%--on 12th Oct, 2019--%>
                                              </div>
                                            </div>

                                            <div class="each-answer-block full-width mb-2">
                                              <div class="each-qa-title-content-text full-width"><%--Lorem ipsum dolor sit amet consectetur--%><asp:Label ID="lblAnswer3" runat="server"></asp:Label>  </div>
                                              <div class="each-qa-user-date-block full-width">
                                                By <a href="#"><%--Yugandhar--%><asp:Label ID="lblUsername1Answer" runat="server"></asp:Label></a><%-- on 12th Oct, 2019--%><asp:Label ID="lblDateDispaly" runat="server"></asp:Label>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </li>

                                  <li class="full-width">
                                      <div class="each-qa-block full-width">
                                      <div class="each-qa-up-down-vote-block">
                                        <button class="up-vote-btn">
                                          <i class="mdi mdi-thumb-up"></i>
                                        </button>
                                        <div class="each-qa-vote-count">
                                          <span
                                            class="each-qa-vote-count-number"
                                            >99K</span
                                          >
                                          <span class="each-qa-vote-text"
                                            >Votes</span
                                          >
                                        </div>
                                        <button class="down-vote-btn">
                                          <i class="mdi mdi-thumb-down"></i>
                                        </button>
                                      </div>
                                      <div class="each-qa-content-block">
                                        <div class="each-question full-width">
                                          <div class="each-qa-title each-question-title">Question:</div>
                                          <div class="each-qa-title-content each-question-title-content">
                                           <%-- What is a visit or tourist visa?  --%>
                                                <asp:Label ID="lblQuestion2" runat="server"></asp:Label>
                                          </div>
                                        </div>
                                        <div class="each-answer full-width">
                                          <div class="each-qa-title each-answer-title">Answer:</div>
                                          <div class="each-qa-title-content each-answer-title-content">
                                            <div class="each-answer-block full-width mb-2">
                                              <div class="each-qa-title-content-text full-width"><%--Alias aliquam velit, ex ipsum beatae dolores est illum ut? Sed aliquam id eveniet perspiciatis. Fugiat omnis nisi ipsa?--%>
                                                  <asp:Label ID="lblSecondAnswer1" runat="server"></asp:Label></div>
                                              <div class="each-qa-user-date-block full-width">
                                                By <a href="#"><%--Yugandhar--%> <asp:Label ID="lblUsername3" runat="server"></asp:Label></a> <%--on 12th Oct, 2019--%><asp:Label ID="lblDate3" runat="server"></asp:Label>
                                              </div>
                                                 <div class="each-qa-title-content-text full-width">
                                                  <%--Alias aliquam velit, ex ipsum beatae dolores est illum ut? Sed aliquam id eveniet perspiciatis. Fugiat omnis nisi ipsa?--%>
                                                 <asp:Label ID="lblSecondAnswer2" runat="server"></asp:Label>
                                               </div>
                                           <div class="each-qa-user-date-block full-width">
                                           By <a href="#">
                                            <asp:Label ID="lblUsername4" runat="server"></asp:Label></a> <%--on 12th Oct, 2019--%>
                                            <asp:Label ID="lblDate4" runat="server"></asp:Label>
                                            </div>
                                            <button class="each-more-answers-btn">
                                                See more answers (3)
                                              </button>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </li>
                                </ul>
                              </div>
                            </div>
                          </div>
                          <div
                            class="tab-pane fade"
                            id="reviews"
                            role="tabpanel"
                            aria-labelledby="reviews-tab"
                          >
                            <div class="full-width customer-reviews-block">
                             <asp:DataList ID="dvReviews" runat="server">
                                 <ItemTemplate>
                                     <ul class="reviews-list list-unstyled mb-0 full-width">
                                <li class="full-width mb-3">
                                    <div class="each-review full-width">
                                        <a href="#" class="each-review-user-info full-width">
                                            <div class="each-review-user-photo no-photo background-cover"
                                                style="background-image: url(images/photos/pexels-photo-380769.jpeg);">
                                            </div>
                                            <div class="each-review-user-name pl-1"> <%--Srirangam Yugandhar Srirangam
                                                Yugandhar--%>
                                                <asp:Label ID="lblRaterName" runat="server" Text='<%#Eval("Name")%>'></asp:Label>
                                            </div>
                                        </a>
                                        <div class="each-review-rating-title full-width pt-05">
                                          
                                        <div class="each-review-rating">
                                                <a href="#">
                                           <%--     <asp:ImageButton ID="img1" runat="server" ImageUrl="~/images/star1.gif"  Width="30"  Visible="false" OnClick="img1_Click"/>
                                                <asp:ImageButton ID="img2" runat="server" ImageUrl="~/images/star2.gif" Width="30" Visible="false" onClick="img2_Click"/>
                                                <asp:ImageButton ID="img3" runat="server" ImageUrl="~/images/star3.gif" Width="30" Visible="false" OnClick="img3_Click" />
                                                <asp:ImageButton ID="img4" runat="server" ImageUrl="~/images/latestimagefour.gif" Width="30" Visible="false" OnClick="img4_Click" />
                                                <asp:ImageButton ID="img5" runat="server" ImageUrl="~/images/fivestar.gif" Width="30" Visible="false" OnClick="img5_Click" />&nbsp;&nbsp;&nbsp;--%>
                                                <asp:Image  ID="imgFirst" runat="server" ImageUrl='<%#Eval("ImgFirst")%>' />
                                                    <asp:Image  ID="imgSecond" runat="server" ImageUrl='<%#Eval("ImgSecond")%>' />
                                                    <asp:Image  ID="imgThird" runat="server" ImageUrl='<%#Eval("ImgThird")%>' />
                                                    <asp:Image  ID="imgFourth" runat="server" ImageUrl='<%#Eval("ImgFourth")%>' />
                                                    <asp:Image  ID="imgFifth" runat="server" ImageUrl='<%#Eval("ImgFifth")%>' />
                                              <%--      <i class="mdi mdi-star"></i>
                                                    <i class="mdi mdi-star"></i>
                                                    <i class="mdi mdi-star"></i>
                                                    <i class="mdi mdi-star-half"></i>
                                                    <i class="mdi mdi-star-outline"></i>--%>
                                                </a>
                                                
                                               <%--  <asp:ImageButton ID="img1" runat="server" ImageUrl="~/images/star1.gif"  Width="30"  Visible="false" OnClick="img1_Click"/>
                                                <asp:ImageButton ID="img2" runat="server" ImageUrl="~/images/star2.gif" Width="30" Visible="false" onClick="img2_Click"/>
                                                <asp:ImageButton ID="img3" runat="server" ImageUrl="~/images/star3.gif" Width="30" Visible="false" OnClick="img3_Click" />
                                                <asp:ImageButton ID="img4" runat="server" ImageUrl="~/images/latestimagefour.gif" Width="30" Visible="false" OnClick="img4_Click" />
                                                <asp:ImageButton ID="img5" runat="server" ImageUrl="~/images/fivestar.gif" Width="30" Visible="false" OnClick="img5_Click" />&nbsp;&nbsp;&nbsp;--%>
                                                  
                                            </div>
                                            <div>
                                                <%--<asp:label id="productrate" runat="server" Text='<%#Eval("DisplayDetails")%>' />--%>
                                            </div>
                                                   
                                            <div>
                                            <a href="#" class="each-review-title font-weight-bold"> Awesome service,
                                                with 100%
                                                satisfactions </a>
                                            </div>
                                        </div>

                                        <div class="each-review-date full-width"> 10th February, 2019 </div>
                                        <div class="each-review-content full-width pt-15">
                                            <p>LACK PLAYBACK VOLUME & DISPLAY MIRRORING.</p>
                                            <p>If anyone is buying this firestick only because of the volume keys
                                                upfront which were missing in the previous models, don't buy this. It
                                                doesn't control playback volume of the fire stick instead firestick
                                                remote detects the infrared signals of the TV, and controls TV volume.
                                                Pretty useless for me as I have bought this to control playback volume
                                                of the firestick because my wireless hands free doesn't have any buttons
                                                for volume. Thanks to Amazon return policy . I have made the return
                                                request. Also this one missing display mirroring I mean why? Other than
                                                that new firestick is flowless and more powerful than previous models.
                                            </p>
                                        </div>
                                        <div class="each-review-footer full-width">
                                            <div class="each-review-footer-helpful-count full-width text-gray">79 people
                                                found this helpful</div>
                                            <div class="each-review-footer-actions full-width">
                                                <button class="btn btn-outline-gray btn-sm
                                                        px-2 mr-1">Helpful</button><button type="button"
                                                    class="btn btn-sm btn-link text-gray comment-review-btn">Comment</button>|<button
                                                    type="button" data-target="#reportAbuse" data-toggle="modal"
                                                    class="btn btn-sm btn-link text-gray">Report Abuse</button>
                                            </div>
                                        </div>
                                        <div class="each-review-comment-input d-none full-width">
                                            <div class="questionEditor"></div>
                                             <div class="questionEditorSubit-block full-width text-right pt-2">
                                              <button class="btn btn-outline-gray px-3 mr-2 comment-close-btn" type="button">Close</button>
                                              <button class="btn btn-primary px-3" type="button">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                               <%-- <li class="full-width mb-3">
                                    <div class="each-review full-width">
                                        <a href="#" class="each-review-user-info full-width">
                                            <div class="each-review-user-photo no-photo background-cover"
                                                style="background-image: url(images/photos/pexels-photo-380769.jpeg);">
                                            </div>
                                            <div class="each-review-user-name pl-1"> Srirangam Yugandhar Srirangam
                                                Yugandhar </div>
                                        </a>
                                        <div class="each-review-rating-title full-width pt-05">
                                            <div class="each-review-rating">
                                                <a href="#">
                                                    <i class="mdi mdi-star"></i>
                                                    <i class="mdi mdi-star"></i>
                                                    <i class="mdi mdi-star"></i>
                                                    <i class="mdi mdi-star-half"></i>
                                                    <i class="mdi mdi-star-outline"></i>
                                                </a>
                                            </div>
                                            <a href="#" class="each-review-title font-weight-bold"> Awesome service,
                                                with 100%
                                                satisfactions </a>
                                        </div>
                                        <div class="each-review-date full-width"> 10th February, 2019 </div>
                                        <div class="each-review-content full-width pt-15">
                                            <p>LACK PLAYBACK VOLUME & DISPLAY MIRRORING.</p>
                                            <p>If anyone is buying this firestick only because of the volume keys
                                                upfront which were missing in the previous models, don't buy this. It
                                                doesn't control playback volume of the fire stick instead firestick
                                                remote detects the infrared signals of the TV, and controls TV volume.
                                                Pretty useless for me as I have bought this to control playback volume
                                                of the firestick because my wireless hands free doesn't have any buttons
                                                for volume. Thanks to Amazon return policy . I have made the return
                                                request. Also this one missing display mirroring I mean why? Other than
                                                that new firestick is flowless and more powerful than previous models.
                                            </p>
                                        </div>
                                        <div class="each-review-footer full-width">
                                            <div class="each-review-footer-helpful-count full-width text-gray">79 people
                                                found this helpful</div>
                                            <div class="each-review-footer-actions full-width">
                                                <button class="btn btn-outline-gray btn-sm
                                                        px-2 mr-1">Helpful</button><button type="button"
                                                    class="btn btn-sm btn-link text-gray comment-review-btn">Comment</button>|<button
                                                    type="button" data-target="#reportAbuse" data-toggle="modal"
                                                    class="btn btn-sm btn-link text-gray">Report Abuse</button>
                                            </div>
                                        </div>
                                        <div class="each-review-comment-input d-none full-width">
                                            <div class="questionEditor"></div>
                                        </div>
                                    </div>
                                </li>
                                <li class="full-width mb-3">
                                    <div class="each-review full-width">
                                        <a href="#" class="each-review-user-info full-width">
                                            <div class="each-review-user-photo no-photo background-cover"
                                                style="background-image: url(images/photos/pexels-photo-380769.jpeg);">
                                            </div>
                                            <div class="each-review-user-name pl-1"> Srirangam Yugandhar Srirangam
                                                Yugandhar </div>
                                        </a>
                                        <div class="each-review-rating-title full-width pt-05">
                                            <div class="each-review-rating">
                                                <a href="#">
                                                    <i class="mdi mdi-star"></i>
                                                    <i class="mdi mdi-star"></i>
                                                    <i class="mdi mdi-star"></i>
                                                    <i class="mdi mdi-star-half"></i>
                                                    <i class="mdi mdi-star-outline"></i>
                                                </a>
                                            </div>
                                            <a href="#" class="each-review-title font-weight-bold"> Awesome service,
                                                with 100%
                                                satisfactions </a>
                                        </div>
                                        <div class="each-review-date full-width"> 10th February, 2019 </div>
                                        <div class="each-review-content full-width pt-15">
                                            <p>LACK PLAYBACK VOLUME & DISPLAY MIRRORING.</p>
                                            <p>If anyone is buying this firestick only because of the volume keys
                                                upfront which were missing in the previous models, don't buy this. It
                                                doesn't control playback volume of the fire stick instead firestick
                                                remote detects the infrared signals of the TV, and controls TV volume.
                                                Pretty useless for me as I have bought this to control playback volume
                                                of the firestick because my wireless hands free doesn't have any buttons
                                                for volume. Thanks to Amazon return policy . I have made the return
                                                request. Also this one missing display mirroring I mean why? Other than
                                                that new firestick is flowless and more powerful than previous models.
                                            </p>
                                        </div>
                                        <div class="each-review-footer full-width">
                                            <div class="each-review-footer-helpful-count full-width text-gray">79 people
                                                found this helpful</div>
                                            <div class="each-review-footer-actions full-width">
                                                <button class="btn btn-outline-gray btn-sm
                                                        px-2 mr-1">Helpful</button><button type="button"
                                                    class="btn btn-sm btn-link text-gray comment-review-btn">Comment</button>|<button
                                                    type="button" data-target="#reportAbuse" data-toggle="modal"
                                                    class="btn btn-sm btn-link text-gray">Report Abuse</button>
                                            </div>
                                        </div>
                                        <div class="each-review-comment-input d-none full-width">
                                            <div class="questionEditor"></div>
                                        </div>
                                    </div>
                                </li>
                                <li class="full-width">
                                    <div class="each-review full-width">
                                        <a href="#" class="each-review-user-info full-width">
                                            <div class="each-review-user-photo no-photo background-cover"
                                                style="background-image: url(images/photos/pexels-photo-380769.jpeg);">
                                            </div>
                                            <div class="each-review-user-name pl-1"> Srirangam Yugandhar Srirangam
                                                Yugandhar </div>
                                        </a>
                                        <div class="each-review-rating-title full-width pt-05">
                                            <div class="each-review-rating">
                                                <a href="#">
                                                    <i class="mdi mdi-star"></i>
                                                    <i class="mdi mdi-star"></i>
                                                    <i class="mdi mdi-star"></i>
                                                    <i class="mdi mdi-star-half"></i>
                                                    <i class="mdi mdi-star-outline"></i>
                                                </a>
                                            </div>
                                            <a href="#" class="each-review-title font-weight-bold"> Awesome service,
                                                with 100%
                                                satisfactions </a>
                                        </div>
                                        <div class="each-review-date full-width"> 10th February, 2019 </div>
                                        <div class="each-review-content full-width pt-15">
                                            <p>LACK PLAYBACK VOLUME & DISPLAY MIRRORING.</p>
                                            <p>If anyone is buying this firestick only because of the volume keys
                                                upfront which were missing in the previous models, don't buy this. It
                                                doesn't control playback volume of the fire stick instead firestick
                                                remote detects the infrared signals of the TV, and controls TV volume.
                                                Pretty useless for me as I have bought this to control playback volume
                                                of the firestick because my wireless hands free doesn't have any buttons
                                                for volume. Thanks to Amazon return policy . I have made the return
                                                request. Also this one missing display mirroring I mean why? Other than
                                                that new firestick is flowless and more powerful than previous models.
                                            </p>
                                        </div>
                                        <div class="each-review-footer full-width">
                                            <div class="each-review-footer-helpful-count full-width text-gray">79 people
                                                found this helpful</div>
                                            <div class="each-review-footer-actions full-width">
                                                <button class="btn btn-outline-gray btn-sm
                                                        px-2 mr-1">Helpful</button><button type="button"
                                                    class="btn btn-sm btn-link text-gray comment-review-btn">Comment</button>|<button
                                                    type="button" data-target="#reportAbuse" data-toggle="modal"
                                                    class="btn btn-sm btn-link text-gray">Report Abuse</button>
                                            </div>
                                        </div>
                                        <div class="each-review-comment-input d-none full-width">
                                            <div class="chatting-input full-width pt-1">
                                                <div class="chatting-input-block full-width">
                                                    <div class="full-width">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div
                                                                    class="chatting-input-textarea-block full-width p-1">
                                                                    <textarea name="" id="" cols="" rows="3"
                                                                        class="chatting-textarea-input autoExpand full-width"
                                                                        placeholder="Type your comment here"
                                                                        data-emoji-input="unicode"
                                                                        data-emojiable="true"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="chatting-options full-width">
                                                        <div class="chatting-count float-left">2/100</div>
                                                        <div class="chatting-attachment-block float-right">
                                                            <div class="chatting-attachment-input-block"><input
                                                                    type="file" class="chatting-attachment-input">
                                                                <div class="chatting-attachment-text"><i
                                                                        class="material-icons">attachment</i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="chatting-action-block full-width text-right mt-2">
                                                    <input type="button"
                                                        class="chatting-submit btn btn-primary px-3 py-1"
                                                        value="Submit Comment" disabled="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>--%>
                            </ul>
                                 </ItemTemplate>
                             </asp:DataList>
                              
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- Questions & Answers, Reviews Ends -->
                  </div>
                </div>
              </div>
              <!-- Product Detail Full Block Ends -->
            </div>
          </div>
          <div class="col-12 col-lg-4">
            <div class="full-width position-relative h-100 bg-light p-2">
              <div class="full-width related-visas-block">
                <h3 class="heading-sm ful-width pb-15 text-uppercase">Related Visas</h3>
                <div class="full-width related-visas-list">
                  <ul class="each-home-section-list row align-items-stretch">
                         <%--<asp:DataList ID="dlrelatedvisas" runat="server" RepeatColumns="4">
                    <ItemTemplate>
                    <li class="col-12 col-md-6 col-lg-12">
                      <div class="each-product move-product">
                        <a
                          href="product-detail.html"
                          class="full-width each-product-link-block">
                          <div class="each-product-title-block full-width">
                            <span
                              class="each-product-image full-width"
                              style="background-image: url(images/visa-1.jpg);"
                            >
                            </span>
                            <span class="each-product-title-text"
                              ><%#Eval("Name")%></span
                            >
                          </div>
                          <div class="each-product-desc full-width text-justify">
                           <%#Eval("Description")%>
                          </div>
                          <div class="each-product-action full-width text-center">
                              <a href="ProductDetails.aspx?pid=<%#Eval("ProductId")%>">
                            <span class="each-product-btn">
                              Get Visa
                            </span>
                          </div>
                        </a>

                        <div class="each-product-tags-list full-width">
                          <a href="#" class="each-tag">spouse</a>
                          <a href="#" class="each-tag">parents</a>
                          <a href="#" class="each-tag">+2 more</a>
                        </div>
                      </div>
                    </li>--%>
                    <li class="col-12 col-md-6 col-lg-12">
                      <div class="each-product move-product">
                        <a
                          href="ProductDetails.aspx"
                          class="full-width each-product-link-block"
                        >
                          <div class="each-product-title-block full-width">
                            <span
                              class="each-product-image full-width"
                              style="background-image: url(images/visa-1.jpg);"
                            >
                            </span>
                            <span class="each-product-title-text"
                              >Dependent Visa 2</span
                            >
                          </div>
                          <div class="each-product-desc full-width text-justify">
                            Lorem ipsum dolor sit, amet consectetur adipisicing
                            elit. Necessitatibus eius aspernatur officia quisquam
                            porro quaerat dignissimos rem iste esse amet sint
                            facilis placeat, aliquid neque doloremque eos laborum
                            ipsa nobis.
                          </div>
                          <div class="each-product-action full-width text-center">
                            <span class="each-product-btn">
                              Get Visa
                            </span>
                          </div>
                        </a>

                        <div class="each-product-tags-list full-width">
                          <a href="#" class="each-tag">parents</a>
                          <a href="#" class="each-tag">spouse</a>
                        </div>
                      </div>
                    </li>
                    <li class="col-12 col-md-6 col-lg-12">
                      <div class="each-product move-product">
                        <a
                          href="product-detail.html"
                          class="full-width each-product-link-block"
                        >
                          <div class="each-product-title-block full-width">
                            <span
                              class="each-product-image full-width"
                              style="background-image: url(images/visa-1.jpg);"
                            >
                            </span>
                            <span class="each-product-title-text"
                              >Dependent Visa 3</span
                            >
                          </div>
                          <div class="each-product-desc full-width text-justify">
                            Lorem ipsum dolor sit, amet consectetur adipisicing
                            elit. Necessitatibus eius aspernatur officia quisquam
                            porro quaerat dignissimos rem iste esse amet sint
                            facilis placeat, aliquid neque doloremque eos laborum
                            ipsa nobis.
                          </div>
                          <div class="each-product-action full-width text-center">
                            <span class="each-product-btn">
                              Get Visa
                            </span>
                          </div>
                        </a>

                        <div class="each-product-tags-list full-width">
                          <a href="#" class="each-tag">spouse</a>
                          <a href="#" class="each-tag">parents</a>
                        </div>
                      </div>
                    </li>
                    <li class="col-12 col-md-6 col-lg-12">
                      <div class="each-product move-product">
                        <a
                          href="product-detail.html"
                          class="full-width each-product-link-block"
                        >
                          <div class="each-product-title-block full-width">
                            <span
                              class="each-product-image full-width"
                              style="background-image: url(images/visa-1.jpg);"
                            >
                            </span>
                            <span class="each-product-title-text"
                              >Dependent Visa 4</span
                            >
                          </div>
                          <div class="each-product-desc full-width text-justify">
                            Lorem ipsum dolor sit, amet consectetur adipisicing
                            elit. Necessitatibus eius aspernatur officia quisquam
                            porro quaerat dignissimos rem iste esse amet sint
                            facilis placeat, aliquid neque doloremque eos laborum
                            ipsa nobis.
                          </div>
                          <div class="each-product-action full-width text-center">
                            <span class="each-product-btn">
                              Get Visa
                            </span>
                          </div>
                        </a>

                        <div class="each-product-tags-list full-width">
                          <a href="#" class="each-tag">spouse</a>
                          <a href="#" class="each-tag">parents</a>
                          <a href="#" class="each-tag">+2 more</a>
                        </div>
                      </div>
                    </li>
                         <%--</ItemTemplate>
                           </asp:DataList>--%>
                  </ul>
                       
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
   </form>
    <footer class="footer full-width py-3 bg-light">
      <div class="full-width">
        <div class="container">
          <div class="row">
            <div class="col-12 col-md-6 col-lg-3 col-xl-2 mb-3 mb-lg-0">
              <div class="full-width footer-navigation-block">
                <a href="Home.aspx" class="each-footer-nav">Home</a>
                <a href="products.aspx" class="each-footer-nav">Products</a>
                <a href="about-us.html" class="each-footer-nav">About us</a>
                <a href="faqs.html" class="each-footer-nav">FQAs</a>
                <a href="contact-us.html" class="each-footer-nav">Contact us</a>
                <a href="terms-conditions.html" class="each-footer-nav"
                  >Terms &amp; Conditions</a
                >
                <a href="privacy-policy.html" class="each-footer-nav"
                  >Privacy Policy</a
                >
                <a href="refund-policy.html" class="each-footer-nav"
                  >Refund Policy</a
                >
              </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3 col-xl-3 mb-3 mb-lg-0">
              <div class="full-width footer-navigation-block">
                <a href="products.html" class="each-footer-nav"
                  >Canada Dependent Visa</a
                >
                <a href="products.html" class="each-footer-nav"
                  >Germany Dependent Visa</a
                >
                <a href="products.html" class="each-footer-nav"
                  >Uk Dependent Visa</a
                >
                <a href="products.html" class="each-footer-nav"
                  >Canada Dependent Visa</a
                >
                <a href="products.html" class="each-footer-nav"
                  >Germany Dependent Visa</a
                >
                <a href="products.html" class="each-footer-nav"
                  >Uk Dependent Visa</a
                >
              </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3 col-xl-3 mb-3 mb-lg-0">
              <div class="full-width footer-navigation-block">
                <a href="products.html" class="each-footer-nav"
                  >Dependent Visa for Spouse</a
                >
                <a href="products.html" class="each-footer-nav"
                  >Dependent Visa for Kids</a
                >
                <a href="products.html" class="each-footer-nav"
                  >Dependent Visa for Parents</a
                >
              </div>
            </div>

            <div class="col-12 col-md-6 col-lg-3 col-xl-4 mb-3 mb-lg-0">
              <div class="full-width subscribe-social-block">
                <div class="full-width pb-1 font-size-md subscribe-heading">
                  Subscribe for latest updates
                </div>
                <div class="subscribe-block full-width mb-3">
                  <input type="email" class="subscribe-input form-control" />
                  <button class="subscribe-button btn-primary">
                    Subscribe
                  </button>
                </div>
                <div
                  class="social-block full-width font-size-md subscribe-heading py-1"
                >
                  Follow us on
                </div>
                <div class="social-list-block full-width">
                  <a
                    href="#"
                    class="footer-each-social-link facebook"
                    target="_blank"
                    ><i class="mdi mdi-facebook"></i
                  ></a>
                  <a
                    href="#"
                    class="footer-each-social-link twitter"
                    target="_blank"
                    ><i class="mdi mdi-twitter"></i
                  ></a>
                  <a
                    href="#"
                    class="footer-each-social-link instagram"
                    target="_blank"
                    ><i class="mdi mdi-instagram"></i
                  ></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="full-width copy-rights-block text-center mt-2">
        <span class="copy-rights-text bg-light"
          >Copy Rights &copy; 2019&nbsp;&nbsp;|&nbsp;&nbsp;<a href="#"
            >www.dependentvisas.com</a
          ></span
        >
      </div>
    </footer>

    <script src="js/bootstrap.min.js"></script>
    <script src="js/common.js"></script>

    <script>
      $(document).ready(function() {
        $(".product-stars-block")
          .mouseenter(function() {
            $(this).addClass("show");
          })
          .mouseleave(function() {
            $(this).removeClass("show");
          });
      });
    </script>
  </body>
</html>
