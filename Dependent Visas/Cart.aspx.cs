﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static Dependent_Visas.Translayer;

namespace Dependent_Visas
{
    public partial class Cart : System.Web.UI.Page
    {
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CartCount();
                CartDetails();
            }
        }

        public void CartCount()
        {
            CartDetails objCart = new CartDetails();
            objCart.Operation = "Get";
            objCart.RegistrationId = Convert.ToInt32(Session["UserId"]);
            ds = BLL.GetDetails(objCart);
            if (ds.Tables[0].Rows.Count > 0)
                lblcount.Text = Convert.ToString(ds.Tables[0].Rows[0]["Count"]);
            else
                lblcount.Text = "0";
        }

        public void CartDetails()
        {
            CartDetails objCart = new CartDetails();
            objCart.Operation = "GetDetails";
            objCart.RegistrationId = Convert.ToInt32(Session["UserId"]);
            ds = BLL.GetDetails(objCart);
            if (ds.Tables[0].Rows.Count > 0)
            {
                dlCart.DataSource = ds.Tables[0];
                dlCart.DataBind();
                lblSubTotal.Text = Convert.ToString(ds.Tables[1].Rows[0]["Total"]);
                decimal Subtotal = Convert.ToDecimal(ds.Tables[1].Rows[0]["Total"]);
                decimal Tax = (Subtotal * 18 / 100);
                lblTax.Text = Convert.ToString(Tax);
                lblTotal.Text = Convert.ToString(Subtotal + Tax);
                ViewState["Total"] = lblSubTotal.Text;
            }

        }


        protected void btnCheckOut_Click(object sender, EventArgs e)
        {

           // Session["Total"] = lblTotal.Text;

        }

        //protected void ddlQunatity_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    lblTotal.Text = ddlQunatity.SelectedValue;
        //}

        protected void ddlQty_SelectedIndexChanged(object sender, EventArgs e)
        {
            var ddlList = (DropDownList)sender;
            string selectedValue = ((DropDownList)ddlList.NamingContainer.FindControl("ddlQunatity")).SelectedValue;
            lblSubTotal.Text = Convert.ToString(Convert.ToDecimal(ViewState["Total"]) * Convert.ToInt16(selectedValue));
            // lblTotal.Text = ddlList.SelectedValue;
            decimal Subtotal = Convert.ToDecimal(lblSubTotal.Text);
            decimal Tax = (Subtotal * 18 / 100);
            lblTax.Text = Convert.ToString(Tax);
            lblTotal.Text = Convert.ToString(Subtotal + Tax);
        }

        //protected void btnDelete_Click(object sender, EventArgs e)
        //{
            

        //}

        protected void btnDelete_Command(object sender, CommandEventArgs e)
        {
            CartDetails objcart = new CartDetails();
            objcart.Operation = "Delete";
            objcart.ProductId = Convert.ToInt32(e.CommandArgument);
            objcart.RegistrationId = Convert.ToInt32(Session["UserId"]);
            bool status = Convert.ToBoolean(BLL.Deletecart(objcart));
            if (status == true)
            {
                Response.Write("<script language=javascript>alert('Item Deleted')</script>");
                CartDetails();
                CartCount();
                //clearcontrols();
            }

            else
                Response.Write("<script language=javascript>alert('Item Not Deleted')</script>");

        }
    }
}