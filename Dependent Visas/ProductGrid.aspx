﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProductGrid.aspx.cs" Inherits="Dependent_Visas.ProductGrid" %>

<!DOCTYPE html>

<%--<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
</body>
</html>--%>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CRM - Dashboard</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700,800" rel="stylesheet">
    <link rel="stylesheet" href="css/crm.css">
    <link rel="stylesheet" href="css/fonts/font-awesome-material-icons.css">
    <style>
        .branding-block-logo {
            background-image: url(images/opulentus-circle.png);
        }

        @media screen and (min-width: 600px) {
            .branding-block-logo {
                background-image: url(images/opulentus-logo.jpg);
            }
        }
    </style>
    <script src="js/jquery.min.js"></script>
    
</head>

<body class="dashboard menu-small">
  
    <!--  has-sidebar-md -->
    <form id="form1" runat="server">
        <div class="outer-block">
            <header class="header full-width">
                <div class="branding-block">
                    <a href="index.html" class="branding-block-logo background-contain"></a>
                </div>
                <div class="header-actions">
                    <div class="full-width ">
                        <button class="desktop-menu-toggle d-none d-lg-block">
                            <i
                                class="material-icons">menu</i></button>
                        <nav class="header-nav-block ">
                            <ul class="header-nav-list full-width">
                                <li>
                                    <div class="each-header-nav-icon-block dropdown">
                                        <button
                                            class="each-header-nav-icon dropdown-btn notifications-btn"
                                            data-toggle="tooltip" title="Notifications">
                                            <i class="material-icons">notifications</i>
                                        </button>
                                        <div
                                            class="each-header-nav-content dropdown-content notifications-content">
                                            <button class="dropdown-content-close">
                                                <i
                                                    class="material-icons">close</i></button>
                                            <ul class="notifications-list max-height-300">
                                                <li>
                                                    <a href="#" class="each-notification">
                                                        <div class="each-notificatoion-icon">
                                                            <i
                                                                class="material-icons">notifications</i>
                                                        </div>
                                                        <div class="each-notification-text">
                                                            Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="each-notification">
                                                        <div class="each-notificatoion-icon">
                                                            <i
                                                                class="material-icons">notifications</i>
                                                        </div>
                                                        <div class="each-notification-text">
                                                            Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="each-notification">
                                                        <div class="each-notificatoion-icon">
                                                            <i
                                                                class="material-icons">notifications</i>
                                                        </div>
                                                        <div class="each-notification-text">
                                                            Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="each-notification">
                                                        <div class="each-notificatoion-icon">
                                                            <i
                                                                class="material-icons">notifications</i>
                                                        </div>
                                                        <div class="each-notification-text">
                                                            Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="each-notification">
                                                        <div class="each-notificatoion-icon">
                                                            <i
                                                                class="material-icons">notifications</i>
                                                        </div>
                                                        <div class="each-notification-text">
                                                            Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="each-notification">
                                                        <div class="each-notificatoion-icon">
                                                            <i
                                                                class="material-icons">notifications</i>
                                                        </div>
                                                        <div class="each-notification-text">
                                                            Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="each-notification">
                                                        <div class="each-notificatoion-icon">
                                                            <i
                                                                class="material-icons">notifications</i>
                                                        </div>
                                                        <div class="each-notification-text">
                                                            Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="each-notification">
                                                        <div class="each-notificatoion-icon">
                                                            <i
                                                                class="material-icons">notifications</i>
                                                        </div>
                                                        <div class="each-notification-text">
                                                            Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="each-notification">
                                                        <div class="each-notificatoion-icon">
                                                            <i
                                                                class="material-icons">notifications</i>
                                                        </div>
                                                        <div class="each-notification-text">
                                                            Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="each-notification">
                                                        <div class="each-notificatoion-icon">
                                                            <i
                                                                class="material-icons">notifications</i>
                                                        </div>
                                                        <div class="each-notification-text">
                                                            Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                        </div>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="each-header-nav-icon-block dropdown">
                                        <button class="each-header-nav-icon dropdown-btn email-btn boun"
                                            data-toggle="tooltip" title="Emails">
                                            <i class="material-icons">email</i>
                                        </button>
                                        <div
                                            class="each-header-nav-content dropdown-content email-content">
                                            <button class="dropdown-content-close">
                                                <i
                                                    class="material-icons">close</i></button>
                                            <ul class="notifications-list max-height-300">
                                                <li>
                                                    <a href="#" class="each-notification">
                                                        <div class="each-notificatoion-icon">
                                                            <i
                                                                class="material-icons">email</i>
                                                        </div>
                                                        <div class="each-notification-text">
                                                            Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="each-notification">
                                                        <div class="each-notificatoion-icon">
                                                            <i
                                                                class="material-icons">email</i>
                                                        </div>
                                                        <div class="each-notification-text">
                                                            Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="each-notification">
                                                        <div class="each-notificatoion-icon">
                                                            <i
                                                                class="material-icons">email</i>
                                                        </div>
                                                        <div class="each-notification-text">
                                                            Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="each-notification">
                                                        <div class="each-notificatoion-icon">
                                                            <i
                                                                class="material-icons">email</i>
                                                        </div>
                                                        <div class="each-notification-text">
                                                            Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="each-notification">
                                                        <div class="each-notificatoion-icon">
                                                            <i
                                                                class="material-icons">email</i>
                                                        </div>
                                                        <div class="each-notification-text">
                                                            Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="each-notification">
                                                        <div class="each-notificatoion-icon">
                                                            <i
                                                                class="material-icons">email</i>
                                                        </div>
                                                        <div class="each-notification-text">
                                                            Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="each-notification">
                                                        <div class="each-notificatoion-icon">
                                                            <i
                                                                class="material-icons">email</i>
                                                        </div>
                                                        <div class="each-notification-text">
                                                            Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="each-notification">
                                                        <div class="each-notificatoion-icon">
                                                            <i
                                                                class="material-icons">email</i>
                                                        </div>
                                                        <div class="each-notification-text">
                                                            Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="each-notification">
                                                        <div class="each-notificatoion-icon">
                                                            <i
                                                                class="material-icons">email</i>
                                                        </div>
                                                        <div class="each-notification-text">
                                                            Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="each-notification">
                                                        <div class="each-notificatoion-icon">
                                                            <i
                                                                class="material-icons">email</i>
                                                        </div>
                                                        <div class="each-notification-text">
                                                            Each
                                                                                    Notifications runs here like this
                                                                                    and looks like this
                                                        </div>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="each-header-nav-icon-block dropdown">
                                        <button class="each-header-nav-icon dropdown-btn schedule-btn"
                                            data-toggle="tooltip" title="Calls Details">
                                            <i class="material-icons">schedule</i>
                                        </button>
                                        <div
                                            class="each-header-nav-content dropdown-content pt-0 schedule-content">
                                            <button class="dropdown-content-close">
                                                <i
                                                    class="material-icons">close</i></button>
                                            <ul class="notifications-list max-height-300">
                                                <li class="alert-success">
                                                    <div class="row">
                                                        <div class="col-7 text-left">
                                                            Completed
                                                                                    Call Duration
                                                        </div>
                                                        <div class="col-5 text-right">
                                                            00:00:00
                                                                             
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="alert-info">
                                                    <div class="row">
                                                        <div class="col-7 text-left">
                                                            Wrapup Call
                                                                                    Duration
                                                        </div>
                                                        <div class="col-5 text-right">
                                                            00:00:00
                                                                             
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="alert-danger">
                                                    <div class="row">
                                                        <div class="col-7 text-left">
                                                            Failed Call
                                                                                    Duration
                                                        </div>
                                                        <div class="col-5 text-right">
                                                            00:00:00
                                                                             
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="alert-warning">
                                                    <div class="row">
                                                        <div class="col-7 text-left">
                                                            Walkin
                                                                                    Duration
                                                        </div>
                                                        <div class="col-5 text-right">
                                                            00:00:00
                                                                             
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="bg-success text-white">
                                                    <div class="row">
                                                        <div class="col-7 text-left">
                                                            Total
                                                                                    Duration
                                                        </div>
                                                        <div class="col-5 text-right">
                                                            00:00:00
                                                                             
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </nav>
                        <div class="header-profile-block">
                            <button class="header-profile-pic background-cover"
                                style="background-image: url(images/executive.jpg);">
                                S
                            </button>
                            <div class="header-profile-nav">
                                <ul class="header-profile-nav-list">
                                    <li>
                                        <a href="#">My profile</a>
                                    </li>
                                    <li>
                                        <a href="#">My Settings</a>
                                    </li>
                                    <li>
                                        <a href="#">Logout</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <button class="mobile-menu-toggle-btn"><i class="mobile-menu-toggle-icon"></i></button>
                    </div>
                </div>
            </header>
            <section class="full-width main-content">

                <nav class="left-sidebar">
                    <div class="main-navigation full-width">
                        <ul class="main-navigation-list full-width">
                            <li>
                                <a href="index.html" class="each-link">
                                    <div class="each-link-icon">
                                        <i class="material-icons">dashboard</i>
                                    </div>
                                    <div class="each-link-text">Dashboard</div>
                                </a>
                            </li>
                            <li>
                                <a href="profile.html" class="each-link">
                                    <div class="each-link-icon">
                                        <i class="material-icons">face</i>
                                    </div>
                                    <div class="each-link-text">Profile</div>
                                </a>
                            </li>
                            <li>
                                <a href="form.html" class="each-link ">
                                    <div class="each-link-icon">
                                        <i class="material-icons">info</i>
                                    </div>
                                    <div class="each-link-text">Form Page</div>
                                </a>
                            </li>
                            <li>
                                <a href="grid.html" class="each-link active">
                                    <div class="each-link-icon">
                                        <i class="material-icons">border_all</i>
                                    </div>
                                    <div class="each-link-text">Grid Page</div>
                                </a>
                            </li>
                            <li>
                                <a href="popup.html" class="each-link">
                                    <div class="each-link-icon">
                                        <i class="material-icons">launch</i>
                                    </div>
                                    <div class="each-link-text">Popup Page</div>
                                </a>
                            </li>
                            <li>
                                <a href="push-notifications.html" class="each-link">
                                    <div class="each-link-icon">
                                        <i class="material-icons">arrow_right_alt</i>
                                    </div>
                                    <div class="each-link-text">Push Notifications</div>
                                </a>
                            </li>
                            <li>
                                <a href="push-notifications.html" class="each-link">
                                    <div class="each-link-icon">
                                        <i class="material-icons">format_align_right</i>
                                    </div>
                                    <div class="each-link-text">Right Sidebar</div>
                                </a>
                            </li>
                            <li>
                                <a href="order-detail.html" class="each-link">
                                    <div class="each-link-icon">
                                        <i class="material-icons">assignment_turned_in</i>
                                    </div>
                                    <div class="each-link-text">Order Detail Page</div>
                                </a>
                            </li>
                            <li class="">
                                <div class="has-sub-nav-link-block">
                                    <a href="push-notifications.html" class="each-link">
                                        <div class="each-link-icon">
                                            <i class="material-icons">list</i>
                                        </div>
                                        <div class="each-link-text">Other UI Elements</div>
                                    </a>
                                    <button class="has-sub-nav-toggle-btn">
                                        <i class="has-sub-nav-toggle-btn-icon"></i>
                                    </button>
                                </div>
                                <ul class="sub-nav">
                                    <li>
                                        <a href="loading.html" class="each-link">
                                            <div class="each-link-icon">
                                                <i class="material-icons">replay</i>
                                            </div>
                                            <div class="each-link-text">Loading</div>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="">
                                <div class="has-sub-nav-link-block">
                                    <a href="leads.html" class="each-link">
                                        <div class="each-link-icon">
                                            <i class="material-icons">scatter_plot</i>
                                        </div>
                                        <div class="each-link-text">Dummy</div>
                                    </a>
                                    <button class="has-sub-nav-toggle-btn">
                                        <i class="has-sub-nav-toggle-btn-icon"></i>
                                    </button>
                                </div>
                                <ul class="sub-nav">
                                    <li>
                                        <a href="#" class="each-link">
                                            <div class="each-link-icon">
                                                <i class="material-icons">equalizer</i>
                                            </div>
                                            <div class="each-link-text">Dummy Sub Link 1</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="each-link">
                                            <div class="each-link-icon">
                                                <i class="material-icons">equalizer</i>
                                            </div>
                                            <div class="each-link-text">Dummy Sub Link 2</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="each-link">
                                            <div class="each-link-icon">
                                                <i class="material-icons">equalizer</i>
                                            </div>
                                            <div class="each-link-text">Dummy Sub Link 3</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="each-link">
                                            <div class="each-link-icon">
                                                <i class="material-icons">equalizer</i>
                                            </div>
                                            <div class="each-link-text">Dummy Sub Link 4</div>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
                <div class="content">
                    <div class="full-width bg-light position-relative h-100 p-2">
                        <div class="full-width  bg-white ">
                            <div class="table-responsive bg-white pr-05">
                                <%--<asp:GridView ID="gdProducts" runat="server" AutoGenerateColumns="false">
                                            <Columns>
                                                <asp:BoundField DataField="ProductId" HeaderText="ProductId" />
                                                <asp:BoundField DataField="Name" HeaderText="Name" />
                                                 <asp:BoundField DataField="Description" HeaderText="Description" />
                                                 <asp:BoundField DataField="Amount" HeaderText="Amount" />
                                                <asp:BoundField DataField="CountryID" HeaderText="Country" />
                                            </Columns>
                                        </asp:GridView>--%>
                                <table class="table table-bordered mb-0 font-size-sm">
                                    <thead>
                                        <tr>
                                            <th style="width: 225px;">Name</th>
                                            <th style="width: 420px;">Description </th>
                                            <th style="width: 180px;">Country </th>
                                            <th style="width: 150px;">Amount </th>
                                            <th>Edit </th>
                                             <th >Delete </th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="table-responsive max-height-300">
                                <table class="table table-bordered mb-0">
                                    <asp:DataList ID="dlProducts" runat="server">
                                        <ItemTemplate>
                                            <tbody style="border-bottom:groove;width:100%">
                                                <tr>
                                                     <td style="width: 225px;" hidden="hidden"><%# Eval("ProductId") %></td>
                                                    <td style="width: 225px;"><%# Eval("Name") %></td>
                                                    <td style="width: 430px;"><%# Eval("Description") %></td>
                                                    <td style="width: 250px;"><%# Eval("Country") %></td>
                                                    <td style="width: 150px;"><%# Eval("Amount") %> </td>
                                                    <td><asp:Button ID="btnEdit" runat="server" Text="Edit" CommandName="Edit" CommandArgument='<%# Eval("ProductId") %>' OnCommand="btnEdit_Command" /></td>
                                                    <td><asp:Button ID="btnDelete" runat="server" Text="Delete" CommandName="Delete" CommandArgument='<%# Eval("ProductId") %>' OnCommand="btnDelete_Command"/></td>
                                                </tr>
                                            </tbody>
                                        </ItemTemplate>
                                    </asp:DataList>
                                </table>
                            </div>

                        </div>
                        <%--<div class="full-width bg-white mt-4">
                                    <div class="full-width border-bottom pb-1 mb-1">
                                          <div class="row">
                                                <div class="col-6 col-lg-5">
                                                      <div class="full-width px-2 py-1 font-size-md font-weight-bold">
                                                            Title of the table runs here like this</div>
                                                </div>
                                                <div class="col-6 col-lg-7 text-right">
                                                      <div class="full-width text-right pt-1 pr-1">
                                                            <div class="float-left w-60">
                                                                  <div class="input-group">
                                                                        <input type="search" class="form-control">
                                                                        <button class="btn btn-secondary px-1"
                                                                              type="button"><i
                                                                                    class="material-icons">search</i></button>
                                                                  </div>
                                                            </div>
                                                            <div class="float-left w-40 pl-2">
                                                                  <select name="" id="" class="form-control">
                                                                        <option value="">Select Filter</option>
                                                                        <option value="">Filter 1</option>
                                                                        <option value="">Filter 2</option>
                                                                        <option value="">Filter 3</option>
                                                                  </select>
                                                            </div>
                                                      </div>
                                                </div>
                                          </div>
                                    </div>
                                    <div class="full-width">
                                          <table class="table table-striped w-auto">
                                                <thead>
                                                      <tr>
                                                            <th>Sl.No</th>
                                                            <th>Description</th>
                                                            <th class="text-center">Qty.</th>
                                                            <th class="text-right">Price</th>
                                                            <th class="text-right">Discount</th>
                                                            <th class="text-right">Total</th>
                                                      </tr>
                                                </thead>
                                                <tbody>
                                                      <tr>
                                                            <td> 1 </td>
                                                            <td style="max-width: 150px">
                                                                  <div class="full-width ellipsis">Lorem ipsum dolor sit
                                                                        amet consectetur adipisicing elit.
                                                                        Exercitationem beatae assumenda mollitia eos,
                                                                        sit dolore, nam molestiae, molestias vel velit
                                                                        cum. Blanditiis reprehenderit amet maxime vero
                                                                        illum pariatur, nisi voluptatem.</div>
                                                            </td>
                                                            <td class="text-center">5</td>
                                                            <td class="text-right">$120</td>
                                                            <td class="text-right">$10</td>
                                                            <td class="text-right">$110</td>
                                                      </tr>
                                                      <tr>
                                                            <td> 1 </td>
                                                            <td> lorem </td>
                                                            <td style="max-width: 150px" class="text-center">
                                                                  <div class="full-width ellipsis">Lorem ipsum dolor sit
                                                                        amet consectetur adipisicing elit. Nisi ab dolor
                                                                        iure modi eaque laudantium voluptates debitis id
                                                                        rem adipisci? Eveniet commodi corrupti in illum,
                                                                        rerum voluptas assumenda dolorum sapiente.</div>
                                                            </td>
                                                            <td class="text-right">$120</td>
                                                            <td class="text-right">$10</td>
                                                            <td class="text-right">$110</td>
                                                      </tr>
                                                      <tr>
                                                            <td> 1 </td>
                                                            <td> lorem </td>
                                                            <td class="text-center">5</td>
                                                            <td class="text-right">$120</td>
                                                            <td class="text-right">$10</td>
                                                            <td class="text-right">$110</td>
                                                      </tr>
                                                      <tr>
                                                            <td> 1 </td>
                                                            <td> lorem </td>
                                                            <td class="text-center">5</td>
                                                            <td class="text-right">$120</td>
                                                            <td class="text-right">$10</td>
                                                            <td class="text-right">$110</td>
                                                      </tr>
                                                      <tr>
                                                            <td> 1 </td>
                                                            <td> lorem </td>
                                                            <td class="text-center">5</td>
                                                            <td class="text-right">$120</td>
                                                            <td class="text-right" style="max-width: 150px;">
                                                                  <div class="full-width ellipsis">Lorem ipsum dolor sit
                                                                        amet consectetur adipisicing elit.
                                                                        Exercitationem beatae assumenda mollitia eos,
                                                                        sit dolore, nam molestiae, molestias vel velit
                                                                        cum. Blanditiis reprehenderit amet maxime vero
                                                                        illum pariatur, nisi voluptatem.</div>
                                                            </td>
                                                            <td class="text-right">$110</td>
                                                      </tr>
                                                      <tr>
                                                            <td> 1 </td>
                                                            <td> lorem </td>
                                                            <td class="text-center">5</td>
                                                            <td class="text-right">$120</td>
                                                            <td class="text-right">$10</td>
                                                            <td class="text-right">$110</td>
                                                      </tr>
                                                      <tr>
                                                            <td> 1 </td>
                                                            <td> lorem </td>
                                                            <td class="text-center">5</td>
                                                            <td class="text-right">$120</td>
                                                            <td class="text-right">$10</td>
                                                            <td class="text-right" style="max-width: 150px;">
                                                                  <div class="full-width ellipsis">Lorem ipsum dolor sit
                                                                        amet consectetur adipisicing elit.
                                                                        Exercitationem beatae assumenda mollitia eos,
                                                                        sit dolore, nam molestiae, molestias vel velit
                                                                        cum. Blanditiis reprehenderit amet maxime vero
                                                                        illum pariatur, nisi voluptatem.</div>
                                                            </td>
                                                      </tr>
                                                      <tr>
                                                            <td> 1 </td>
                                                            <td> lorem </td>
                                                            <td class="text-center">5</td>
                                                            <td class="text-right">$120</td>
                                                            <td class="text-right">$10</td>
                                                            <td class="text-right">$110</td>
                                                      </tr>
                                                      <tr>
                                                            <td> 1 </td>
                                                            <td> lorem </td>
                                                            <td class="text-center">5</td>
                                                            <td class="text-right">$120</td>
                                                            <td class="text-right">$10</td>
                                                            <td class="text-right">$110</td>
                                                      </tr>
                                                      <tr>
                                                            <td> 1 </td>
                                                            <td> lorem </td>
                                                            <td class="text-center">5</td>
                                                            <td class="text-right">$120</td>
                                                            <td class="text-right">$10</td>
                                                            <td class="text-right">$110</td>
                                                      </tr>
                                                      <tr>
                                                            <td> 1 </td>
                                                            <td> lorem </td>
                                                            <td class="text-center">5</td>
                                                            <td class="text-right">$120</td>
                                                            <td class="text-right">$10</td>
                                                            <td class="text-right">$110</td>
                                                      </tr>
                                                      <tr>
                                                            <td> 1 </td>
                                                            <td> lorem </td>
                                                            <td class="text-center">5</td>
                                                            <td class="text-right">$120</td>
                                                            <td class="text-right">$10</td>
                                                            <td class="text-right">$110</td>
                                                      </tr>
                                                      <tr>
                                                            <td> 1 </td>
                                                            <td> lorem </td>
                                                            <td class="text-center">5</td>
                                                            <td class="text-right">$120</td>
                                                            <td class="text-right">$10</td>
                                                            <td class="text-right">$110</td>
                                                      </tr>
                                                      <tr>
                                                            <td> 1 </td>
                                                            <td> lorem </td>
                                                            <td class="text-center">5</td>
                                                            <td class="text-right">$120</td>
                                                            <td class="text-right">$10</td>
                                                            <td class="text-right">$110</td>
                                                      </tr>
                                                      <tr>
                                                            <td> 1 </td>
                                                            <td> lorem </td>
                                                            <td class="text-center">5</td>
                                                            <td class="text-right">$120</td>
                                                            <td class="text-right">$10</td>
                                                            <td class="text-right">$110</td>
                                                      </tr>
                                                      <tr>
                                                            <td> 1 </td>
                                                            <td> lorem </td>
                                                            <td class="text-center">5</td>
                                                            <td class="text-right">$120</td>
                                                            <td class="text-right">$10</td>
                                                            <td class="text-right">$110</td>
                                                      </tr>
                                                      <tr>
                                                            <td> 1 </td>
                                                            <td> lorem </td>
                                                            <td class="text-center">5</td>
                                                            <td class="text-right">$120</td>
                                                            <td class="text-right">$10</td>
                                                            <td class="text-right">$110</td>
                                                      </tr>
                                                      <tr>
                                                            <td> 1 </td>
                                                            <td> lorem </td>
                                                            <td class="text-center">5</td>
                                                            <td class="text-right">$120</td>
                                                            <td class="text-right">$10</td>
                                                            <td class="text-right">$110</td>
                                                      </tr>
                                                      <tr>
                                                            <td> 1 </td>
                                                            <td> lorem </td>
                                                            <td class="text-center">5</td>
                                                            <td class="text-right">$120</td>
                                                            <td class="text-right">$10</td>
                                                            <td class="text-right">$110</td>
                                                      </tr>
                                                      <tr>
                                                            <td> 1 </td>
                                                            <td> lorem </td>
                                                            <td class="text-center">5</td>
                                                            <td class="text-right">$120</td>
                                                            <td class="text-right">$10</td>
                                                            <td class="text-right">$110</td>
                                                      </tr>
                                                      <tr>
                                                            <td> 1 </td>
                                                            <td> lorem </td>
                                                            <td class="text-center">5</td>
                                                            <td class="text-right">$120</td>
                                                            <td class="text-right">$10</td>
                                                            <td class="text-right">$110</td>
                                                      </tr>
                                                      <tr>
                                                            <td> 1 </td>
                                                            <td> lorem </td>
                                                            <td class="text-center">5</td>
                                                            <td class="text-right">$120</td>
                                                            <td class="text-right">$10</td>
                                                            <td class="text-right">$110</td>
                                                      </tr>
                                                </tbody>
                                          </table>
                                    </div>
                              </div>--%>
                    </div>
                </div>
       <%-- </div>--%>
        </section>
        </div>
        <script src="js/bootstrap.bundle.min.js"></script>
        <script src="js/common.js"></script>
    </form>
</body>

</html>
