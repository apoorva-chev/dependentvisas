﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static Dependent_Visas.Translayer;

namespace Dependent_Visas
{
    public partial class Login : System.Web.UI.Page
    {
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
    
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                CartCount();
            }
        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {
            if (fileUpload1.HasFile)
            {
                string[] validFileTypes = { "png", "doc", "docx" };
                string ext = System.IO.Path.GetExtension(fileUpload1.FileName);
                //string ext = System.IO.Path.GetExtension(fileUpload1.PostedFile.FileName);
                bool isValidFile = false;
                for (int i = 0; i < validFileTypes.Length; i++)
                {
                    if (ext == "." + validFileTypes[i])
                    {
                        isValidFile = true;
                        break;
                    }
                }
                if (!isValidFile)
                {
                    lblfile.ForeColor = System.Drawing.Color.Red;
                    lblfile.Text = "Invalid File. Please upload a File with extension " +
                                   string.Join(",", validFileTypes);
                    return;
                }
            }
            //else
            //{
            //    lblfile.ForeColor = System.Drawing.Color.Green;
            //    lblfile.Text = "File uploaded successfully.";
            //}


            Registration objregistration = new Registration();
            objregistration.Name = txtName.Value;
            objregistration.Email = txtEmail.Value;
            if (!string.IsNullOrEmpty(txtPhone.Text))
                objregistration.ContactNo = Convert.ToInt64(txtPhone.Text);
            objregistration.Password = txtPassword.Value;
            if (txtEmail.Value != string.Empty)
            {
                dt = BLL.Validate(objregistration);
                if (dt.Rows.Count > 0)
                {
                    Response.Write("<script language=javascript>alert(EmailId already Exists')</script>");
                    return;
                }
            }

            string FileName = "";
            if (fileUpload1.HasFile)
            {

                FileName = Path.GetFileName(fileUpload1.FileName);
                fileUpload1.SaveAs(Server.MapPath("~/Documents/") + FileName);
            }


            objregistration.RegisterUpload = FileName;
            bool status = Convert.ToBoolean(BLL.Registration(objregistration));
            if (status == true)
            {
                Response.Write("<script language=javascript>alert('Registration Successful')</script>");
                clearcontrols();
            }

            else
                Response.Write("<script language=javascript>alert('Not Registered')</script>");
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            clearcontrols();
        }

        public void clearcontrols()
        {
            txtName.Value = string.Empty;
            txtEmail.Value = string.Empty;
            txtPhone.Text = string.Empty;
            txtPassword.Value = string.Empty;
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            if (txtEmailId.Value != string.Empty && txtPwd.Value != string.Empty)
            {
                Registration objregistration = new Registration();
                objregistration.Email = txtEmailId.Value;
                objregistration.Password = txtPwd.Value;
                dt = BLL.Login(objregistration);
                if (dt.Rows.Count > 0)
                {
                    Session["UserName"] = dt.Rows[0]["Name"];
                    Session["UserId"] = dt.Rows[0]["RegistrationID"];
                    Response.Redirect("~/Products.aspx");
                }
                else
                    Response.Write("<script language=javascript>alert(Invalid UserName or Password')</script>");
            }
            else
                Response.Write("<script language=javascript>alert(Please enter EmailId and Password')</script>");
        }

        public void CartCount()
        {
            CartDetails objCart = new CartDetails();
            objCart.Operation = "Get";
            objCart.RegistrationId = Convert.ToInt32(Session["UserId"]);
            ds = BLL.GetDetails(objCart);
            if (ds.Tables[0].Rows.Count > 0)
                lblcount.Text = Convert.ToString(ds.Tables[0].Rows[0]["Count"]);
            else
                lblcount.Text = "0";
        }
    }
}