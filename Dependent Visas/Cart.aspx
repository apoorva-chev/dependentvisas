﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Cart.aspx.cs" Inherits="Dependent_Visas.Cart" %>



<%--<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
</body>
</html>--%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Dependent Visas</title>
    <!-- PWA Resources Starts -->
    <meta name="theme-color" content="#1a248f" />
    <link
        rel="icon"
        sizes="192x192"
        href="images/icons/c634d504-b2c5-a929-eaea-0974e25a3fe5.webPlatform.png" />
    <link rel="manifest" href="manifest.json" />
    <!-- PWA Resources Starts -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <link
        href="https://fonts.googleapis.com/css?family=PT+Sans:400,700&display=swap"
        rel="stylesheet" />
    <link
        rel="stylesheet"
        href="https://cdn.materialdesignicons.com/4.7.95/css/materialdesignicons.min.css" />
    <link rel="stylesheet" href="css/dependent-visas.css" />
    <script src="js/jquery.min.js"></script>

</head>
<body>
    <form id="form1" runat="server">
        <header class="full-width header border-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-5 col-md-2">
                        <div class="full-width branding-block">
                            <a
                                href="index.html"
                                class="branding d-block"
                                title="Dependent Visas">
                                <img
                                    src="images/dependent-visas-logo.svg"
                                    alt="Depdent Visas"
                                    title="Dependent Visas" />
                            </a>
                        </div>
                    </div>
                    <div class="col-7 col-md-10">
                        <div class="full-width mobile-menu-btn-nav-block">
                            <nav class="main-navigation-block">
                                <ul class="main-navigation list-unstyled mb-0">
                                    <li>
                                        <a href="Home.aspx" class="each-nav-link">Home
                    </a>
                                    </li>
                                    <li class="">
                                        <a href="products.aspx" class="each-nav-link active">Products</a>
                                    </li>
                                    <li>
                                        <a href="about-us.html" class="each-nav-link">About us</a>
                                    </li>
                                    <li>
                                        <a href="faqs.html" class="each-nav-link">FAQs</a>
                                    </li>
                                    <li>
                                        <a href="contact-us.html" class="each-nav-link">Contact us</a>
                                    </li>
                                    <li class="login-register-block ">
                                        <a href="Login.aspx" class="login-register-btn">Login / Signup</a>
                                    </li>
                                </ul>
                            </nav>
                            <a href="cart.html" class="header-cart-btn mr-1 mr-md-0 ml-md-1">
                                <i class="mdi mdi-cart"></i>
                                <span class="header-cart-count">
                                    <asp:Label ID="lblcount" runat="server"></asp:Label></span>
                            </a>
                            <button class="mobile-menu-toggle-btn">
                                <i class="mobile-menu-toggle-icon"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section class="full-width main-content cart py-2">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2">
                        <div class="full-width heading-md pb-15">
                            <div class="float-left w-30">
                                My Cart
             
                            </div>
                            <div class="float-right w-70 text-right">
                                <asp:Button class="btn btn-primary px-4" ID="btnReview" runat="server" Text="Review & Checkout" OnClick="btnCheckOut_Click"></asp:Button>
                            </div>
                        </div>
                        <div class="full-width cart-list-block mt-2">
                            <asp:DataList ID="dlCart" runat="server" RepeatColumns="4">
                                <ItemTemplate>
                                    <ul class="cart-items-list full-width list-unstyled mb-0">

                                        <%--<li class="">--%>
                                        <div class="each-cart-item full-width">
                                          <%--  <span class="each-cart-item-count">1</span>--%>
                                            <div class="each-cart-title-info-block">
                                                 <div class="each-cart-title full-width heading-sm" hidden="hidden" >
                                                    <%#Eval("ProductId")%>
                                                </div>
                                                <div class="each-cart-title full-width heading-sm">
                                                    <%#Eval("Name")%>
                                                    <%-- Dependent visa name runs here like this--%>
                                                    <%--  <asp:Label ID="lblName" runat="server"></asp:Label>--%>
                                                </div>
                                                <div
                                                    class="each-cart-desc full-width font-size text-gray text-truncate">
                                                    <%#Eval("Description")%>
                                                    <%--Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Quia libero qui, similique dolorem laboriosam quis harum
                        laudantium impedit ullam eos. Repudiandae earum natus
                        cum odio blanditiis incidunt, dolor deleniti
                        reprehenderit.--%>
                                                    <%--<asp:Label ID="lblDescription" runat="server"></asp:Label>--%>
                                                </div>
                                            </div>
                                            <div class="each-cart-item-actions full-width">
                                                <div class="each-cart-items-count">
                                                   <%-- <asp:Label ID="lblQty" runat="server"></asp:Label>--%>
                                                    <span class="qty-text">Qty:</span>
                                                  <%--  <span class="qty-select-box">
                                                        <select
                                                            name=""
                                                            id=""
                                                            class="form-control form-control-sm d-inline-block">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                        </select>
                                                    </span>--%>
                                                    <span class="qty-select-box">
                                                    <asp:DropDownList class="form-control form-control-sm d-inline-block" ID="ddlQunatity" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlQty_SelectedIndexChanged">
                                                        <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                                        <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                                        <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                                        <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                                    </asp:DropDownList></span>
                                                </div>
                                                <div class="each-cart-item-delete">
                                                    <asp:button class="delete-item-btn" runat="server" Text="Delete" ID="btnDelete"  CommandArgument='<%#Eval("ProductId")%>' OnCommand="btnDelete_Command">
                                                    </asp:button>
                                                </div>
                                                <div class="each-cart-item-price">
                                                    <i class="mdi mdi-currency-inr"></i><%#Eval("Amount")%>
                                                    <%--<asp:Label ID="lblAmount" runat="server"></asp:Label>--%>
                                                </div>
                                            </div>
                                        </div>
                                        <%--</li>--%>
                                        <%--<li class="">
                  <div class="each-cart-item full-width">
                    <span class="each-cart-item-count">3</span>
                    <div class="each-cart-title-info-block">
                      <div class="each-cart-title full-width heading-sm">
                        Dependent visa name runs here like this
                      </div>
                      <div
                        class="each-cart-desc full-width font-size text-gray text-truncate"
                      >
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Quia libero qui, similique dolorem laboriosam quis harum
                        laudantium impedit ullam eos. Repudiandae earum natus
                        cum odio blanditiis incidunt, dolor deleniti
                        reprehenderit.
                      </div>
                    </div>
                    <div class="each-cart-item-price">
                      <i class="mdi mdi-currency-inr"></i> 15,000
                    </div>
                  </div>
                </li>--%>
                                    </ul>
                                </ItemTemplate>
                            </asp:DataList>
                            <div class="full-width total-block">
                                <div class="total-outer-block">
                                    <div class="each-total-row">
                                        <div class="each-total-title">Sub Total:</div>
                                        <div class="each-total-number">
                                            <span><i class="mdi mdi-currency-inr"></i></span>
                                            <asp:Label ID="lblSubTotal" runat="server"></asp:Label>

                                        </div>
                                    </div>
                                    <div class="each-total-row">
                                        <div class="each-total-title">Tax:</div>
                                        <div class="each-total-number">
                                            <span><i class="mdi mdi-currency-inr"></i></span>
                                            <asp:Label ID="lblTax" runat="server"></asp:Label>

                                        </div>
                                    </div>
                                    <div class="each-total-row grand-total">
                                        <div class="each-total-title">Total:</div>
                                        <div class="each-total-number">
                                            <asp:Label ID="lblTotal" runat="server"></asp:Label>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="full-width mt-2 text-right">
                                <asp:Button class="btn btn-primary px-4" runat="server" ID="btnCheckOut" Text="Review & Checkout" OnClick="btnCheckOut_Click"></asp:Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <footer class="footer full-width py-3 bg-light">
            <div class="full-width">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-6 col-lg-3 col-xl-2 mb-3 mb-lg-0">
                            <div class="full-width footer-navigation-block">
                                <a href="index.html" class="each-footer-nav">Home</a>
                                <a href="products.html" class="each-footer-nav">Products</a>
                                <a href="about-us.html" class="each-footer-nav">About us</a>
                                <a href="faqs.html" class="each-footer-nav">FQAs</a>
                                <a href="contact-us.html" class="each-footer-nav">Contact us</a>
                                <a href="terms-conditions.html" class="each-footer-nav">Terms &amp; Conditions</a>
                                <a href="privacy-policy.html" class="each-footer-nav">Privacy Policy</a>
                                <a href="refund-policy.html" class="each-footer-nav">Refund Policy</a>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-3 col-xl-3 mb-3 mb-lg-0">
                            <div class="full-width footer-navigation-block">
                                <a href="products.html" class="each-footer-nav">Canada Dependent Visa</a>
                                <a href="products.html" class="each-footer-nav">Germany Dependent Visa</a>
                                <a href="products.html" class="each-footer-nav">Uk Dependent Visa</a>
                                <a href="products.html" class="each-footer-nav">Canada Dependent Visa</a>
                                <a href="products.html" class="each-footer-nav">Germany Dependent Visa</a>
                                <a href="products.html" class="each-footer-nav">Uk Dependent Visa</a>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-3 col-xl-3 mb-3 mb-lg-0">
                            <div class="full-width footer-navigation-block">
                                <a href="products.html" class="each-footer-nav">Dependent Visa for Spouse</a>
                                <a href="products.html" class="each-footer-nav">Dependent Visa for Kids</a>
                                <a href="products.html" class="each-footer-nav">Dependent Visa for Parents</a>
                            </div>
                        </div>

                        <div class="col-12 col-md-6 col-lg-3 col-xl-4 mb-3 mb-lg-0">
                            <div class="full-width subscribe-social-block">
                                <div class="full-width pb-1 font-size-md subscribe-heading">
                                    Subscribe for latest updates
               
                                </div>
                                <div class="subscribe-block full-width mb-3">
                                    <input type="email" class="subscribe-input form-control" />
                                    <button class="subscribe-button btn-primary">
                                        Subscribe
                 
                                    </button>
                                </div>
                                <div
                                    class="social-block full-width font-size-md subscribe-heading py-1">
                                    Follow us on
               
                                </div>
                                <div class="social-list-block full-width">
                                    <a
                                        href="#"
                                        class="footer-each-social-link facebook"
                                        target="_blank"><i class="mdi mdi-facebook"></i></a>
                                    <a
                                        href="#"
                                        class="footer-each-social-link twitter"
                                        target="_blank"><i class="mdi mdi-twitter"></i></a>
                                    <a
                                        href="#"
                                        class="footer-each-social-link instagram"
                                        target="_blank"><i class="mdi mdi-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="full-width copy-rights-block text-center mt-2">
                <span class="copy-rights-text bg-light">Copy Rights &copy; 2019&nbsp;&nbsp;|&nbsp;&nbsp;<a href="#">www.dependentvisas.com</a></span>
            </div>
        </footer>

        <script src="js/common.js"></script>
        <script src="js/anime.min.js"></script>
        <script>
            $(document).ready(function () {
                /* Products display animation */
                anime({
                    targets: ".products-list li",
                    opacity: [0, 1],
                    delay: anime.stagger(100)
                });
            });
    </script>
    </form>
</body>
</html>

