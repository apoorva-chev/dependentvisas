﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static Dependent_Visas.Translayer;

namespace Dependent_Visas
{
    public partial class Home : System.Web.UI.Page
    {
        DataTable dt = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            LoadCountryCode();
            AllProductDetails();
            SpouseVisas();
            KidsVisas();
            MostViewed();
        }

        protected void btnEnquiry_Click(object sender, EventArgs e)
        {
            if (chkTerms.Checked == true)
            {
                Enquiry objenquiry = new Enquiry();
            objenquiry.Name = txtName.Value;
            objenquiry.Email = txtEmail.Value;
            objenquiry.ContactNo = Convert.ToInt64(txtContact.Value);
            bool status = Convert.ToBoolean(BLL.Enquiry(objenquiry));
            if (status == true)
            {
                Response.Write("<script language=javascript>alert('Enquiry submitted Successfully')</script>");
                clearcontrols();
            }
            else
                Response.Write("<script language=javascript>alert('Enquiry Not Submitted')</script>");
            }
            else
                Response.Write("<script language=javascript>alert('Please accept Terms and Conditions for Enquiry')</script>");
        }

        public void clearcontrols()
        {
            txtName.Value = string.Empty;
            txtEmail.Value = string.Empty;
            txtContact.Value = string.Empty;
        }

        public void LoadCountryCode()
        {
            dt = BLL.CountryCode();
            if(dt.Rows.Count>0)
            {
                ddlCountry.DataTextField = "Code";
                ddlCountry.DataValueField = "id";
                ddlCountry.DataSource = dt;
                //ddlCountry.SelectedIndex = 98;
                ddlCountry.DataBind();
            }
            
        }
        public void AllProductDetails()
         {
             Product objproduct = new Product();
             objproduct.Action = "AllProducts";
             dt = BLL.GetProduct(objproduct);
            if (dt.Rows.Count > 0)
            {
                dlProductsDisplay.DataSource = dt;
                dlProductsDisplay.DataBind();
            }
        }

        public void SpouseVisas()
        {
            Product objproduct = new Product();
            objproduct.Action = "SpouseVisas";
            dt = BLL.GetProduct(objproduct);
            if (dt.Rows.Count > 0)
            {
                dlSpouseVisas.DataSource = dt;
                dlSpouseVisas.DataBind();
            }
        }
        public void KidsVisas()
        {
            Product objproduct = new Product();
            objproduct.Action = "KidsVisas";
            dt = BLL.GetProduct(objproduct);
            if (dt.Rows.Count > 0)
            {
                dlKidsVisa.DataSource = dt;
                dlKidsVisa.DataBind();
            }
        }

        public void MostViewed()
        {
            Product objproduct = new Product();
            objproduct.Action = "MostViewed";
            DataTable dt = BLL.GetRelatedVisas(objproduct);
            if (dt.Rows.Count > 0)
            {
                dlMostViewed.DataSource = dt;
                dlMostViewed.DataBind();
            }
        }

    }
}