﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProductPreview.aspx.cs" Inherits="Dependent_Visas.ProductPreview" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<%--<head runat="server">
    <title></title>
</head>--%>
    <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Dependent Visas</title>
    <!-- PWA Resources Starts -->
    <meta name="theme-color" content="#1a248f" />
         <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <link
      rel="icon"
      sizes="192x192"
      href="images/icons/c634d504-b2c5-a929-eaea-0974e25a3fe5.webPlatform.png"
    />
    <link rel="manifest" href="manifest.json" />
    <!-- PWA Resources Starts -->
    <!-- <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <link rel="icon" href="favicon.ico" type="image/x-icon" /> -->
    <link
      href="https://fonts.googleapis.com/css?family=PT+Sans:400,700&display=swap"
      rel="stylesheet"
    />
    <link
      rel="stylesheet"
      href="https://cdn.materialdesignicons.com/4.7.95/css/materialdesignicons.min.css"
    />
    <!-- Editor styles -->
    <link rel="stylesheet" href="editor/css/froala_editor.css">
    <link rel="stylesheet" href="editor/css/froala_style.css">
    <link rel="stylesheet" href="editor/css/plugins/code_view.css">
    <link rel="stylesheet" href="editor/css/plugins/image.css">

    <link rel="stylesheet" href="css/dependent-visas.css" />
    <script src="js/jquery.min.js"></script>
  </head>
<body>
    <form id="form1" runat="server">
    <div>
    <div class="full-width product-detail-info-block">
                <div class="product-detail-basic full-width py-3">
                  <div class="row pb-2">
                    <div class="col-12 col-md-5 col-lg-4">
                      <div class="full-width product-image-block">
                       <%-- <img
                          src="images/product-detail.jpg"
                          alt=""
                          class="img-fluid border"
                        />--%>
                          <img id="imgProduct" runat="server" alt="ProuctImage" class="img-fluid border" />
                      </div>
                    </div>
                    <div class="col-12 col-md-7 col-lg-8">
                      <div class="full-width product-info">
                        <div class="full-width border-bottom pb-1">
                          <h1 class="full-width product-title heading-md">
                            <%--Australia--%>
                              <asp:Label ID="lblProductName" runat="server"></asp:Label>
                          </h1>
                          <%--<div class="product-review-stars full-width">
                            <div class="product-stars-block">
                              <div class="product-stars with-arrow">
                                <i class="mdi mdi-star"></i>
                                <i class="mdi mdi-star"></i>
                                <i class="mdi mdi-star"></i>
                                <i class="mdi mdi-star-half"></i>
                                <i class="mdi mdi-star-outline"></i>
                              </div>
                              <div class="product-stars-percentage-info">
                                <div class="product-stars-block-main">
                                  <div
                                    class="product-stars-percentage-count full-width text-center"
                                  >
                                    3.5 out of 5
                                  </div>
                                  <div
                                    class="product-percentage-block full-width"
                                  >
                                    <div
                                      class="each-product-percentage full-width"
                                    >
                                      <div class="each-product-percentage-text">
                                        5 stars
                                      </div>
                                      <div class="each-product-percentage-fill">
                                        <span style="width: 70%;"></span>
                                      </div>
                                      <div
                                        class="each-product-percentage-count"
                                      >
                                        53%
                                      </div>
                                    </div>
                                    <div
                                      class="each-product-percentage full-width"
                                    >
                                      <div class="each-product-percentage-text">
                                        4 stars
                                      </div>
                                      <div class="each-product-percentage-fill">
                                        <span style="width: 60%;"></span>
                                      </div>
                                      <div
                                        class="each-product-percentage-count"
                                      >
                                        75%
                                      </div>
                                    </div>
                                    <div
                                      class="each-product-percentage full-width"
                                    >
                                      <div class="each-product-percentage-text">
                                        3 stars
                                      </div>
                                      <div class="each-product-percentage-fill">
                                        <span style="width: 60%;"></span>
                                      </div>
                                      <div
                                        class="each-product-percentage-count"
                                      >
                                        63%
                                      </div>
                                    </div>
                                    <div
                                      class="each-product-percentage full-width"
                                    >
                                      <div class="each-product-percentage-text">
                                        2 stars
                                      </div>
                                      <div class="each-product-percentage-fill">
                                        <span style="width: 60%;"></span>
                                      </div>
                                      <div
                                        class="each-product-percentage-count"
                                      >
                                        33%
                                      </div>
                                    </div>
                                    <div
                                      class="each-product-percentage full-width"
                                    >
                                      <div class="each-product-percentage-text">
                                        1 stars
                                      </div>
                                      <div class="each-product-percentage-fill">
                                        <span style="width: 60%;"></span>
                                      </div>
                                      <div
                                        class="each-product-percentage-count"
                                      >
                                        23%
                                      </div>
                                    </div>
                                  </div>
                                  <div
                                    class="product-see-all-reviews full-width text-center"
                                  >
                                    <a href="#">See all 615 reviews</a>
                                  </div>
                                </div>
                              </div>
                            </div>
                        <%--    <div class="product-start-reviews-faqs-count">
                              <a href="#" class="product-start-reviews-count"
                                >759 customers</a
                              ><span class="px-1 text-gray">|</span>
                              <a href="#" class="product-start-reviews-count"
                                >1000+ answered questions</a
                              >
                            </div>
                          </div>--%>
                        </div>
                        <div class="full-width pt-15 product-detail-pricing">
                          <div class="orginal-price text-primary heading-sm">
                            <span class="font-size text-gray">Price:</span>
                            <span class="price-currency-symbol"><i class="mdi mdi-currency-inr"></i></span>
                           <%-- <span class="pricing-number">5,000.00</span>--%>
                              <asp:Label ID="lblPrice" runat="server"></asp:Label>
                          </div>
                        </div>
                        <div class="full-width product-detail-actions-block my-15">
                         <%-- <asp:button class="btn btn-primary px-4 heading-sm" id="btnCartdetails" runat="server" Text="Get Visa" OnClick="btnCartdetails_Click"></asp:button>--%>
                        </div>
                        <div class="full-width product-small-info mb-1 pt-1">
                          <p> <strong>Description:</strong> <br>  
                           <%-- Lorem ipsum, dolor sit amet consectetur adipisicing
                          elit. Quae, dolorum recusandae voluptatum architecto
                          delectus maxime labore perspiciatis nemo.--%>
                        <asp:Label ID="lblDescription" runat="server"></asp:Label>
                          </p>
                        </div>
                        <div class="full-width pt-1">
                          <div class="full-width">
                            <strong>Benefits</strong>
                          </div>
                          <ul class="check-list list-unstyled mb-0 full-width">
                           <%-- <li>--%>
                              <%--Lorem ipsum dolor sit amet consectetur,
                              adipisicing elit. Porro.--%>
                              <asp:Label ID="lblBenefits" runat="server"></asp:Label>
                          <%--  </li>--%>
                          <%--  <li>
                              Praesentium explicabo adipisci iusto sapiente,
                              itaque, cumque voluptatum neque.
                            </li>--%>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <!-- About Depedent Visas Starts -->
                    <div class="col-12 pb-2">
                      <div class="full-width each-product-detail-row">
                        <h2 class="heading-sm font-weight-bold full-width pb-1">
                          About Dependent Visas
                        </h2>
                        <div class="full-width each-product-detail-description">
                          <p>
                          <%--  Lorem ipsum dolor sit amet consectetur, adipisicing
                            elit. Porro, in ex! Exercitationem illo totam neque
                            voluptatum mollitia. Praesentium explicabo adipisci
                            iusto sapiente, itaque, cumque voluptatum neque,
                            magnam voluptate tempora dignissimos?--%>
                             <asp:Label ID="lblDependentVisas" runat="server"></asp:Label>
                          </p>
                          <ul class="mb-0">
                           <%-- <li>--%>
                             <%-- Lorem ipsum dolor sit amet consectetur,
                              adipisicing elit.--%>
                               <%--<asp:Label ID="lblSplitData" runat="server"></asp:Label>
                            </li>--%>
                           <%-- <li>
                              Porro, in ex! Exercitationem illo totam neque
                              voluptatum mollitia.
                            </li>
                            <li>
                              Praesentium explicabo adipisci iusto sapiente,
                              itaque, cumque voluptatum neque, magnam voluptate
                              tempora dignissimos?
                            </li>--%>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <!-- About Depedent Visas Ends -->

                    <!-- Who are eligible Starts -->
                    <div class="col-12 pb-2">
                      <div class="full-width each-product-detail-row">
                        <h2 class="heading-sm font-weight-bold full-width pb-1">
                          Who are eligible
                        </h2>
                        <div class="full-width each-product-detail-description">
                          <ul class="check-list list-unstyled mb-0">
                            <%--<li>--%>
                             <%-- Lorem ipsum dolor sit amet consectetur,
                              adipisicing elit.--%>
                              <asp:Label ID="lblEligible" runat="server"></asp:Label>
                           <%-- </li>--%>
                          <%--  <li>
                              Porro, in ex! Exercitationem illo totam neque
                              voluptatum mollitia.
                            </li>
                            <li>
                              Praesentium explicabo adipisci iusto sapiente,
                              itaque, cumque voluptatum neque, magnam voluptate
                              tempora dignissimos?
                            </li>--%>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <!-- Who are eligible Ends -->

                    <!-- Requirements Starts -->
                    <div class="col-12 pb-2">
                      <div class="full-width each-product-detail-row">
                        <h2 class="heading-sm font-weight-bold full-width pb-1">
                          Requirements
                        </h2>
                        <div class="full-width each-product-detail-description">
                          <ul class="check-list list-unstyled mb-0">
                            <%--<li>--%>
                             <%-- Lorem ipsum dolor sit amet consectetur,
                              adipisicing elit.--%>
                                <asp:Label ID="lblRequirements" runat="server"></asp:Label>
                          <%--  </li>--%>
                           <%-- <li>
                              Porro, in ex! Exercitationem illo totam neque
                              voluptatum mollitia.
                            </li>
                            <li>
                              Praesentium explicabo adipisci iusto sapiente,
                              itaque, cumque voluptatum neque, magnam voluptate
                              tempora dignissimos?
                            </li>--%>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <!-- Requirements Ends -->

                    <!-- Process flow Starts -->
                    <div class="col-12 pb-2">
                      <div class="full-width each-product-detail-row">
                        <h2 class="heading-sm font-weight-bold full-width pb-1">
                          Process flow
                        </h2>
                        <div
                          class="full-width each-product-detail-description mt-3"
                        >
                          <div class="row align-items-stretch">
                            <div
                              class="col-12 col-md-3 mb-3 mb-md-0 pb-3 pb-md-0"
                            >
                              <div >
                               <%-- <span class="process-flow-each-step-number"
                                  >1</span
                                >--%>
                                <span>
                                    <img id="imgprocess" runat="server"  />
                                </span>
                              </div>
                            </div>
                             <%-- width="700" height="300"--%>
                          <%--  <div
                              class="col-12 col-md-3 mb-3 mb-md-0 pb-3 pb-md-0"
                            >
                              <div class="process-flow-each-step">
                                <span class="process-flow-each-step-number"
                                  >2</span
                                >
                                <span class="process-folow-each-step-title"
                                  >Submit Requirements</span
                                >
                              </div>
                            </div>
                            <div
                              class="col-12 col-md-3 mb-3 mb-md-0 pb-3 pb-md-0"
                            >
                              <div class="process-flow-each-step">
                                <span class="process-flow-each-step-number"
                                  >3</span
                                >
                                <span class="process-folow-each-step-title"
                                  >We will process requirements</span
                                >
                              </div>
                            </div>
                            <div class="col-12 col-md-3">
                              <div class="process-flow-each-step">
                                <span class="process-flow-each-step-number"
                                  >4</span
                                >
                                <span class="process-folow-each-step-title"
                                  >Visa is delivered</span
                                >
                              </div>
                            </div>--%>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- Process flow Ends -->

                    
                  </div>
                </div>
              </div>
    </div>
    </form>
</body>
</html>
