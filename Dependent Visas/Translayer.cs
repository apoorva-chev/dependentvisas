﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dependent_Visas
{
    public class Translayer
    {
        public class Registration
        {
            public string RegisteredBy { get; set; }
            public string RegisterUpload { get; set; }
            public string Name { get; set; }
            public string Email { get; set; }
            public Int64 ContactNo { get; set; }
            public string Password { get; set; }
            public Boolean TermsandConditions { get; set; }
        }

        public class Enquiry
        {
            public string Name { get; set; }
            public string Email { get; set; }
            public Int64 ContactNo { get; set; }
        }

        public class CountryCodes
        {
            public int id { get; set; }
            public string code { get; set; }
        }
        public class Product
        {
            public int ProductId { get; set; }
            public string Action { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
            public string Benefits { get; set; }
            public string AboutDependentVisas { get; set; }
            public string Eligibility { get; set; }
            public string Requirements { get; set; }
            public string Processflow { get; set; }
            public int DependencyType { get; set; }
            public int status { get; set; }
            public decimal Amount { get; set; }
            public int CountryId { get; set; }
            public string ImageUrl { get; set; }
            public string CreatedBy { get; set; }
            public string ModifiedBy { get; set; }
        }

        public class CartDetails
        {
            public string Operation { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
            public Int32 ProductId { get; set; }
            public Decimal Amount { get; set; }
            public String InvoiceNo { get; set; }
            public Int32 RegistrationId { get; set; }
            public string CreatedBy { get; set; }
            public DateTime CreatedDate { get; set; }
            public string ModifiedBy { get; set; }
            public DateTime ModifiedDate { get; set; }
        }

        public class paymentdetails
        {
            public string orderRefNo { set; get; }
            public string amount { set; get; }
            public string paymentId { set; get; }
            public string merchantId { set; get; }
            public string transactionId { set; get; }
            public string responseMessage { set; get; }
            public string responseCode { set; get; }
            public string transactionType { set; get; }
            public string transactionDate { set; get; }
            public string currencyCode { set; get; }
            public string paymentMethod { set; get; }
            public string paymentBrand { set; get; }
            public string Paymentstatus { set; get; }
        }

        public  class Question
        {
            public string StrAction { get; set; }
            public string QuestionName { get; set; }
            public string CreatedBy { get; set; }
            public DateTime CreatedDate { get; set; }
            public int ProductID { get; set; }
            public int QuestionID { get; set; }
            public string ModifedBy { get; set; }
            public string IPAddress { get; set; }
        }
        public  class Rating
        {
            public string StrAction { get; set; }
            public int RateID { get; set; }
            public int RatingCount1 { get; set; }
            public int RatingCount2 { get; set; }
            public int RatingCount3 { get; set; }
            public int RatingCount4 { get; set; }
            public int RatingCount5 { get; set; }
            public int ProductID { get; set; }
            public string CreatedBy { get; set; }
            public string IPAddress { get; set; }
        }

        public class MostViewing
        {
            public string Action { get; set; }
            public int Aid { get; set; }
            public int ProductID { get; set; }
            public string ViewedBy { get; set; }
            public string IPAddress { get; set; }
        }
    }
}