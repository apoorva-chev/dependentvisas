$(document).ready(function () {
    /* Mobile Menu Toggle */
    $('.mobile-menu-toggle-btn').click(function () {
        $('body').toggleClass('show-menu');
    });

    /* Password Toggle */
    $('.password-toggle').click(function () {
        if ($(this).parent().hasClass('showPassword')) {
            $(this).prev('.password-input').attr('type', 'password');
            $(this).parent().removeClass('showPassword');
            $(this).find('i').addClass('mdi-eye-off').removeClass('mdi-eye');

        } else {
            $(this).prev('.password-input').attr('type', 'text');
            $(this).parent().addClass('showPassword');
            $(this).find('i').addClass('mdi-eye').removeClass('mdi-eye-off');
        }
    });

    /* Filters List */
    $('.showHiddenList').click(function () {
        var filterList = $(this).parent().find('.filters-list');

        if ($(filterList).hasClass('hideAll')) {
            $(filterList).removeClass('hideAll');
            $(this).addClass('active');
            $(this).html('Show less');
        } else {
            $(filterList).addClass('hideAll');
            $(this).removeClass('active');
            $(this).html('Show more');
        }

    });

});
