﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DependentVisas.Master" AutoEventWireup="true" CodeBehind="Products.aspx.cs" Inherits="Dependent_Visas.Products" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    

      <script src="js/common.js"></script>
      <section class="full-width products main-content py-15">
      <div class="container">
        <div class="row align-items-stretch">
          <div class="col-12 col-md-4 col-lg-3 d-none d-md-block">
            <div class="full-width filters-block">
              <div
                class="full-width filters-title-block font-size text-uppercase text-primary py-1 border-bottom"
              >
                <strong class="">Filters</strong>
              </div>
              <div class="filters-each-section full-width pt-1">
                <div class="filters-each-section-title full-width pb-1">
                  <strong>Country</strong>
                </div>
                <div class="filters-each-section-list-block full-width pb-1">
                  <ul
                    class="filters-section-list list-unstyled mb-0 full-width hideAll filters-list"
                  >
                    <li class="full-width">
                      <label class="checkbox"
                        ><%--<input type="checkbox" class="checkbox-input" /><span
                          class="checkbox-text"
                          >USA</span
                        >--%>
                            <asp:CheckBoxList ID="chkCountry" runat="server" AutoPostBack="true" OnSelectedIndexChanged="chkCountry_SelectedIndexChanged">
                          </asp:CheckBoxList>
                      </label
                      >
                    </li>
                 <%--   <li class="full-width">
                      <label class="checkbox"
                        ><input type="checkbox" class="checkbox-input" /><span
                          class="checkbox-text"
                          >UK</span
                        ></label
                      >
                    </li>
                    <li class="full-width">
                      <label class="checkbox"
                        ><input type="checkbox" class="checkbox-input" /><span
                          class="checkbox-text"
                          >Canada</span
                        ></label
                      >
                    </li>
                    <li class="full-width">
                      <label class="checkbox"
                        ><input type="checkbox" class="checkbox-input" /><span
                          class="checkbox-text"
                          >USA</span
                        ></label
                      >
                    </li>
                    <li class="full-width">
                      <label class="checkbox"
                        ><input type="checkbox" class="checkbox-input" /><span
                          class="checkbox-text"
                          >UK</span
                        ></label
                      >
                    </li>
                    <li class="full-width">
                      <label class="checkbox"
                        ><input type="checkbox" class="checkbox-input" /><span
                          class="checkbox-text"
                          >Canada</span
                        ></label
                      >
                    </li>
                    <li class="full-width">
                      <label class="checkbox"
                        ><input type="checkbox" class="checkbox-input" /><span
                          class="checkbox-text"
                          >USA</span
                        ></label
                      >
                    </li>
                    <li class="full-width">
                      <label class="checkbox"
                        ><input type="checkbox" class="checkbox-input" /><span
                          class="checkbox-text"
                          >UK</span
                        ></label
                      >
                    </li>
                    <li class="full-width">
                      <label class="checkbox"
                        ><input type="checkbox" class="checkbox-input" /><span
                          class="checkbox-text"
                          >Canada</span
                        ></label
                      >
                    </li>
                    <li class="full-width">
                      <label class="checkbox"
                        ><input type="checkbox" class="checkbox-input" /><span
                          class="checkbox-text"
                          >USA</span
                        ></label
                      >
                    </li>
                    <li class="full-width">
                      <label class="checkbox"
                        ><input type="checkbox" class="checkbox-input" /><span
                          class="checkbox-text"
                          >UK</span
                        ></label
                      >
                    </li>
                    <li class="full-width">
                      <label class="checkbox"
                        ><input type="checkbox" class="checkbox-input" /><span
                          class="checkbox-text"
                          >Canada</span
                        ></label
                      >
                    </li>
                    <li class="full-width">
                      <label class="checkbox"
                        ><input type="checkbox" class="checkbox-input" /><span
                          class="checkbox-text"
                          >USA</span
                        ></label
                      >
                    </li>
                    <li class="full-width">
                      <label class="checkbox"
                        ><input type="checkbox" class="checkbox-input" /><span
                          class="checkbox-text"
                          >UK</span
                        ></label
                      >
                    </li>
                    <li class="full-width">
                      <label class="checkbox"
                        ><input type="checkbox" class="checkbox-input" /><span
                          class="checkbox-text"
                          >Canada</span
                        ></label
                      >
                    </li>--%>
                  </ul>
                  <button class="showHiddenList mt-1" type="button">Show more</button>
                </div>
              </div>
              <div class="filters-each-section full-width PB-2">
                <div class="filters-each-section-title full-width pb-1">
                  <strong>Dependents</strong>
                </div>
                <div class="filters-each-section-list-block full-width pb-1">
                  <ul
                    class="filters-section-list list-unstyled mb-0 full-width hideAll filters-list"
                  >
                    <li class="full-width">
                      <label class="checkbox"
                        ><%--<input type="checkbox" class="checkbox-input" /><span
                          class="checkbox-text"
                          >Spouse</span
                        >--%>
                           <asp:CheckBoxList ID="chkDependend"  runat="server" AutoPostBack="true"  OnSelectedIndexChanged="chkDependend_SelectedIndexChanged">
                          </asp:CheckBoxList>
                      </label
                      >
                    </li>
                   <%-- <li class="full-width">
                      <label class="checkbox"
                        ><input type="checkbox" class="checkbox-input" /><span
                          class="checkbox-text"
                          >Parents</span
                        ></label
                      >
                    </li>
                    <li class="full-width">
                      <label class="checkbox"
                        ><input type="checkbox" class="checkbox-input" /><span
                          class="checkbox-text"
                          >Kids</span
                        ></label
                      >
                    </li>--%>
                  </ul>
                  <button class="showHiddenList mt-1" type="button">Show more</button>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 col-md-8 col-lg-9 products-col">
            <div class="full-width">
              <div
                class="full-width heading-md text-secondary products-heading border-bottom"
              >
                Dependent Visas
              </div>
                  <div class="full-width products-list-block pt-2">
                      <asp:DataList ID="dlProductsDisplay" runat="server" RepeatColumns="4">
                    <ItemTemplate>
                        <div class="each-home-section-list-block full-width">
                <ul class="each-home-section-list row align-items-stretch">
                  <li  class="col-12 col-md-6 col-lg-11">
                    <div class="each-product move-product">
                      <a
                        href="productDetails.aspx"
                        class="full-width each-product-link-block"
                      >
                        <div class="each-product-title-block full-width">
                          <span
                            class="each-product-image full-width"
                            style="background-image: url(images/visa-1.jpg);"
                          >
                          </span>
                          <span class="each-product-title-text"
                            ><%#Eval("Name")%></span
                          >
                        </div>
                        <div class="each-product-desc full-width text-justify">
                          <%#Eval("Description")%>
                        </div>
                        <div class="each-product-action full-width text-center">
                            <a href="ProductDetails.aspx?pid=<%#Eval("ProductId")%>">
                          <span class="each-product-btn">
                            Get Visa
                          </span>
                        </div>
                      </a>

                      <div class="each-product-tags-list full-width">
                        <a href="#" class="each-tag">spouse</a>
                        <a href="#" class="each-tag">parents</a>
                        <a href="#" class="each-tag">+2 more</a>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
                    </ItemTemplate>
                </asp:DataList>
                     
                  </div>
              <div class="full-width mt-2 text-right">
                <nav aria-label="Page navigation example">
                  <ul class="pagination justify-content-end">
                    <li class="page-item disabled"><%--<a class="page-link" href="#">Previous</a>--%>
                        <asp:LinkButton ID="lniPrevious" CssClass="page-link"  OnClick="lniPrevious_Click" runat="server">Previous</asp:LinkButton>
                    </li>
                    <li class="page-item active"><%--<a class="page-link" id="pr" runat="server" href="#">1</a>--%>
                        <asp:LinkButton ID="ln1" CssClass="page-link"  OnClick="ln1_Click" runat="server">1</asp:LinkButton>
                    </li>
                    <li class="page-item"><%--<a class="page-link" href="#">2</a>--%>
                        <asp:LinkButton ID="ln2" CssClass="page-link" OnClick="ln2_Click" runat="server">2</asp:LinkButton>
                    </li>
                    <li class="page-item"><%--<a class="page-link" href="#">3</a>--%>
                        <asp:LinkButton ID="ln3" CssClass="page-link" OnClick="ln3_Click" runat="server">3</asp:LinkButton>
                    </li>
                    <li class="page-item"><%--<a class="page-link" href="#">Next</a>--%>
                        <asp:LinkButton ID="lnNext" CssClass="page-link" OnClick="lnNext_Click" runat="server">Next</asp:LinkButton>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>


</asp:Content>
