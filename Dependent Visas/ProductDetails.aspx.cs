﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Services;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Script.Services;
using static Dependent_Visas.Translayer;
using System.Net;

namespace Dependent_Visas
{
    public partial class ProductDetails : System.Web.UI.Page
    {
        decimal iCountRate = 0;

        #region InstanceCreation

        DataTable dt = new DataTable();
        #endregion
        static string strUsername;
        static int iPid;

        static Regex _htmlRegex = new Regex("<.*?>", RegexOptions.Compiled);
        DataTable dtReviewsComments;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["pid"] != null)
                {
                    if (Session["UserName"] != null)
                    {
                        strUsername = Session["UserName"].ToString();
                        hidSession.Value = Session["UserName"].ToString();
                    }
                    //strUsername = "Apporva";
                    //hidSession.Value = "Apporva";
                    iPid = Convert.ToInt16(Request.QueryString["pid"]);
                    LoadCountryCode();
                    ProductDisplay();
                    CartCount();
                    InsertMostViewedProducts();
                    // Question_AnswersDisplay();
                    GetRatingScore();
                }
            }
        }
        void ProductDisplay()
        {
            if (Request.QueryString["pid"] != null)
            {
                BLL.StrAction = "SelectIndividual";
                BLL.IProductID = Convert.ToInt32(Request.QueryString["pid"]);
                DataTable dt = BLL.GetProductDetails();
                if (dt.Rows.Count > 0)
                {
                    lblProductName.Text = dt.Rows[0]["Name"].ToString();
                    imgProduct.Src = "~/Uploads/" + dt.Rows[0]["ImageURL"].ToString();
                    lblPrice.Text = dt.Rows[0]["Amount"].ToString();
                    string Desc = dt.Rows[0]["Description"].ToString();
                    if (Desc.Contains("<br />"))
                        Desc = Desc.Replace("<br />", "</li><li>");
                    lblDescription.Text = Desc;
                    string Benefit = "<li>" + dt.Rows[0]["Benefits"].ToString();
                    if (Benefit.Contains("<br />"))
                        Benefit = Benefit.Replace("<br />", "</li><li>");
                    lblBenefits.Text = Benefit;
                    ViewState["Dependencies"] = dt.Rows[0]["AboutDependentVisas"].ToString();
                    string Dependent = dt.Rows[0]["AboutDependentVisas"].ToString();
                    if (Dependent.Contains("<br />"))
                        Dependent = Dependent.Replace("<br />", "</li><li>");
                    lblDependentVisas.Text = Dependent;
                    string Eligible = "<li>" + dt.Rows[0]["Eligibility"].ToString();
                    if (Eligible.Contains("<br />"))
                        Eligible = Eligible.Replace("<br />", "</li><li>");
                    lblEligible.Text = Eligible;
                    string Requirement = "<li>" + dt.Rows[0]["Requirements"].ToString();
                    if (Requirement.Contains("<br />"))
                        Requirement = Requirement.Replace("<br />", "</li><li>");
                    lblRequirements.Text = Requirement;
                    imgprocess.Src = "~/Uploads/" + dt.Rows[0]["ProcessFlow"].ToString();
                }
                Product objproduct = new Product();
                objproduct.Action = "RelatedVisas";
                objproduct.CountryId = Convert.ToInt32(dt.Rows[0]["CountryID"]);
                objproduct.DependencyType = Convert.ToInt32(dt.Rows[0]["DependencyType"]);
                DataTable dtrelated = BLL.GetRelatedVisas(objproduct);
                if (dtrelated.Rows.Count > 0)
                {
                    //dlrelatedvisas.DataSource = dtrelated;
                   // dlrelatedvisas.DataBind();
                }
            }
        }

        private void InsertMostViewedProducts()
        {
            try
            {
                MostViewing mObj = new MostViewing();
                mObj.Action = "Insert";
                if (strUsername != null || iPid > 0)
                {
                    mObj.ProductID = iPid;
                    mObj.ViewedBy = strUsername;
                }
                mObj.IPAddress = GetIpAddress();
                BLL.InsertMostViewes(mObj);

            }
            catch (Exception ex)
            {
                Response.Write(@"<script language='javascript'>alert('The following errors have occurred: \n" + ex.Message + " .');</script>");
            }
        }

        private static void SplitToLines(string stringToSplit, int maximumLineLength)
        {
            stringToSplit = stringToSplit.Trim();
            var lines = new List<string>();

            while (stringToSplit.Length > 0)
            {
                if (stringToSplit.Length <= maximumLineLength)
                {

                    //lines.Add(stringToSplit);

                    break;
                }

                var indexOfLastSpaceInLine = stringToSplit.Substring(0, maximumLineLength).LastIndexOf(' ');
                lines.Add(stringToSplit.Substring(0, indexOfLastSpaceInLine >= 0 ? indexOfLastSpaceInLine : maximumLineLength).Trim());
                stringToSplit = stringToSplit.Substring(indexOfLastSpaceInLine >= 0 ? indexOfLastSpaceInLine + 1 : maximumLineLength);
            }

            //return lines;
        }

        protected void txtSearch_Changed(object sender, EventArgs e)
        {
            divSelectQuestion.Visible = true;
            divQuestionAnswer.Visible = true;
            Question_AnswersDisplay(txtSearch.Text);

            //if (Session["UserName"] != null)
            //{

            //}

            //List<string> lObj = GetQuestionNames(txtSearch.Text);
            //foreach (var item in lObj)
            //{
            //    txtSearch.Text = item;
            //}
        }

        public void LoadCountryCode()
        {
            dt = BLL.CountryCode();
            if (dt.Rows.Count > 0)
            {
                ddlCountry.DataTextField = "Code";
                ddlCountry.DataValueField = "id";
                ddlCountry.DataSource = dt;
                //ddlCountry.SelectedIndex = 98;
                ddlCountry.DataBind();
            }


        }

        protected void btnEnquiry_Click(object sender, EventArgs e)
        {
            Enquiry objenquiry = new Enquiry();
            objenquiry.Name = txtName.Value;
            objenquiry.Email = txtEmail.Value;
            try
            {
                objenquiry.ContactNo = Convert.ToInt64(txtContact.Value);
            }
            catch { }
            bool status = Convert.ToBoolean(BLL.Enquiry(objenquiry));
            if (status == true)
            {
                Response.Write("<script language=javascript>alert('Enquiry submitted Successfully')</script>");
                clearcontrols();
            }
            else
                Response.Write("<script language=javascript>alert('Enquiry Not Submitted')</script>");
        }

        public void clearcontrols()
        {
            txtName.Value = string.Empty;
            txtEmail.Value = string.Empty;
            txtContact.Value = string.Empty;
        }

        //protected void btnVisa_Click(object sender, EventArgs e)
        //{

        //}

        protected void btnCartdetails_Click(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["UserId"]) != string.Empty)
            {
                int iCount = 0;
                string strInvoiceID = string.Empty;
                CartDetails objCart = new CartDetails();
                objCart.Operation = "GetInvoice";
                dt = BLL.GetInvoice(objCart);
                if (dt.Rows.Count > 0)
                {
                    iCount += 1;
                    strInvoiceID = "ETSI" + "/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + DateTime.Now.Day + iCount;
                }
                else
                {
                    iCount = 1;
                    strInvoiceID = "ETSI" + "/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + DateTime.Now.Day + iCount;
                }

                objCart.Operation = "Insert";
                objCart.Name = lblProductName.Text;
                objCart.Description = lblDescription.Text;
                objCart.Amount = Convert.ToDecimal(lblPrice.Text);
                objCart.ProductId = Convert.ToInt32(Request.QueryString["pid"]);
                objCart.RegistrationId = Convert.ToInt32(Session["UserId"]);
                objCart.InvoiceNo = strInvoiceID;
                bool status = Convert.ToBoolean(BLL.CartDetails(objCart));
                if (status == true)
                {
                    Response.Write("<script language=javascript>alert('Added to Cart')</script>");
                    objCart.Operation = "Get";
                    DataSet ds = BLL.GetDetails(objCart);
                    if (ds.Tables[0].Rows.Count > 0)
                        lblcount.Text = Convert.ToString(ds.Tables[0].Rows[0]["Count"]);
                    else
                        lblcount.Text = "0";
                    clearcontrols();
                }
                else
                    Response.Write("<script language=javascript>alert('Not Added to Cart')</script>");
            }
            else
            {
                Response.Write("<script language=javascript>alert('Please login to add the product to cart')</script>");
                Response.Redirect("Login.aspx");
            }
        }


        //string InvoiceIncrement()
        //{
        //    int iCount = 0;
        //    string strInvoiceID = string.Empty;
        //    Cart objcart = new Cart();

        //    //string strQuery = "select max(id) from tbl_name";
        //    //con.Open();
        //    //SqlCommand cmd = new SqlCommand(strQuery, con);
        //    //object id = cmd.ExecuteScalar();
        //    //con.Close();
        //    if (DbNUll.Value == id)
        //    {
        //        iCount = 1;
        //        strInvoiceID = "ETSI" + "/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + DateTime.Now.Date + iCount;
        //    }
        //    else
        //    {
        //        iCount += 1;
        //        strInvoiceID = "ETSI" + "/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + DateTime.Now.Date + iCount;
        //    }
        //}

        public void CartCount()
        {
            CartDetails objCart = new CartDetails();
            objCart.Operation = "Get";
            objCart.RegistrationId = Convert.ToInt32(Session["UserId"]);
            DataSet ds = BLL.GetDetails(objCart);
            if (ds.Tables[0].Rows.Count > 0)
                lblcount.Text = Convert.ToString(ds.Tables[0].Rows[0]["Count"]);
            else
                lblcount.Text = "0";
        }

        [WebMethod]
        public static string InsertMethod(string Question1)
        {
            Question objquestion = new Question();
            int iCount = 0;
            //SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DependentVisas"].ToString());
            //{
            //    string strValue = string.Empty;
            //    strValue = StripTagsRegexCompiled(Question);

            //        SqlCommand cmd = new SqlCommand("Insert into Questions(Question,CreatedBy,CreatedDate,ProductId) values('" + strValue + "', '" + strUsername + "','"+ DateTime.Now.Date.ToShortDateString() +"',"+ iPid+")", con);
            //        {
            //            con.Open();
            //            cmd.ExecuteNonQuery();
            //            con.Close();
            //            return "True";
            //    }

            //}
            string strValue = string.Empty;
            strValue = StripTagsRegexCompiled(Question1);
            if (string.IsNullOrEmpty(strUsername) != true && iPid > 0)
            {
                DAL dObj = new Dependent_Visas.DAL();

                int iExists = 0;
                string strValidateQuery = "select QuestionID from Questions where Question='" + strValue + "' and  ProductId=" + iPid + "";
                iExists = dObj.ExecuteSingle(strValidateQuery);


                if (iExists > 0)
                {
                    try
                    {
                        objquestion.StrAction = "UpdateQuestion";
                        objquestion.QuestionID = iExists;
                        objquestion.QuestionName = strValue;
                        objquestion.ModifedBy = strUsername;
                        objquestion.ProductID = iPid;
                        objquestion.IPAddress = GetIpAddress();
                        iCount = BLL.Questions(objquestion);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }


                }
                else
                {
                    try
                    {
                        objquestion.StrAction = "InsertQuestion";
                        objquestion.QuestionName = strValue;
                        objquestion.CreatedBy = strUsername;
                        objquestion.CreatedDate = DateTime.Today;
                        objquestion.ProductID = iPid;
                        objquestion.IPAddress = GetIpAddress();
                        iCount = BLL.Questions(objquestion);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

            }
            return "True";

        }
        public static string StripTagsRegexCompiled(string source)
        {
            return _htmlRegex.Replace(source, string.Empty);
        }

        void Question_AnswersDisplay(string strPrefixValue)
        {
            Question objquestion = new Question();
            int iQuestionOne = 0, iQuestionTwo = 0;
            string strQuestionOne = string.Empty, strQuestionTwo = string.Empty;
            DataTable dtQuestion1 = new DataTable();
            DataTable dtQuestion2 = new DataTable();
            objquestion.StrAction = "GetQuestions";
            if (Request.QueryString["pid"] != null)
                objquestion.ProductID = Convert.ToInt16(Request.QueryString["pid"]);
            objquestion.QuestionName = strPrefixValue;
            DataTable dt = BLL.GetQuestions(objquestion);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    if (i == 0)
                    {
                        iQuestionOne = Convert.ToInt16(dt.Rows[i]["QuestionID"]);
                        strQuestionOne = dt.Rows[i]["Question"].ToString();
                        objquestion.StrAction = "FirstQuestionAnswers";
                        objquestion.QuestionID = iQuestionOne;

                        dtQuestion1 = BLL.GetQuestions(objquestion);

                    }
                    else
                    {
                        iQuestionTwo = Convert.ToInt16(dt.Rows[i]["QuestionID"]);
                        strQuestionTwo = dt.Rows[i]["Question"].ToString();
                        objquestion.StrAction = "SecondQuestionAnswers";
                        objquestion.QuestionID = iQuestionTwo;

                        dtQuestion2 = BLL.GetQuestions(objquestion);

                    }
                }//for loop closed of dt table rows

                if (dtQuestion1.Rows.Count > 0)
                {
                    for (int j = 0; j <= dtQuestion1.Rows.Count - 1; j++)
                    {
                        lblQuestion1.Text = strQuestionOne;
                        if (j == 0)
                        {
                            lblAnswer1.Text = dtQuestion1.Rows[j]["Answer"].ToString();
                            lblUsername1.Text = dtQuestion1.Rows[j]["Name"].ToString();
                            lblDate1.Text = dtQuestion1.Rows[0]["CreatedDate"].ToString();
                        }
                        else if (j == 1)
                        {
                            lblAnswer2.Text = dtQuestion1.Rows[j]["Answer"].ToString();
                            lblUsername2.Text = dtQuestion1.Rows[j]["Name"].ToString();
                            lblDate2.Text = dtQuestion1.Rows[0]["CreatedDate"].ToString();
                        }
                        else if (j == 2)
                        {
                            lblAnswer3.Text = dtQuestion1.Rows[j]["Answer"].ToString();
                            lblUsername1Answer.Text = dtQuestion1.Rows[j]["Name"].ToString();
                            lblDateDispaly.Text = dtQuestion1.Rows[0]["CreatedDate"].ToString();
                        }
                    }//for loop of dtQuestion1 is closed
                }//if condition of dtQuestion1 closed
                else
                {
                    divQuestionAnswer.Visible = false;
                    divNoQuestionAnswers.Visible = true;
                    lblError.Text = "There is no Answers for this questions";
                }

                if (dtQuestion2.Rows.Count > 0)
                {
                    for (int j = 0; j <= dtQuestion2.Rows.Count - 1; j++)
                    {
                        lblQuestion2.Text = strQuestionOne;
                        if (j == 0)
                        {
                            lblSecondAnswer1.Text = dtQuestion2.Rows[j]["Answer"].ToString();
                            lblUsername3.Text = dtQuestion2.Rows[j]["Name"].ToString();
                            lblDate3.Text = dtQuestion2.Rows[0]["CreatedDate"].ToString();
                        }
                        else
                        {
                            lblSecondAnswer2.Text = dtQuestion2.Rows[j]["Answer"].ToString();
                            lblUsername4.Text = dtQuestion2.Rows[j]["Name"].ToString();
                            lblDate4.Text = dtQuestion2.Rows[0]["CreatedDate"].ToString();
                        }
                    }//for loop dtQuestion2 is closed

                }//if condition of dtQuestion2 closed
                else
                {
                    divQuestionAnswer.Visible = false;
                    divNoQuestionAnswers.Visible = true;
                    lblError.Text = "There is no Answers for this questions";
                }
            }//if condition closed of dt table
            else
            {
                divQuestionAnswer.Visible = false;
                divNoQuestionAnswers.Visible = true;
                lblError.Text = "There is no questions for this product";
            }

        }//Method Question_AnswersDisplay closed bracket
        //protected void img1_Click(object sender, EventArgs e)
        //{
        //    iCountRate = 1;
        //    InsertRating(iCountRate);
        //}
        //protected void img2_Click(object sender, EventArgs e)
        //{
        //    iCountRate = 2;
        //    InsertRating(iCountRate);
        //}
        //protected void img3_Click(object sender, EventArgs e)
        //{
        //    iCountRate = 3;
        //    InsertRating(iCountRate);
        //}
        //protected void img4_Click(object sender, EventArgs e)
        //{
        //    iCountRate = 4;
        //    InsertRating(iCountRate);
        //}
        //protected void img5_Click(object sender, EventArgs e)
        //{
        //    iCountRate = 5;
        //    InsertRating(iCountRate);
        //}

        //private void InsertRating(decimal iRateCount)
        //{
        //    if (strUsername != null)
        //    {
        //        Rating rObj = new Rating();
        //        rObj.StrAction = "InsertRating";
        //        rObj.RatingCount = iRateCount;

        //        rObj.CreatedBy = strUsername;
        //        if (Request.QueryString["pid"] != null)
        //            rObj.ProductID = iPid;
        //       // rObj.CreatedDate = DateTime.Now;
        //        int iCount = BLL.Rating(rObj);
        //        if (iCount > 0)
        //        {
        //            Response.Write(@"<script language='javascript'>alert('Rating count is inserted : \n .');</script>");
        //        }
        //        else
        //        {
        //            Response.Write(@"<script language='javascript'>alert('Rating count is not inserted: \n .');</script>");
        //        }
        //    }


        //}

        private void GetRatingScore()
        {
            Rating rObj = new Rating();
            string json = string.Empty;
            string RatingCount1 = string.Empty, RatingCount2 = string.Empty, RatingCount3 = string.Empty, RatingCount4 = string.Empty, RatingCount5 = string.Empty;
            dtReviewsComments = new DataTable();
            dtReviewsComments.Columns.Add(new DataColumn("Name", typeof(string)));
            dtReviewsComments.Columns.Add(new DataColumn("ImgFirst", typeof(string)));
            dtReviewsComments.Columns.Add(new DataColumn("ImgSecond", typeof(string)));
            dtReviewsComments.Columns.Add(new DataColumn("ImgThird", typeof(string)));
            dtReviewsComments.Columns.Add(new DataColumn("ImgFourth", typeof(string)));
            dtReviewsComments.Columns.Add(new DataColumn("ImgFifth", typeof(string)));
            //Translayer.Rating.StrAction = "SelectCount";
            rObj.StrAction = "SelectCount";
            //decimal dSum = 0;
            //int iRecordCount = 0;
            //decimal dAverage = 0;
            if (Request.QueryString["pid"] != null)
            {

                rObj.ProductID = iPid;
            }
            DataTable dt = BLL.GetRatings(rObj);
            if (dt.Rows.Count > 0)
            {
                //dvReviews.DataSource = dt;
                //dvReviews.DataBind();



                foreach (DataRow dr in dt.Rows)
                {
                    ViewState["UserName"] = dr["CreatedBy"];
                    string strRateCountOne = DBNull.Value == dr["RatingCount_First"] ? "0" : dr["RatingCount_First"].ToString();
                    string strRateCountTwo = DBNull.Value == dr["RatingCount_Second"] ? "0" : dr["RatingCount_Second"].ToString();
                    string strRateCountThree = DBNull.Value == dr["RatingCount_Three"] ? "0" : dr["RatingCount_Three"].ToString();
                    string strRateCountFour = DBNull.Value == dr["RatingCount_Four"] ? "0" : dr["RatingCount_Four"].ToString();
                    string strRarteCountFive = DBNull.Value == dr["RatingCount_Five"] ? "0" : dr["RatingCount_Five"].ToString();

                    if (Convert.ToInt16(strRateCountOne) == 1)
                    {
                        // dtReviewsComments.Rows.Add(ViewState["UserName"], "~/images/star1.gif", "", "", "", "");
                        RatingCount1 = "~/images/star1.gif";

                    }
                    else
                    {
                        RatingCount1 = "~/images/unstar2.gif";
                    }
                    if (Convert.ToInt16(strRateCountTwo) == 1)
                    {
                        // dtReviewsComments.Rows.Add(ViewState["UserName"], "~/images/star1.gif", "~/images/star2.gif", "", "", "");
                        RatingCount2 = "~/images/star2.gif";
                    }
                    else
                    {
                        RatingCount2 = "~/images/unstar2.gif";
                    }
                    if (Convert.ToInt16(strRateCountThree) == 1)
                    {
                        //dtReviewsComments.Rows.Add(ViewState["UserName"], "~/images/star1.gif", "~/images/star2.gif", "~/images/star3.gif", "", "");
                        RatingCount3 = "~/images/star3.gif";
                    }
                    else
                    {
                        RatingCount3 = "~/images/unstar2.gif";
                    }
                    if (Convert.ToInt16(strRateCountFour) == 1)
                    {
                        // dtReviewsComments.Rows.Add(ViewState["UserName"], "~/images/star1.gif", "~/images/star2.gif", "~/images/star3.gif", "~/images/latestimagefour.gif", "");
                        RatingCount4 = "~/images/latestimagefour.gif";
                    }
                    else
                    {
                        RatingCount4 = "~/images/unstar2.gif";
                    }
                    if (Convert.ToInt16(strRarteCountFive) == 1)
                    {
                        //dtReviewsComments.Rows.Add(ViewState["UserName"], "~/images/star1.gif", "~/images/star2.gif", "~/images/star3.gif", "~/images/latestimagefour.gif", "~/images/fivestar.gif");
                        RatingCount5 = "~/images/fivestar.gif";
                    }
                    else
                    {
                        RatingCount5 = "~/images/unstar2.gif";
                    }

                    //dSum = Convert.ToDecimal(dr["SumCount"]);
                    //iRecordCount = Convert.ToInt32(dr["RecordCount"]);
                    //dAverage = dSum / iRecordCount;
                    //foreach (DataListItem item in dvReviews.Items)
                    //{


                    //    //   TextBox txt = (TextBox)li.FindControl("TextBox1");
                    //    ImageButton imgFirst = (ImageButton)item.FindControl("img1");
                    //    ImageButton imgSecond = (ImageButton)item.FindControl("img2");
                    //    ImageButton imgThree = (ImageButton)item.FindControl("img3");
                    //    ImageButton imgFour = (ImageButton)item.FindControl("img4");
                    //    ImageButton imgFive = (ImageButton)item.FindControl("img5");


                    //    if (Convert.ToInt16(dr["RatingCount"]) == 1)
                    //    {
                    //        imgFirst.Visible = true;
                    //        break;
                    //    }
                    //    else if (Convert.ToInt16(dr["RatingCount"]) == 2)
                    //    {
                    //        imgSecond.Visible = true;
                    //        break;
                    //    }
                    //    else if (Convert.ToInt16(dr["RatingCount"]) == 3)
                    //    {
                    //        imgThree.Visible = true;
                    //        break;
                    //    }
                    //    else if (Convert.ToInt16(dr["RatingCount"]) == 4)
                    //    {
                    //        imgFour.Visible = true;
                    //        break;
                    //    }
                    //    else if (Convert.ToInt16(dr["RatingCount"]) == 5)
                    //    {
                    //        imgFive.Visible = true;
                    //        break;
                    //    }
                    //}//inner foreach loop closed
                    dtReviewsComments.Rows.Add(ViewState["UserName"].ToString(), RatingCount1, RatingCount2, RatingCount3, RatingCount4, RatingCount5);
                }//datalist foreach datareader is closed
                 // json =dAverage.ToString("#.##");
                 //  json += dAverage.ToString("#.##") + " " + "of" + " " + iRecordCount;

                //foreach (DataRow dr in dt.Rows)
                //{

                //   // json += string.Format("Average: {0}, Total: {1}", dr["Average"], dr["Total"]);
                //}

            }
            //dtReviewsComments = new DataTable();
            //dtReviewsComments.Columns.Add(new DataColumn("DisplayDetails", typeof(string)));
            //dtReviewsComments.Columns.Add(new DataColumn("Name", typeof(string)));
            //if (ViewState["UserName"] != null)
            //    dtReviewsComments.Rows.Add(json, ViewState["UserName"].ToString());
            if (dtReviewsComments.Rows.Count > 0)
            {
                dvReviews.DataSource = dtReviewsComments;
                dvReviews.DataBind();
            }
            //  ratingdisplay.InnerText = json;

        }

        [WebMethod]
        public static List<string> GetQuestionsAll(string prefixText)
        {
            //SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DependentVisas"].ToString());
            //con.Open();
            //SqlCommand cmd = new SqlCommand(, con);
            //cmd.Parameters.AddWithValue("@Name", prefixText);
            //SqlDataAdapter da = new SqlDataAdapter(cmd);
            //DataTable dt = new DataTable();
            //da.Fill(dt);
            string strQueryRetrieve = string.Empty;
            strQueryRetrieve = "select * from Questions where Question like '%" + prefixText + "%' and ProductID = " + iPid + "";

            DAL dObj = new Dependent_Visas.DAL();
            DataSet ds = dObj.GetDataSet(strQueryRetrieve);
            List<string> QuestionNames = new List<string>();
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                QuestionNames.Add(ds.Tables[0].Rows[i][1].ToString());
            }
            return QuestionNames;
        }


        public static string GetIpAddress()  // Get IP Address
        {
            string ip = "";
            IPHostEntry ipEntry = Dns.GetHostEntry(GetCompCode());
            IPAddress[] addr = ipEntry.AddressList;
            ip = addr[1].ToString();
            return ip;
        }
        public static string GetCompCode()  // Get Computer Name
        {
            string strHostName = "";
            strHostName = Dns.GetHostName();
            return strHostName;
        }


    }
}
