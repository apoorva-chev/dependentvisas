﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace Dependent_Visas
{
    public class DAL
    {
        ///
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DependentVisas"].ToString());

        public int ExecuteNonQuery(SqlCommand cmd)
        {
            cmd.Connection = con;
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
            con.Open();
            int iCount = cmd.ExecuteNonQuery();
            con.Close();
            return iCount; 
        }

        public string ExecuteScalar(SqlCommand cmd)
        {
            cmd.Connection = con;
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
            con.Open();
            String iCount = cmd.ExecuteScalar().ToString();
            con.Close();
            return iCount;
        }
        public int ExecuteString(string strQuery)
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
            con.Open();
            SqlCommand cmd = new SqlCommand(strQuery, con);
            int iCount = cmd.ExecuteNonQuery();
            con.Close();
            return iCount;
        }
        //public string ExecuteSingle(string strQuery)
        //{
        //    if (con.State == ConnectionState.Open)
        //    {
        //        con.Close();
        //    }
        //    con.Open();
        //    SqlCommand cmd = new SqlCommand(strQuery, con);
        //    string strValue = cmd.ExecuteNonQuery().ToString();
        //    con.Close();
        //    return strValue;

        //}
        public int ExecuteSingle(string strQuery)
        {
            int value = 0;
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
            con.Open();
            SqlCommand cmd = new SqlCommand(strQuery, con);
            try
            {
                value = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch { }
            con.Close();
            return value;

        }

        public DataSet ExecuteDataSet(SqlCommand cmd)
        {
            cmd.Connection = con;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }
        public DataTable ExecuteDataTable(SqlCommand cmd)
        {
            cmd.Connection = con;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        public DataSet GetDataSet(string strQuery)
        {
            SqlDataAdapter da = new SqlDataAdapter(strQuery, con);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }
    }
}