﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Dependent_Visas.Login" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>

        <section class="full-width login-register main-content py-3 bg-light">
            <div class="container">
                <div class="row align-items-stretch">
                    <div class="col-12 col-md-6 col-md-1 login-column">
                        <div
                            class="full-width login-form-block shadow-sm bg-white py-3 px-2">
                            <div class="full-width login-form-heading-block">
                                <div
                                    class="small-heading full-width text-uppercase text-gray pt-1">
                                    <span class="d-inline-block border-bottom border-secondary">New to our Website?</span>
                                </div>
                                <div class="full-width heading-lg">Register for FREE</div>
                                <div class="full-width pt-2 ">
                                    <div class="row">
                                        <div class="col-12 col-md-4 mb-2 mb-md-0">
                                            <a
                                                href="#"
                                                class="py-1 px-1 facebook-login full-width border">
                                                <i class="mdi mdi-facebook-box"></i>Facebook
                      </a>
                                        </div>
                                        <div class="col-12 col-md-4">
                                            <a
                                                href="#"
                                                class="py-1 px-1 google-login full-width border">
                                                <i class="mdi mdi-google"></i>Google
                      </a>
                                        </div>
                                        <div class="col-12 col-md-4">
                                            <a
                                                href="#"
                                                class="py-1 px-1 linkedin-login full-width border">
                                                <i class="mdi mdi-linkedin-box"></i>Linkedin
                      </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="full-width or-dividor text-center pt-2">
                                    OR
               
                                </div>
                                <div class="login-form pt-1 full-width pt-2">
                                    <div class="full-width uploadResume-block">
                                        <form action="#" class="dropzone" id="">
                                            <div class="dz-message needsclick">
                                                <span class="d-md-none">Tap here to upload</span>
                                                <span class="d-none d-md-inline-block">Drop your resume here.</span>
                                                <br />
                                                <span class="note needsclick text-gray">(Upload .doc, .pdf only. and upto 2 MB)</span>
                                            </div>
                                            <div class="fallback" id="myAwesomeDropzone">
                                                <input name="file" type="file" multiple />
                                            </div>
                                        </form>
                                    </div>
                                    <div class="full-width my-2 or-block text-center">
                                        <span>OR</span>
                                    </div>
                                    <ul class="form-list list-unstyled mb-0 pt-1 login-form-list">
                                        <li class="">
                                            <div class="full-width">
                                                  <label>Name</label>
                                                <input type="text" class="form-control full-width" id="txtName" runat="server" />
                                            </div>
                                        </li>
                                        <li>
                                            <div class="full-width">
                                                <label>Email:</label>
                                               
                                                <input type="email" class="form-control full-width" runat="server" id="txtEmail"/>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="full-width">
                                                <label>Contact Number:</label>
                                                <div class="row">
                                                    <div class="col-4 col-md-3">
                                                        <select name="" id="" class="form-control">
                                                            <option value="">+1234 INDIA</option>
                                                            <option value="">123 IND</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-8 col-lg-9 pl-1">
                                                        <input type="tel" class="form-control" runat="server" id="txtContact" />
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="password">
                                            <label>Create Password:</label>
                                            <asp:Label ID="lblcreatepwd" runat="server" Text="Create Password"></asp:Label>
                                            <div class="password-input-block">
                                                <input type="password" class="form-control password-input" id="txtPassword" runat="server" />
                                              
                                                <span class="password-toggle"><i class="mdi mdi-eye-off"></i></span>
                                            </div>
                                            <div
                                                class="password-strength float-left font-size-sm full-width py-1 px-15"
                                                id="pwdsn"
                                                style="display: none;">
                                                <div
                                                    class="password-star-rating-block rating font-size-sm d-flex align-items-center pb-15"
                                                    id="pstrength">
                                                    <span class="mr-1 font-italic">Password strength:</span>
                                                    <i class="mdi mdi-star text-danger"></i>
                                                    <i class="mdi mdi-star text-danger"></i>
                                                    <i class="mdi mdi-star-outline"></i>
                                                    <i class="mdi mdi-star-outline"></i>
                                                    <i class="mdi mdi-star-outline"></i>
                                                </div>
                                                <div class="password-strength-title pb-05">
                                                    Your password must have:
                       
                                                </div>
                                                <ul class="password-strength-list pl-2">
                                                    <li class="text-danger pb-05 show" id="specialcase">One Special Character
                          </li>
                                                    <li class="text-danger pb-05 show" id="upcase">One upper alphabet
                          </li>
                                                    <li class="text-danger pb-05 show" id="lowcase">One lower alphabet
                          </li>
                                                    <li class="text-danger pb-05 show" id="numberlength">One numeric value
                          </li>
                                                    <li class="text-danger show" id="minlength">Minimum length - 8
                          </li>
                                                </ul>
                                            </div>
                                            <div
                                                class="full-width text-danger"
                                                id="signupform_password_errorloc">
                                            </div>
                                        </li>

                                        <li>
                                            <div class="full-width text-gray pb-1">
                                                On submitting, you are agreeing our
                       
                                                <a href="#">Terms &amp; Conditions</a>
                                            </div>
                                            <div class="full-width text-right pt-2">
                                                <asp:button runat="server" Text="Reset" class="btn btn-outline-gray px-2 mr-1" ID="btnReset" OnClick="btnReset_Click">
                                                </asp:button>
                                                <asp:Button class="btn btn-primary px-3" runat="server" ID="btnRegister" Text="Register" OnClick="btnRegister_Click"></asp:Button>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-1 position-relative py-3">
                        <div class="login-or-block">
                            <span>OR</span>
                        </div>
                    </div>
                    <div class="col-12 col-md-5 col-md-1 register-column">
                        <div
                            class="full-width login-form-block shadow-sm bg-white py-3 px-2">
                            <div class="full-width login-form-heading-block">
                                <div
                                    class="small-heading full-width text-uppercase text-gray pt-1">
                                    <span class="d-inline-block border-bottom border-secondary">Already have account?</span>
                                </div>
                                <div class="full-width heading-lg">Login now</div>
                                <div class="full-width pt-2 ">
                                    <div class="row">
                                        <div class="col-12 col-md-4 mb-2 mb-md-0">
                                            <a
                                                href="#"
                                                class="py-1 px-1 facebook-login full-width border">
                                                <i class="mdi mdi-facebook-box"></i>Facebook
                      </a>
                                        </div>
                                        <div class="col-12 col-md-4">
                                            <a
                                                href="#"
                                                class="py-1 px-1 google-login full-width border">
                                                <i class="mdi mdi-google"></i>Google
                      </a>
                                        </div>
                                        <div class="col-12 col-md-4">
                                            <a
                                                href="#"
                                                class="py-1 px-1 linkedin-login full-width border">
                                                <i class="mdi mdi-linkedin-box"></i>Linkedin
                      </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="full-width or-dividor text-center pt-2">
                                    OR
               
                                </div>
                                <div class="login-form pt-1 full-width pt-2">
                                    <ul class="form-list list-unstyled mb-0 pt-1 login-form-list">
                                        <li class="">
                                            <div class="full-width">
                                                <label>Email Id:</label>
                                                <input type="email" class="form-control full-width" />
                                            </div>
                                        </li>
                                        <li>
                                            <div class="full-width">
                                                <label>Password:</label>
                                                <input
                                                    type="password"
                                                    class="form-control full-width" />
                                                <div class="full-width text-right pt-1">
                                                    <a href="#">Forgot Password?</a>
                                                </div>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="full-width text-right pt-2">
                                                <button class="btn btn-outline-gray px-2 mr-1">
                                                    Reset
                       
                                                </button>
                                                <button class="btn btn-primary px-3">Login</button>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</asp:Content>--%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Dependent Visas</title>
    <!-- PWA Resources Starts -->
    <meta name="theme-color" content="#1a248f" />
    <link
        rel="icon"
        sizes="192x192"
        href="images/icons/c634d504-b2c5-a929-eaea-0974e25a3fe5.webPlatform.png" />
    <link rel="manifest" href="manifest.json" />
    <!-- PWA Resources Starts -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <link
        href="https://fonts.googleapis.com/css?family=PT+Sans:400,700&display=swap"
        rel="stylesheet" />
    <link
        rel="stylesheet"
        href="https://cdn.materialdesignicons.com/4.7.95/css/materialdesignicons.min.css" />
    <link rel="stylesheet" href="css/dependent-visas.css" />
    <script src="js/jquery.min.js"></script>

    <script>
        //$(document).ready(function () {
        //    debugger;
        //    alert("sadsfds");
        //    $('#Fileupload').bind('change', function () {
        //        debugger;
        //        if ($('#Fileupload').val() != '') {
        //            $.each($('#Fileupload').prop("files"), function (k, v) {
        //                var filename = v['name'];
        //                var ext = filename.split('.').pop().toLowerCase();
        //                if ($.inArray(ext, ['pdf', 'doc']) == -1) {
        //                    alert('Please upload only pdf,doc format files.');
        //                    return false;
        //                }
        //            });
        //        }
        //    })
        //});

        function controlEnter(obj, event) {
            var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            if (keyCode == 13) {
                __doPostBack(obj, '');
                //document.getElementById(obj).click();
                return false;
            }
            else {
                return true;
            }
        }
        function CheckFile(Cntrl) {
            var file = document.getElementById(Cntrl.name);
            var len = file.value.length;
            var ext = file.value;
            if (ext.substr(len - 3, len) != "pdf") {
                alert("Please select a doc or pdf file ");
                return false;
            }
        }

        $("#fileupload1").change(function () {
            alert("test");
            var fileUpload = $(this).get(0);

            var files = fileUpload.files;
            alert(files);

            var data = new FormData();
            for (var i = 0; i < files.length; i++) {
                data.append(files[i].name, files[i]);
            }
            $.ajax({
                url: "/fileupload.ashx?type=Orderfiles",
                type: "POST",
                data: data,
                contentType: false,
                processData: false,
                success: function (result) {

                    for (var i = 0; i < $("#fileupload1").prop("files").length; i++) {
                        $("#txtmsgbox").addClass("col-md-7 col-lg-8");
                        $("#divmsgfiles").removeClass("d-none");
                        $(".attchflname").text("");
                        var str = $('#fileupload1').prop("files")[i].name;
                        str = str.replace(/\s+/g, '_');
                        $(".attachments-list").append("<li class='attfile' data-uname='" + str + "' data-fname='" + result.split(',')[i] + "'><div class='each-attachment dlt_remove'><button class='btn btn-link remove-attachment-btn' data-fname=" + result.split(',')[i] + " title='delete' onclick=deletefile(\'" + str + "\',\'" + result.split(',')[i] + "\'); type='button'><i class='fa fa-times-circle'  aria-hidden='true'></i></button><i class='fa fa-file-pdf-o'></i><span class='file-name'>" + str + "</span></div></li>");
                    }
                },
                error: function (err) {
                }
            });
        });

    </script>
</head>
<body>
    <header class="full-width header border-bottom">
        <div class="container">
            <div class="row">
                <div class="col-5 col-md-2">
                    <div class="full-width branding-block">
                        <a
                            href="index.html"
                            class="branding d-block"
                            title="Dependent Visas">
                            <img
                                src="images/dependent-visas-logo.svg"
                                alt="Depdent Visas"
                                title="Dependent Visas" />
                        </a>
                    </div>
                </div>
                <div class="col-7 col-md-10">
                    <div class="full-width mobile-menu-btn-nav-block">
                        <nav class="main-navigation-block">
                            <ul class="main-navigation list-unstyled mb-0">
                                <li>
                                    <a href="Home.aspx" class="each-nav-link">Home
                                    </a>
                                </li>
                                <li>
                                    <a href="products.aspx" class="each-nav-link">Products</a>
                                </li>
                                <li>
                                    <a href="about-us.html" class="each-nav-link">About us</a>
                                </li>
                                <li>
                                    <a href="faqs.html" class="each-nav-link">FAQs</a>
                                </li>
                                <li>
                                    <a href="contact-us.html" class="each-nav-link">Contact us</a>
                                </li>
                                <li class="login-register-block active">
                                    <a href="Login.aspx" class="login-register-btn">Login / Signup</a>
                                </li>
                            </ul>
                        </nav>
                        <a href="Cart.aspx" class="header-cart-btn mr-1 mr-md-0 ml-md-1">
                            <i class="mdi mdi-cart"></i>
                            <span class="header-cart-count"><asp:Label ID="lblcount" runat="server"></asp:Label></span>
                        </a>
                        <button class="mobile-menu-toggle-btn">
                            <i class="mobile-menu-toggle-icon"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section class="full-width login-register main-content py-3 bg-light">
        <div class="container">
            <div class="row align-items-stretch">
                <div class="col-12 col-md-6 col-md-1 login-column">
                    <div
                        class="full-width login-form-block shadow-sm bg-white py-3 px-2">
                        <div class="full-width login-form-heading-block">
                            <div
                                class="small-heading full-width text-uppercase text-gray pt-1">
                                <span class="d-inline-block border-bottom border-secondary">New to our Website?</span>
                            </div>
                            <div class="full-width heading-lg">Register for FREE</div>
                            <div class="full-width pt-2 ">
                                <div class="row">
                                    <div class="col-12 col-md-4 mb-2 mb-md-0">
                                        <a
                                            href="#"
                                            class="py-1 px-1 facebook-login full-width border">
                                            <i class="mdi mdi-facebook-box"></i>Facebook
                                        </a>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <a
                                            href="#"
                                            class="py-1 px-1 google-login full-width border">
                                            <i class="mdi mdi-google"></i>Google
                                        </a>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <a
                                            href="#"
                                            class="py-1 px-1 linkedin-login full-width border">
                                            <i class="mdi mdi-linkedin-box"></i>Linkedin
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="full-width or-dividor text-center pt-2">
                                OR
               
                            </div>
                            <div class="login-form pt-1 full-width pt-2">
                                <div class="full-width uploadResume-block">
                                    <form action="#" class="dropzone" id="form1" runat="server">
                                        <div class="dz-message needsclick">
                                            <span class="d-md-none">Tap here to upload</span>
                                            <span class="d-none d-md-inline-block">Drop your resume here.</span>
                                            <br />
                                            <span class="note needsclick text-gray">(Upload .doc, .pdf only. and upto 2 MB)</span>
                                        </div>
                                     <%--   <div class="fallback" id="myAwesomeDropzone">--%>
                                           <%-- <span>--%>
                                            <asp:FileUpload ID="fileUpload1" runat="server"/>    
                                           <%-- </span>--%>
                                               <%-- <asp:FileUpload name="file" type="file" ID="Fileupload" runat="server" accept=".pdf,.doc"/></span>--%>
                                            <%-- <input type="file" ID="FileUpload" runat="server" accept="application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" />--%>
                                       <%-- </div>--%>
                                    <asp:label id="lblfile" runat="server"></asp:label>
                                </div>
                                <div class="full-width my-2 or-block text-center">
                                    <span>OR</span>
                                </div>
                                <ul class="form-list list-unstyled mb-0 pt-1 login-form-list">
                                    <li class="">
                                        <div class="full-width">
                                            <label>Name:</label>
                                            <input type="text" id="txtName" runat="server" class="form-control full-width" />
                                        </div>
                                    </li>
                                    <li>
                                        <div class="full-width">
                                            <label>Email:</label>
                                            <input type="email" class="form-control full-width" id="txtEmail" runat="server" />
                                            <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtEmail" ErrorMessage="Invalid Email Format" ForeColor="Red" Display="Dynamic"></asp:RegularExpressionValidator>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="full-width">
                                            <label>Contact Number:</label>
                                            <div class="row">
                                                <div class="col-4 col-md-3">
                                                    <select name="" id="" class="form-control">
                                                        <option value="">+1234 INDIA</option>
                                                        <option value="">123 IND</option>
                                                    </select>
                                                </div>
                                                <div class="col-8 col-lg-9 pl-1">
                                                    <%--<input type="tel" class="form-control" id="txtContact" runat="server" />--%>
                                                    <asp:TextBox ID="txtPhone" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="password">
                                        <label>Create Password:</label>
                                        <div class="password-input-block">
                                            <input
                                                type="password"
                                                name="password"
                                                class="form-control password-input"
                                                id="txtPassword" runat="server" />
                                            <span class="password-toggle"><i class="mdi mdi-eye-off"></i></span>
                                        </div>
                                        <div
                                            class="password-strength float-left font-size-sm full-width py-1 px-15"
                                            id="pwdsn"
                                            style="display: none;">
                                            <div
                                                class="password-star-rating-block rating font-size-sm d-flex align-items-center pb-15"
                                                id="pstrength">
                                                <span class="mr-1 font-italic">Password strength:</span>
                                                <i class="mdi mdi-star text-danger"></i>
                                                <i class="mdi mdi-star text-danger"></i>
                                                <i class="mdi mdi-star-outline"></i>
                                                <i class="mdi mdi-star-outline"></i>
                                                <i class="mdi mdi-star-outline"></i>
                                            </div>
                                            <div class="password-strength-title pb-05">
                                                Your password must have:
                       
                                            </div>
                                            <ul class="password-strength-list pl-2">
                                                <li class="text-danger pb-05 show" id="specialcase">One Special Character
                                                </li>
                                                <li class="text-danger pb-05 show" id="upcase">One upper alphabet
                                                </li>
                                                <li class="text-danger pb-05 show" id="lowcase">One lower alphabet
                                                </li>
                                                <li class="text-danger pb-05 show" id="numberlength">One numeric value
                                                </li>
                                                <li class="text-danger show" id="minlength">Minimum length - 8
                                                </li>
                                            </ul>
                                        </div>
                                        <div
                                            class="full-width text-danger"
                                            id="signupform_password_errorloc">
                                        </div>
                                    </li>

                                    <li>
                                        <div class="full-width text-gray pb-1">
                                            On submitting, you are agreeing our
                       
                                            <a href="#">Terms &amp; Conditions</a>
                                        </div>
                                        <div class="full-width text-right pt-2">
                                            <%--<button class="btn btn-outline-gray px-2 mr-1">
                                                Reset
                       
                                            </button>
                                            <button class="btn btn-primary px-3">Register</button>--%>
                                            <%--<form id="form2" runat="server">--%>
                                            <asp:button runat="server" Text="Reset" class="btn btn-outline-gray px-2 mr-1" ID="btnReset" OnClick="btnReset_Click">
                                                </asp:button>
                                                <asp:Button class="btn btn-primary px-3" runat="server" ID="btnRegister" Text="Register" OnClick="btnRegister_Click"></asp:Button>
                                               <%-- </form>--%>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-1 position-relative py-3">
                    <div class="login-or-block">
                        <span>OR</span>
                    </div>
                </div>
                <div class="col-12 col-md-5 col-md-1 register-column">
                    <div
                        class="full-width login-form-block shadow-sm bg-white py-3 px-2">
                        <div class="full-width login-form-heading-block">
                            <div
                                class="small-heading full-width text-uppercase text-gray pt-1">
                                <span class="d-inline-block border-bottom border-secondary">Already have account?</span>
                            </div>
                            <div class="full-width heading-lg">Login now</div>
                            <div class="full-width pt-2 ">
                                <div class="row">
                                    <div class="col-12 col-md-4 mb-2 mb-md-0">
                                        <a
                                            href="#"
                                            class="py-1 px-1 facebook-login full-width border">
                                            <i class="mdi mdi-facebook-box"></i>Facebook
                                        </a>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <a
                                            href="#"
                                            class="py-1 px-1 google-login full-width border">
                                            <i class="mdi mdi-google"></i>Google
                                        </a>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <a
                                            href="#"
                                            class="py-1 px-1 linkedin-login full-width border">
                                            <i class="mdi mdi-linkedin-box"></i>Linkedin
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="full-width or-dividor text-center pt-2">
                                OR
               
                            </div>
                            <div class="login-form pt-1 full-width pt-2">
                                <ul class="form-list list-unstyled mb-0 pt-1 login-form-list">
                                    <li class="">
                                        <div class="full-width">
                                            <label>Email Id:</label>
                                            <input type="email" class="form-control full-width" id="txtEmailId" runat="server"  />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtEmailId" ErrorMessage="Invalid Email Format" ForeColor="Red" Display="Dynamic"></asp:RegularExpressionValidator>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="full-width">
                                            <label>Password:</label>
                                            <input
                                                type="password"
                                                class="form-control full-width" id="txtPwd" runat="server"  />
                                            <div class="full-width text-right pt-1" >
                                                <a href="#">Forgot Password?</a>
                                            </div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="full-width text-right pt-2">
                                            <asp:button runat="server" ID="Reset" class="btn btn-outline-gray px-2 mr-1" Text="Reset">
                                            </asp:button>
                                            <asp:button runat="server" Text="Login" CssClass="btn btn-primary px-3" ID="btnLogin" OnClick="btnLogin_Click"></asp:button>
                                        </div>
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <footer class="footer full-width py-3 bg-light">
        <div class="full-width">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-3 col-xl-2 mb-3 mb-lg-0">
                        <div class="full-width footer-navigation-block">
                            <a href="index.html" class="each-footer-nav">Home</a>
                            <a href="products.aspx" class="each-footer-nav">Products</a>
                            <a href="about-us.html" class="each-footer-nav">About us</a>
                            <a href="faqs.html" class="each-footer-nav">FQAs</a>
                            <a href="contact-us.html" class="each-footer-nav">Contact us</a>
                            <a href="terms-conditions.html" class="each-footer-nav">Terms &amp; Conditions</a>
                            <a href="privacy-policy.html" class="each-footer-nav">Privacy Policy</a>
                            <a href="refund-policy.html" class="each-footer-nav">Refund Policy</a>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3 col-xl-3 mb-3 mb-lg-0">
                        <div class="full-width footer-navigation-block">
                            <a href="products.html" class="each-footer-nav">Canada Dependent Visa</a>
                            <a href="products.html" class="each-footer-nav">Germany Dependent Visa</a>
                            <a href="products.html" class="each-footer-nav">Uk Dependent Visa</a>
                            <a href="products.html" class="each-footer-nav">Canada Dependent Visa</a>
                            <a href="products.html" class="each-footer-nav">Germany Dependent Visa</a>
                            <a href="products.html" class="each-footer-nav">Uk Dependent Visa</a>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3 col-xl-3 mb-3 mb-lg-0">
                        <div class="full-width footer-navigation-block">
                            <a href="products.html" class="each-footer-nav">Dependent Visa for Spouse</a>
                            <a href="products.html" class="each-footer-nav">Dependent Visa for Kids</a>
                            <a href="products.html" class="each-footer-nav">Dependent Visa for Parents</a>
                        </div>
                    </div>

                    <div class="col-12 col-md-6 col-lg-3 col-xl-4 mb-3 mb-lg-0">
                        <div class="full-width subscribe-social-block">
                            <div class="full-width pb-1 font-size-md subscribe-heading">
                                Subscribe for latest updates
               
                            </div>
                            <div class="subscribe-block full-width mb-3">
                                <input type="email" class="subscribe-input form-control" />
                                <button class="subscribe-button btn-primary">
                                    Subscribe
                 
                                </button>
                            </div>
                            <div
                                class="social-block full-width font-size-md subscribe-heading py-1">
                                Follow us on
               
                            </div>
                            <div class="social-list-block full-width">
                                <a
                                    href="#"
                                    class="footer-each-social-link facebook"
                                    target="_blank"><i class="mdi mdi-facebook"></i></a>
                                <a
                                    href="#"
                                    class="footer-each-social-link twitter"
                                    target="_blank"><i class="mdi mdi-twitter"></i></a>
                                <a
                                    href="#"
                                    class="footer-each-social-link instagram"
                                    target="_blank"><i class="mdi mdi-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="full-width copy-rights-block text-center mt-2">
            <span class="copy-rights-text bg-light">Copy Rights &copy; 2019&nbsp;&nbsp;|&nbsp;&nbsp;<a href="#">www.dependentvisas.com</a></span>
        </div>
    </footer>

    <script src="js/common.js"></script>
    <script src="js/dropzone.js"></script>
    <script src="js/anime.min.js"></script>
    <script>
        $(document).ready(function () {
            anime({
                targets: ".each-home-section .each-product",
                opacity: [0, 1],
                delay: anime.stagger(100)
            });
        });
    </script>
</body>
</html>

